﻿using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MoreMountains.TopDownEngine
{
    /// <summary>
    /// An AIACtion used to request a reload on the weapon
    /// </summary>
    [AddComponentMenu("TopDown Engine/Character/AI/Actions/AIActionReload")]
    public class AIActionReload : AIAction
    {
        public bool OnlyReloadOnceInThisSate = true;

        protected CharacterHandleWeapon _characterHandleWeapon;
        protected bool _reloadedOnce = false;

        /// <summary>
        /// On init we grab our components
        /// </summary>
        protected override void Initialization()
        {
            base.Initialization();
            _characterHandleWeapon = this.gameObject.GetComponentInParent<Character>()?.FindAbility<CharacterHandleWeapon>();
        }

        /// <summary>
        /// Requests a reload
        /// </summary>
        public override void PerformAction()
        {
            RotateToTarget();
            if (OnlyReloadOnceInThisSate && _reloadedOnce)
            {
                return;
            }
            if (_characterHandleWeapon == null)
            {
                return;
            }
            _characterHandleWeapon.Reload();
            _reloadedOnce = true;
        }
        
        /// <summary>
        /// Поворачивает Обьект в таргету
        /// </summary>
        protected virtual void RotateToTarget() // Не из библиотеки
        {
            var characterMovement = this.gameObject.GetComponentInParent<TopDownController>();
            var direction = _brain.Target.position - characterMovement.transform.position;
            direction = direction.normalized;
            characterMovement.CurrentDirection = new Vector3(direction.x, direction.y, direction.y);
        }
        /// <summary>
        /// On enter state we reset our counter
        /// </summary>
        public override void OnEnterState()
        {
            base.OnEnterState();
            _reloadedOnce = false;
        }
    }
}
