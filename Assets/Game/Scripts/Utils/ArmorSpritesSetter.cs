﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmorSpritesSetter : MonoBehaviour
{
   [Tooltip("Ссылка на непосредственно компоненты отображения")]
   [SerializeField]
   private SpriteRenderer[] renderers;

   private Sprite[] defaultArmorSprites;

   private void Awake()
   {
      defaultArmorSprites = new Sprite[renderers.Length];
      for (int i = 0; i < renderers.Length; i++)
      {
         defaultArmorSprites[i] = renderers[i].sprite;
      }
   }

   public void SetArmorSprites(Sprite[] armorSprites)
   {
      for (int i = 0; i < armorSprites.Length; i++)
      {
         renderers[i].sprite = armorSprites[i];
      }
   }

   public void SetDefaultArmorSprites()
   {
      SetArmorSprites(defaultArmorSprites);
   }
}
