﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Класс для получения значения из корутины
/// </summary>
/// <typeparam name="T"></typeparam>
public class Ref<T>
{
    public T value;
    public Ref(T val)
    {
        value = val;
    }
    
    public Ref()
    {
        value = default(T);
    }
}
