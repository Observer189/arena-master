﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AccuracyConverter
{
    private const float maxSpreadAngle = 50;
    
    public static float SpreadToAccuracyParabolical(float spread)
    {
        if (spread > maxSpreadAngle) spread = maxSpreadAngle;

        return 100 - Mathf.Sqrt(spread/0.005f);
    }

    public static float AccuracyToSpreadParabolical(float accuracy)
    {
        if (accuracy > 100) accuracy = 100;
        var t = 100 - accuracy;
        float r = -0.005f*t*t + 50;
        r = 50 - r;
        return r;
    }
    
    

    public static float SpreadToAccuracyLinear(float spread)
    {
        return 100-spread / (maxSpreadAngle/100);
    }

    public static float AccuracyToSpreadLinear(float accuracy)
    {
        return maxSpreadAngle - accuracy * (maxSpreadAngle/100);
    }
}
