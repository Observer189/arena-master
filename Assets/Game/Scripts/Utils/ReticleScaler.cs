﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;
using UnityEngine.Animations;
/// <summary>
/// Класс который выставляет размер прицела в соответствии с разбросом пушки
/// </summary>
public class ReticleScaler : MonoBehaviour
{
    private RectTransform rectTransform;

    private Camera _camera;
    // Start is called before the first frame update
    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        _camera=Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        //var s= LevelManager.Instance.Players[0].GetComponent<ProjectileWeapon>().Spread.z;
        if (LevelManager.Instance.Players.First().GetComponent<CharacterHandleWeapon>().CurrentWeapon is ExtendedProjWeapon weapon)
        {
            var s = weapon.Spread.z;
            s=(s>Consts.MaxReticleDegreesSize)?Consts.MaxReticleDegreesSize:s;
            Vector2 reticlePos = _camera.ScreenToWorldPoint(Input.mousePosition);
            Vector2 playerPos = weapon.GetSpawnPosition();
            var dist = (playerPos - reticlePos).magnitude;
            s = dist * Mathf.Sin(Mathf.Deg2Rad*s);
            //Debug.Log("World size = "+s);
            s=(s>Consts.MaxReticleSizeInUnits)?Consts.MaxReticleSizeInUnits:s;
            //s=2*(Screen.height * s)/_camera.MMCameraWorldSpaceHeight();;//я хз откуда берется 512, но это работает
            rectTransform.transform.localScale = new Vector3(s,s,0);
            //Debug.Log("UI size = "+rectTransform.transform.localScale.x);
            //rectTransform.anchoredPosition=new Vector2(s,s);
            //SetSize(rectTransform,new Vector2(s,s));
        }
        
        
    }
    
    public static void SetSize(RectTransform trans, Vector2 newSize) {
        Vector2 oldSize = trans.rect.size;
        Vector2 deltaSize = newSize - oldSize;
        trans.offsetMin = trans.offsetMin - new Vector2(deltaSize.x * trans.pivot.x, deltaSize.y * trans.pivot.y);
        trans.offsetMax = trans.offsetMax + new Vector2(deltaSize.x * (1f - trans.pivot.x), deltaSize.y * (1f - trans.pivot.y));
    }
}
