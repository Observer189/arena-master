﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleDecalSetter : MonoBehaviour
{
    
   [SerializeField]
    private GameObject decal;
    
    

    private ParticleSystem particleSystem;

    private ParticleSystem.Particle[] particles;

    void Start()
    {
        particleSystem = GetComponent<ParticleSystem>();
        particles = new ParticleSystem.Particle[particleSystem.main.maxParticles];
    }

    private void LateUpdate()
    {
        int particleCount = particleSystem.GetParticles(particles);
        for (int i = 0; i < particleCount; i++)
        {
            if (particles[i].remainingLifetime <= Time.deltaTime)
            {
                //Debug.Log("Inst");
                Instantiate(decal, transform.TransformPoint(particles[i].position),
                        Quaternion.Euler(particles[i].rotation3D));
            }
        }
    }
}
