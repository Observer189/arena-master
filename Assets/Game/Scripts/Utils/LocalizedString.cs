﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean.Localization;
using UnityEngine;
[Serializable]
public class LocalizedString
{
    public Translation[] translations;

    public string GetTranslation(string lang)
    {
        for (int i = 0; i < translations.Length; i++)
        {
            if (translations[i].language == lang)
            {
                return translations[i].translation;
            }
        }

        return null;
    }

}

[Serializable]
public class Translation
{
    [LeanLanguageName]
    public string language;
    [TextArea(5,7)]
    public string translation;
}
