﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Feedbacks;
using MoreMountains.TopDownEngine;
using UnityEngine;

public class PlayerVisionChecker : MonoBehaviour
{
    [SerializeField]
    private MMFeedbacks startVisionFeedback;
    [SerializeField]
    private MMFeedbacks endVisionFeedback;
    [Tooltip("Точка от которой идет проверка на видимость")]
    [SerializeField]
    private Transform VisionPoint;
    [Tooltip("Минимальное расстояние на котором проверяется видимость")]
    [SerializeField]
    private float visionDistance;
    [Tooltip("Маска проверяющая препятствия на пути")]
    [SerializeField]
    private LayerMask obstacleMask;
    [Tooltip("Поставьте true если вам нужно, чтобы проверялась только дистанция без проверки на препятствия")]
    [SerializeField]
    private bool CheckOnlyDistance;
    [SerializeField]
    private float TimeEnterChecks;
    /// <summary>
    /// Текущее состояние видимости/невидимости
    /// </summary>
    private bool currentVision=false;

    private float timeAfterLastCheck=0;

    private void Update()
    {
        if (timeAfterLastCheck > TimeEnterChecks)
        {
            if (CheckVision())
            {
                if (!currentVision)
                {
                    startVisionFeedback.PlayFeedbacks();
                    currentVision = true;
                }
            }
            else
            {
                if (currentVision)
                {
                    endVisionFeedback.PlayFeedbacks();
                    currentVision = false;
                }
            }

            timeAfterLastCheck = 0;
        }

        timeAfterLastCheck += Time.deltaTime;
    }

    private bool CheckVision()
    {
        Vector2 playerPos=LevelManager.Instance.Players[0].transform.position;
        Vector2 visionPos = VisionPoint.position;
        //расстояние до игрока
        float playerDistance=Vector2.Distance(playerPos, visionPos);
        if (playerDistance <
            visionDistance)
        {
            if (!CheckOnlyDistance)
            {
                var hit = Physics2D.Raycast(VisionPoint.position, playerPos - visionPos, 
                    Math.Min(visionDistance,playerDistance),
                    obstacleMask);
                if (hit.collider == null)
                {
                    return true;
                }
                Debug.Log(hit.collider.gameObject.name);
            }
            else
                return true;
        }

        return false;
    }
}
