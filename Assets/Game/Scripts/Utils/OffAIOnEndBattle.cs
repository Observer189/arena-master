﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using UnityEngine;

public class OffAIOnEndBattle : MonoBehaviour,MMEventListener<BattleEvent>
{
    public void OnMMEvent(BattleEvent eventType)
    {
        if (eventType.eventType == BattleEventType.EndBattle)
        {
            GetComponent<AiOnOffBrain>().OffBrain();
        }
    }
    
    private void OnEnable()
    {
        this.MMEventStartListening<BattleEvent> ();
    }

    private void OnDisable()
    {
        this.MMEventStopListening<BattleEvent> ();
    }
}
