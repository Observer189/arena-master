﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchSystem
{
    /// <summary>
    /// Словарь тэгов
    /// К каждому тэгу прикреплён список объектов этот тэг, содержащий
    /// </summary>
    private static Dictionary<string, List<GameObject>> dictionary=new Dictionary<string, List<GameObject>>();
    
    
    /// <summary>
    /// Регистрация объекта
    /// Объект может иметь несколько тэгов
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="tags"></param>
    public static void RegisterObject(GameObject obj, string[] tags)
    {
        for (int i = 0; i < tags.Length; i++)
        {
            if(!dictionary.ContainsKey(tags[i])) dictionary.Add(tags[i],new List<GameObject>());
            
            dictionary[tags[i]].Add(obj);
        }
    }

    public static void UnregisterObject(GameObject obj, string[] tags)
    {
        for (int i = 0; i < tags.Length; i++)
        {
            dictionary[tags[i]].Remove(obj);
        }
    }

    public static GameObject FindGameObjectWithTag(string tag)
    {
        if (!dictionary.ContainsKey(tag))
        {
            Debug.LogError($"Объектов с тэгом {tag} зарегистрировано не было");
            return null;
        }

        return dictionary[tag][0];
    }

    public static List<GameObject> FindGameObjectsWithTag(string tag)
    {
        if (!dictionary.ContainsKey(tag))
        {
            Debug.LogError($"Объектов с тэгом {tag} зарегистрировано не было");
            return null;
        }
        
        return dictionary[tag];
    }

}
