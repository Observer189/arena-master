﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchableScript : MonoBehaviour
{
   public string[] tags;

   private void OnEnable()
   {
      SearchSystem.RegisterObject(gameObject,tags);
   }

   private void OnDisable()
   {
      SearchSystem.UnregisterObject(gameObject,tags);
   }
}
