﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using InventorySystem;
using Lean.Localization;
using UnityEngine;

public class ExLeanLanguageCSV : LeanLanguageCSV
{
    protected override void LoadLegacy()
    {
        var text = Source.text;
        
        var pattern = @"<(.+)>\s*=\s*'(.+)'";
        RegexOptions options = RegexOptions.Multiline;
    
        
        
        foreach (Match match in Regex.Matches(text,pattern,options))
        {
            var entry = entryPool.Count > 0 ? entryPool.Pop() : new Entry();
            
            entry.Name = match.Groups[1].ToString();
            entry.Text = match.Groups[2].ToString();
            
            Entries.Add(entry);
        }
    }
    
    [ContextMenu("Localization/GenerateLocalizationPrefab")]
    protected virtual void GenerateLocalizationFile()
    {
        Debug.Log("Generation...");
        LoadFromSource();
        string pathToDestination = $"C:/Users/user/Documents/Arena master/Assets/Game/Localizations/{Language}Prefab.txt";
        StringBuilder output = new StringBuilder();
        output.AppendLine("\n//Предметы\n");
        var items =Resources.FindObjectsOfTypeAll<ItemInfo>();
        foreach (var item in items)
        {
            var ent=Entries.Find(e => e.Name == item.Name);
            if (ent==null)
            {
                output.AppendLine($"<{item.Name}> = ''");
            }
            else
            {
                output.AppendLine($"<{ent.Name}> = '{ent.Text}'");
            }
            var ent2=Entries.Find(e => e.Name == item.Name+"Desc");
            if (ent2 == null)
            {
                output.AppendLine($"    <{item.Name + "Desc"}> = ''");
            }
            else
            {
                output.AppendLine($"    <{ent2.Name}> = '{ent2.Text}'");   
            }

        }
        
        output.AppendLine(" \n//Свойства\n");
        var properties = Resources.FindObjectsOfTypeAll<ObjectPropertyDescription>();
        foreach (var property in properties)
        {
            var ent=Entries.Find(e => e.Name == property.Name);
            if (ent == null)
            {
                output.AppendLine($"<{property.Name}> = ''");
            }
            else
            {
                output.AppendLine($"<{ent.Name}> = '{ent.Text}'");
            }
            var ent2=Entries.Find(e => e.Name == property.ShortName);
            if (ent2 == null)
            {
                output.AppendLine($"    <{property.ShortName}> = ''");
            }
            else
            {
                output.AppendLine($"    <{ent2.Name}> = '{ent2.Text}'");
            }
            var ent3=Entries.Find(e => e.Name == property.Name+"Desc");
            if (ent3 == null)
            {
                output.AppendLine($"    <{property.Name+"Desc"}> = ''");
            }
            else
            {
                output.AppendLine($"    <{ent3.Name}> = '{ent3.Text}'");
            }

            //if (!Entries.Exists(e => e.Name == property.Name))
        }
        File.WriteAllText(pathToDestination,output.ToString());
        Debug.Log("Generation complete!");
    }
}
