﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;
using UnityEngine.Serialization;

[RequireComponent(typeof(DirectionalLight))]
public class SunSimulator : MonoBehaviour
{
    private Light sun;
    
    private float timer=0;
    private PartOfDay partOfDay;
    [SerializeField] 
    private Color morningColor;
    [SerializeField]
    private Color afternoonColor;
    [SerializeField]
    private Color eveningColor;
    [SerializeField]
    private Color nightColor;
    [Tooltip("Время смены одного времени суток в секундах")]
    [SerializeField]
    private float partSwitchTime;
    
    private float speed;
    private void Awake()
    {
        sun = GetComponent<Light>();
    }

    // Start is called before the first frame update
    void Start()
    {
        speed = 1/partSwitchTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (timer > 1)
        {
            timer = 0;
            SwitchPartOfDay();
        }
        Color col=new Color();
        timer += Time.deltaTime*speed;
        switch (partOfDay)
        {
            case PartOfDay.Night: col = Color.Lerp(nightColor, morningColor, timer);
                break;
            case PartOfDay.AfterNoon: col = Color.Lerp(afternoonColor, eveningColor, timer);
                break;
            case PartOfDay.Morning: col = Color.Lerp(morningColor,afternoonColor, timer);
                break;
            case PartOfDay.Evening: col = Color.Lerp(eveningColor, nightColor, timer);
                break;
        }

        sun.color = col;
        
    }

    public void SwitchPartOfDay()
    {
        switch (partOfDay)
        {
            case PartOfDay.AfterNoon:
                partOfDay = PartOfDay.Evening;
                break;
            case PartOfDay.Night:
                partOfDay = PartOfDay.Morning;
                break;
            case PartOfDay.Morning:
                partOfDay = PartOfDay.AfterNoon;
                break;
            case PartOfDay.Evening:
                partOfDay = PartOfDay.Night;
                break;
        }
    }


}

public enum PartOfDay
{
    Morning,AfterNoon,Evening,Night
}
