﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationSwitch : MonoBehaviour {

    public Animator anim;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space)) {
            anim.SetInteger("anim",anim.GetInteger("anim")+1);
            if (anim.GetInteger("anim") == 3) {
                anim.SetInteger("anim", 0);
            }
        }
	}

    
}
