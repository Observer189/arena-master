﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Компонент для зон, которые накладывают на игрока определённые эффекты
/// </summary>
[RequireComponent(typeof(Collider2D))]
public class EffectZone : MonoBehaviour
{
    [Tooltip("Список эффектов, которые ложатся на персонжа при входе в зону и снимаются при выходе")]
    [SerializeField]
    protected EffectDescription[] zoneEffects;
    /// <summary>
    /// Список объектов находящихся в зоне
    /// </summary>
    protected HashSet<PropertyManager> registeredObjects;

    private void Awake()
    {
        registeredObjects= new HashSet<PropertyManager>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        PropertyManager propertyManager = other.attachedRigidbody.GetComponent<PropertyManager>();
        if (propertyManager != null)
        {
            Debug.Log("Trigger enter!");
            registeredObjects.Add(propertyManager);
            for (int i = 0; i < zoneEffects.Length; i++)
            {
                propertyManager.AddEffect(new Effect(zoneEffects[i]));
            }
        }
    }
    
    private void OnTriggerExit2D(Collider2D other)
    {
        PropertyManager propertyManager = other.attachedRigidbody.GetComponent<PropertyManager>();
        if (propertyManager != null)
        {
            Debug.Log("Trigger exit!");
            for (int i = 0; i < zoneEffects.Length; i++)
            {
                propertyManager.RemoveEffect(zoneEffects[i].Id);
            }
        }
    }
}
