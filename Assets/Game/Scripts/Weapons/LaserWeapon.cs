﻿using System;
using System.Collections;
using System.Collections.Generic;
using InventorySystem;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;
using UnityEngine.PlayerLoop;
using Random = UnityEngine.Random;

public class LaserWeapon : Weapon, IExWeapon, IAmmoBasedWeapon,MMEventListener<MMGameEvent>
{
    protected PropertyManager propertyManager;

    public PropertyManager PropertyManager => propertyManager;

    public WeaponAnimationType WeaponAnimationType => weaponAnimationType;

    public Sprite DisplayImage => displayImage;
    
    public bool NeedAimToShoot => needAimToShoot;
    public WeaponItem Item { get; set; }

    [MMInspectorGroup("Beam Properties", true, 2)] 
    [SerializeField]
    protected GameObject beamPrefab;
    
    private List<LineRenderer> beamRenderers;
    [SerializeField] 
    protected Transform beamStartPosition;
    [SerializeField] 
    protected float beamRange;
    [SerializeField] 
    protected float beamWidth;
    [SerializeField] protected LayerMask beamTargetMask;
    [Tooltip("Расстояния для компенсации неровности луча, добавляется к длине луча")]
    [SerializeField] protected float beamCompDist=0.2f;
    [Tooltip("Количество целей,пробиваемых насквозь")]
    [SerializeField] protected int pierceCount;
    [Tooltip("Количество рикошетов луча")]
    [SerializeField] protected int bounceCount;
    [Tooltip("Количество лучей лазера")]
    [SerializeField] protected int raysCount=1;
    [SerializeField] protected bool needAimToShoot = true;

    [SerializeField]
    protected EffectDescription[] effects;
    [SerializeField] protected bool isImpulse;
    [Tooltip("Время после выстрела через которое луч лазера исчезает в импульсном режиме")]
    [SerializeField] protected float beamDisappearTime;
    [Tooltip("Должен ли луч пропадать постепенно")]
    [SerializeField]
    protected bool useTransparentDisappear;
    [SerializeField] 
    protected float accuracy=0;
    
    [MMInspectorGroup("Additional animation", true, 10)]
    [SerializeField]
    protected string ReloadSpeedAnimationParameter="ReloadSpeed";
    [SerializeField]
    protected string ExShootingAnimationParameter="ExShooting";

    [Tooltip("Время после выстрела в течении, которого считается, что персонаж находится в состоянии стрельбы")]
    [SerializeField]
    protected float afterUseShootingStateDuration = 0.1f;
    
    [SerializeField]
    protected Sprite displayImage;
    
    [SerializeField] 
    protected WeaponAnimationType weaponAnimationType;

    protected float timeAfterLastUse;
    
    protected int _reloadSpeedAnimationParameter;

    protected int _exShootingAnimationParameter;
    
    private float timeAfterLastAttack;
    /// <summary>
    /// Коллайдер цели по которой в данный момент бьет луч
    /// </summary>
    protected Collider2D[] targetColliders;
    /// <summary>
    /// Массив уже использованных лучей в импульсном режиме
    /// </summary>
    protected List<LineRenderer> oldBeams;
    [MMInspectorGroup("Inventory connection", true, 11)]
    [Tooltip("Должны ли патроны для этого оружия браться из инвентаря")]
    [SerializeField]
    protected bool isAmmoInventoryBased;
    [Tooltip("Id предмета инвентаря, который будет использоваться как патрон для оружия")]
    [SerializeField]
    protected int ammoId;
    
    private int availableAmmo;
    private Inventory relatedInventory;

    /// <summary>
    /// Переменная определяющая урон по щиту
    /// </summary>
    protected float damageToShield;
    
    public override void Initialization()
    {
        base.Initialization();
        propertyManager = Owner.GetComponent<PropertyManager>();
        Transform chwTransform = Owner.GetComponent<CharacterHandleWeapon>().ProjectileSpawn;
        if (chwTransform != null)
        {
            beamStartPosition = chwTransform;
        }

        timeAfterLastAttack = 0;
        oldBeams=new List<LineRenderer>();
        if (!isImpulse)
        {
            beamRenderers=new List<LineRenderer>();
            for (int i = 0; i < raysCount; i++)
            {
                var beamRenderer = Instantiate(beamPrefab, beamStartPosition.position, Quaternion.identity).GetComponent<LineRenderer>();
                beamRenderer.startWidth = beamWidth;
                beamRenderer.endWidth = beamWidth;
                beamRenderers.Add(beamRenderer);
                beamRenderer.gameObject.SetActive(false);
                beamRenderer.transform.parent = null;
            }
        }

        if (isAmmoInventoryBased)
        {
            relatedInventory = PlayerData.instance.inventory;
        }
    }

    public override void WeaponUse()
    {
        base.WeaponUse();
       // Debug.Log("Use laser");
       if (isImpulse)
       {
           for (int i = 0; i < raysCount; i++)
           {
               var newBeam = Instantiate(beamPrefab, beamStartPosition.position, Quaternion.identity)
                   .GetComponent<LineRenderer>();
               var spread = AccuracyConverter.AccuracyToSpreadParabolical(accuracy);
               spread = Random.Range(-spread, spread);
               var randDir = ((Vector2) transform.right).MMRotate(spread);
               var targets = DrawBeam(newBeam, randDir);
               foreach (var tar in targets)
               {
                   OnHitTarget(tar);
               }

               if (useTransparentDisappear)
               {
                   //oldBeams.Add(newBeam);
                   newBeam.GetComponent<LaserBeamDisappearer>().InitializeDisappear(beamDisappearTime);
               }
               else
               {
                   Destroy(newBeam.gameObject, beamDisappearTime);
               }
           }
       }
       else
       {
           /*foreach (var beamRenderer in beamRenderers)
           {
               targetColliders = DrawBeam(beamRenderer,transform.right);
               foreach (var target in targetColliders)
               {
                   OnHitTarget(target);   
               }
           }*/
           DrawNativeRenderers(true);
       }
       timeAfterLastAttack = 0;
    }

    protected void OnHitTarget(Collider2D target)
    {
        if (target != null)
        {
            var propertyManager = target.attachedRigidbody.GetComponent<PropertyManager>();
                if (propertyManager != null)
                {
                    AffectTarget(propertyManager);
                }
        }
    }

    protected void AffectTarget(PropertyManager target)
    {
        for (int i = 0; i < effects.Length; i++)
        {
            var eff= new Effect(effects[i],PropertyManager);
            target.AddEffect(eff);
        }
    }
    /// <summary>
    /// При вызове этой функции щит всегда будет вражеский
    /// </summary>
    /// <param name="shield"></param>
    protected void AffectShield(Shield shield)
    {
        shield.HitShield(damageToShield);
        Debug.Log("HitShield");
    }

    protected Collider2D[] DrawBeam(LineRenderer beam, Vector2 direction)
    {
        List<Collider2D> targets=new List<Collider2D>();
        List<Vector3> positions = new List<Vector3>();
        var col=Physics2D.OverlapPoint(beamStartPosition.position,beamTargetMask);
        ///Если вдруг точка из которой мы делаем рэйкаст оказывается внутри препятствия,то завершаем путь лазера
        if (col!=null && col.attachedRigidbody!=null && col.attachedRigidbody.GetComponent<Health>()==null)
        {
            //Debug.Log(col.name);
            HideBeams();
            return targets.ToArray();
        }
        positions.Add(beamStartPosition.position);
        DrawBeamRec(targets,positions,direction,beamRange+beamCompDist,pierceCount,bounceCount);
        beam.gameObject.SetActive(true);
        beam.positionCount = positions.Count;
        beam.SetPositions(positions.ToArray());
        return targets.ToArray();
    }

    private void OnDrawGizmos()
    {
        var spread = AccuracyConverter.AccuracyToSpreadParabolical(accuracy);
        var dir1 = ((Vector2) transform.right).MMRotate(-spread);
        var dir2 = ((Vector2) transform.right).MMRotate(spread);
        Gizmos.DrawRay(beamStartPosition.position,dir1*20);
        Gizmos.DrawRay(beamStartPosition.position,dir2*20);
    }

    /// <summary>
    /// Рекурсивно отрисовывает луч с заданными параметрами и сохраняет все задетые цели
    /// </summary>
    /// <param name="beam"></param>
    /// <param name="hitTargets">Массив, где будут сохранены все пораженные цели</param>
    /// <param name="positions">Массив, в котором сохраняются позиции луча</param>>
    /// <param name="direction">Направляющий вектор луча</param>
    /// <param name="distRemain">оставшаяся длина луча</param>
    /// <param name="pierceRemain">оставшееся количество пробитий цели</param>
    /// <param name="bounceRemain">оставшееся количество рикошетов луча</param>
    protected void DrawBeamRec(List<Collider2D> hitTargets,List<Vector3> positions,Vector2 direction,
        float distRemain,int pierceRemain,int bounceRemain)
    {
        var dist = distRemain;
        var startPosition = positions[positions.Count-1];
        var remP = pierceRemain;
        MMDebug.DrawPoint(startPosition,Color.red, 0.1f);
        ///Эта компенсация необходима, так как без нее луч не всегда точно определяет
        /// находится он в коллайдере препятствия
        var physicsCompensation=direction*0.05f;
        var col=Physics2D.OverlapPoint((Vector2)startPosition + physicsCompensation,beamTargetMask);
        //Debug.Log(col);
        ///Если вдруг точка из которой мы делаем рэйкаст оказывается внутри препятствия,то завершаем путь лазера

        if (col!=null && col.attachedRigidbody!=null && col.attachedRigidbody.GetComponent<Health>()==null)
        {
           // Debug.Log(col.name);
            return;
        }

        RaycastHit2D[] hits = Physics2D.RaycastAll((Vector2)startPosition+physicsCompensation,direction,distRemain,beamTargetMask);
        if (hits.Length > 0)
        {
            foreach (var hit in hits)
            {
                MMDebug.DrawPoint(hit.point,Color.magenta, 0.1f);
                //Debug.DrawRay(hit.point,direction*5,Color.magenta,10);
                Shield shield = hit.collider.GetComponent<Shield>();
                if (shield != null)
                {
                    //Если попадаем по своему щиту, то ничего не делаем
                    if (shield.transform.root.gameObject == Owner.gameObject)
                    {
                        Debug.Log("passShield");
                    }
                    //Если попадаем по щиту противника, то воздействуем на него и завершаем цикл
                    else
                    {
                        positions.Add(hit.point);
                        AffectShield(shield);
                        break;
                    }
                }
                ///Если попадаем по уничтожимому объекту, то по возможности пробиваем его
                else if (hit.collider.attachedRigidbody!=null && hit.collider.attachedRigidbody.GetComponent<Health>() != null)
                {
                    hitTargets.Add(hit.collider);
                    pierceRemain--;
                    if (pierceRemain<0)
                    {
                        positions.Add(hit.point+direction*beamCompDist);
                        break;
                    }
                }
                ///В противном случае инициируем рикошет
                else
                {
                    dist -= hit.distance;
                    //Debug.Log(hit.distance);
                    positions.Add(hit.point/*+direction*beamCompDist*/);
                    if(bounceRemain>0)
                    {
                        var newDirection = Vector2.Reflect(direction, hit.normal);
                    DrawBeamRec(hitTargets,positions,newDirection,dist,remP,bounceRemain-1);
                    }
                    break;
                }
            }
        }
        else
        {
            positions.Add((Vector2)startPosition+direction.normalized*distRemain);
        }
    }

    protected override void Update()
    {
        base.Update();
        if (isImpulse)
        {
            //ProceedOldBeams();
        }
        else
        {
            if ((WeaponState.CurrentState == WeaponStates.WeaponDelayBetweenUses ||
                WeaponState.CurrentState == WeaponStates.WeaponUse)&& !_triggerReleased)
            {
                DrawNativeRenderers();
            }
            else
            {
                HideBeams();
            }
        }
        
        
        if (Owner is ExCharacter exCharacter)
        {
            timeAfterLastUse += Time.deltaTime;
            if (timeAfterLastUse > afterUseShootingStateDuration && exCharacter.actionState.CurrentState==CharacterActionState.Shooting)
            {
                exCharacter.actionState.ChangeState(CharacterActionState.Aiming);
            }
        }
        
        if(Item!=null)
            Item.AmmoCount = CurrentAmmoLoaded;
    }

    protected void HideBeams()
    {
        if (beamRenderers != null)
        {
            foreach (var beamRenderer in beamRenderers)
            {
                beamRenderer.gameObject.SetActive(false);
            }
        }
    }

    /// <summary>
    /// Отображает лучи в неимпульсном режиме и бьет по задетым объектам, если это указать
    /// </summary>
    protected void DrawNativeRenderers(bool hitTargets=false)
    {
        var spread = AccuracyConverter.AccuracyToSpreadParabolical(accuracy);
        var step = (beamRenderers.Count==1)?0:2 * spread / (beamRenderers.Count - 1);
        for (int i = 0; i < beamRenderers.Count; i++)
        {
            float angle = Mathf.Pow(-1, i) * step * ((i + 1) / 2);
            //Debug.Log(angle);
            var dir = ((Vector2) transform.right).MMRotate(angle);
            targetColliders = DrawBeam(beamRenderers[i],dir);
            if (hitTargets)
            {
                foreach (var target in targetColliders)
                {
                    OnHitTarget(target);
                }
            }
        }
    }
    /// <summary>
    /// В импульсном режиме отстрелянные лучи должны медленнно пропадать
    /// Эта функция и обеспечивает их постепенное исчезновение
    /// </summary>
     protected void ProceedOldBeams()
    {
        for (int i = 0; i < oldBeams.Count; i++)
        {
            var beam = oldBeams[i];
            var sc = beam.startColor;
            var alpha = sc.a;
            if (alpha > 0)
            {
                var newAlpha = alpha - Time.deltaTime/beamDisappearTime;
                Color c1= new Color(sc.r,sc.g,sc.b,newAlpha/*255*Time.deltaTime/beamDisappearTime*/);
                Color c2=new Color(beam.endColor.r,beam.endColor.g,beam.endColor.b,newAlpha);
                beam.startColor = c1;
                beam.endColor = c2;
            }
            else
            {
                Destroy(beam.gameObject);
                oldBeams.Remove(beam);
            }
        }
    }
    /// <summary>
    /// Разрушаем все активные лучи при уничтожении объекта
    /// Срабатывает например при смене оружия или при закрытии инвентаря
    /// </summary>
    private void OnDestroy()
    {
        if (beamRenderers != null)
        {
            for (int i = 0; i < beamRenderers.Count; i++)
            {
                Destroy(beamRenderers[i].gameObject);
            }

            beamRenderers.Clear();
        }
    }

    public bool IsAmmoInventoryBased => isAmmoInventoryBased;

    public int AmmoId => ammoId;

    public int AvailableAmmo => availableAmmo;

    public Inventory RelatedInventory => relatedInventory;

    public void UpdateAvailableAmmo()
    {
        if(IsAmmoInventoryBased)
        availableAmmo = relatedInventory.GetItemQuantity(ammoId);
    }
    /// <summary>
    /// Абсолютно такое же переопределение есть в ExProjWeapon
    /// Пришлось продублировать, так как лазер не наследуется от ExProjWeapon
    /// </summary>
    public override void CaseWeaponReloadStop()
    {
        //base.CaseWeaponReloadStop();
        _reloading = false;
        WeaponState.ChangeState(WeaponStates.WeaponIdle);
        //Добавлена работа с инвентарем
        if (isAmmoInventoryBased)
        {
            int neededToFull = MagazineSize - CurrentAmmoLoaded;
            int count = Mathf.Min(availableAmmo,neededToFull);
            CurrentAmmoLoaded += count;
            relatedInventory.RemoveItem(ammoId,count);
            availableAmmo -= count;
        }
        else if (WeaponAmmo == null)
        {
            CurrentAmmoLoaded = MagazineSize;
        }
        
        if(Owner is ExCharacter exCharacter)
          exCharacter.actionState.ChangeState(CharacterActionState.Idle);
            
    }
    /// <summary>
    /// Также продублировано
    /// </summary>
    public override void InitiateReloadWeapon()
    {
        // if we're already reloading, we do nothing and exit
        //Улучшена проверка на необходимость перезарядки
        if (_reloading || MagazineSize==CurrentAmmoLoaded || (isAmmoInventoryBased && availableAmmo==0))
        {
            return;
        }

        if (Owner is ExCharacter exCharacter)
        {
            if (exCharacter.actionState.CurrentState == CharacterActionState.UsingItem
                || exCharacter.actionState.CurrentState == CharacterActionState.ThrowingItem)
            {
                return;
            }
        }

        if (PreventAllMovementWhileInUse && (_characterMovement != null))
        {
            _characterMovement.MovementForbidden = false;
        }
        WeaponState.ChangeState(WeaponStates.WeaponReloadStart);
       if(Owner is ExCharacter character) character.actionState.ChangeState(CharacterActionState.Reloading);
       _reloading = true;
    }
    
    public override void CaseWeaponUse()
    {
        base.CaseWeaponUse();
        if (Owner is ExCharacter exCharacter)
        {
            timeAfterLastUse = 0;
            exCharacter.actionState.ChangeState(CharacterActionState.Shooting);
        }
    }
    
    
    public override void CaseWeaponReloadNeeded()
    {
        base.CaseWeaponReloadNeeded();
        if (Owner is ExCharacter exCharacter)
        {
            exCharacter.actionState.ChangeState(CharacterActionState.Aiming);
        }
    }
    
    /// <summary>
    /// Надо будет поменять на событие обновления инвентаря
    /// Так как сейчас он не ловит изменение инвентаря например через диалог
    /// </summary>
    public virtual void OnMMEvent(MMGameEvent gameEvent)
    {
        switch (gameEvent.EventName)
        {
            case "inventoryOpens":
                break;

            case "inventoryCloses":
                UpdateAvailableAmmo();
                break;
        }
    }
    
    protected override void AddParametersToAnimator(Animator animator, HashSet<int> list)
    {
        base.AddParametersToAnimator(animator, list);
        MMAnimatorExtensions.AddAnimatorParameterIfExists(animator, ReloadSpeedAnimationParameter, out _reloadSpeedAnimationParameter, AnimatorControllerParameterType.Float, list);
        MMAnimatorExtensions.AddAnimatorParameterIfExists(animator, ExShootingAnimationParameter, out _exShootingAnimationParameter, AnimatorControllerParameterType.Bool, list);
    }

    protected override void UpdateAnimator(Animator animator, HashSet<int> list)
    {
        base.UpdateAnimator(animator, list);
        MMAnimatorExtensions.UpdateAnimatorFloat(animator, _reloadSpeedAnimationParameter, ReloadTime, list);
        if(Owner is ExCharacter exCharacter)
            MMAnimatorExtensions.UpdateAnimatorBool(animator, _exShootingAnimationParameter,exCharacter.actionState.CurrentState==CharacterActionState.Shooting , list);
    }
    
    protected void OnEnable()
    {
        this.MMEventStartListening();
    }

    protected void OnDisable()
    {
        this.MMEventStopListening();
    }
}
