﻿using System;
using System.Collections;
using System.Collections.Generic;
using InventorySystem;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;
using UnityEngine.PlayerLoop;


public class ExtendedProjWeapon : ProjectileWeapon, IExWeapon,IAmmoBasedWeapon,MMEventListener<MMGameEvent>
{
   
    
    protected PropertyManager propertyManager;
    
    public PropertyManager PropertyManager { get; }

    public WeaponAnimationType WeaponAnimationType => weaponAnimationType;

    public Sprite DisplayImage => displayImage;
    
    public bool NeedAimToShoot => needAimToShoot;
    
    public WeaponItem Item { get; set; }
    [MMInspectorGroup("Inventory connection", true, 11)]
    [Tooltip("Должны ли патроны для этого оружия браться из инвентаря")]
    [SerializeField]
    protected bool isAmmoInventoryBased;
    [Tooltip("Id предмета инвентаря, который будет использоваться как патрон для оружия")]
    [SerializeField]
    protected int ammoId;
    
    [MMInspectorGroup("Additional animation", true, 10)]
    [SerializeField]
    protected string ReloadSpeedAnimationParameter="ReloadSpeed";
    [SerializeField]
    protected string ExShootingAnimationParameter="ExShooting";

    [Tooltip("Время после выстрела в течении, которого считается, что персонаж находится в состоянии стрельбы")]
    [SerializeField]
    protected float afterUseShootingStateDuration = 0.1f;
    [SerializeField]
    protected Sprite displayImage;
    [SerializeField] 
    protected WeaponAnimationType weaponAnimationType;

    [SerializeField] protected bool needAimToShoot = true;

    [MMInspectorGroup("Multiple spawn positions", true, 5)]
    [Tooltip("Определяет использует ли оружие несколько точек спавна")]
    [SerializeField]
    protected bool useMultipleSpawnPosition = false;
    [Tooltip("Несколько точек спавна пуль используемых последовательно")]
    [SerializeField]
    protected Transform[] multipleSpawnPos;
    /// <summary>
    /// Текущая позиция спавна из множества
    /// </summary>
    protected int curMultipleSpawnPos=0;

    public Transform[] MultipleSpawnPos
    {
        get => multipleSpawnPos;
        set => multipleSpawnPos = value;
    }

    protected float timeAfterLastUse;
    
    protected int _reloadSpeedAnimationParameter;

    protected int _exShootingAnimationParameter;

    protected Inventory relatedInventory;

    protected int availableAmmo;

    public bool IsAmmoInventoryBased => isAmmoInventoryBased;

    public int AmmoId => ammoId;

    public int AvailableAmmo => availableAmmo;

    public Inventory RelatedInventory => relatedInventory;


    public override void Initialization()
    {
        base.Initialization();
        propertyManager = Owner.GetComponent<PropertyManager>();
        if (propertyManager == null)//гарантия того что объекты с компонентом имеют PropertyManager
        {
            propertyManager = Owner.gameObject.AddComponent<PropertyManager>();
        }
         
        if (isAmmoInventoryBased)
        {
            relatedInventory = PlayerData.instance.inventory;
        }
        
    }

    protected override void AddParametersToAnimator(Animator animator, HashSet<int> list)
    {
        base.AddParametersToAnimator(animator, list);
        MMAnimatorExtensions.AddAnimatorParameterIfExists(animator, ReloadSpeedAnimationParameter, out _reloadSpeedAnimationParameter, AnimatorControllerParameterType.Float, list);
        MMAnimatorExtensions.AddAnimatorParameterIfExists(animator, ExShootingAnimationParameter, out _exShootingAnimationParameter, AnimatorControllerParameterType.Bool, list);
    }

    protected override void UpdateAnimator(Animator animator, HashSet<int> list)
    {
        base.UpdateAnimator(animator, list);
        MMAnimatorExtensions.UpdateAnimatorFloat(animator, _reloadSpeedAnimationParameter, 1/ReloadTime, list);
        if(Owner is ExCharacter exCharacter)
        MMAnimatorExtensions.UpdateAnimatorBool(animator, _exShootingAnimationParameter,exCharacter.actionState.CurrentState==CharacterActionState.Shooting , list);
    }

    protected override void Update()
    {
        base.Update();
        if (Owner is ExCharacter exCharacter)
        {
            timeAfterLastUse += Time.deltaTime;
            if (timeAfterLastUse > afterUseShootingStateDuration && exCharacter.actionState.CurrentState==CharacterActionState.Shooting)
            {
               exCharacter.actionState.ChangeState(CharacterActionState.Aiming);
            }
        }
        if(Item!=null)
            Item.AmmoCount = CurrentAmmoLoaded;
    }

    public Vector3 GetSpawnPosition()
    {
        return this.transform.position + this.transform.rotation * ProjectileSpawnOffset;
    }


    public void UpdateAvailableAmmo()
    {
        if(relatedInventory!=null)
        availableAmmo = relatedInventory.GetItemQuantity(ammoId);
    }

    public override void CaseWeaponReloadStop()
    {
        //base.CaseWeaponReloadStop();
        _reloading = false;
        WeaponState.ChangeState(WeaponStates.WeaponIdle);
        //Добавлена работа с инвентарем
        if (isAmmoInventoryBased)
        {
            int neededToFull = MagazineSize - CurrentAmmoLoaded;
            int count = Mathf.Min(availableAmmo,neededToFull);
            CurrentAmmoLoaded += count;
            relatedInventory.RemoveItem(ammoId,count);
            availableAmmo -= count;
        }
        else if (WeaponAmmo == null)
        {
            CurrentAmmoLoaded = MagazineSize;
        }
        ((ExCharacter)Owner).actionState.ChangeState(CharacterActionState.Idle);
    }

    public override void InitiateReloadWeapon()
    {
        // if we're already reloading, we do nothing and exit
        //Улучшена проверка на необходимость перезарядки
        if (_reloading || MagazineSize==CurrentAmmoLoaded || (isAmmoInventoryBased && availableAmmo==0)
            ||((ExCharacter)Owner).actionState.CurrentState==CharacterActionState.UsingItem||
            ((ExCharacter)Owner).actionState.CurrentState==CharacterActionState.ThrowingItem)
        {
            return;
        }
        
        if (PreventAllMovementWhileInUse && (_characterMovement != null))
        {
            _characterMovement.MovementForbidden = false;
        }
        WeaponState.ChangeState(WeaponStates.WeaponReloadStart);
        ((ExCharacter)Owner).actionState.ChangeState(CharacterActionState.Reloading);
        _reloading = true;
    }
    /// <summary>
    /// Это переопределение необходимр так как в противном случае
    /// может получиться ситуация, когда в оружии заряжено отрицательное кол-во патронов
    /// Почему этот случай не предусмотрен библиотекой - неясно 
    /// </summary>
    public override void ShootRequest()
    {
        base.ShootRequest();
        if (CurrentAmmoLoaded < 0) CurrentAmmoLoaded = 0;
    }

    public virtual void OnMMEvent(MMGameEvent gameEvent)
    {
        switch (gameEvent.EventName)
        {
            case "inventoryOpens":
                break;

            case "inventoryCloses":
                UpdateAvailableAmmo();
                break;
        }
    }
    
    protected void OnEnable()
    {
        this.MMEventStartListening();
    }

    protected void OnDisable()
    {
        this.MMEventStopListening();
    }

    public override GameObject SpawnProjectile(Vector3 spawnPosition, int projectileIndex, int totalProjectiles,
        bool triggerObjectActivation = true)
    {
       /// we get the next object in the pool and make sure it's not null
            GameObject nextGameObject = ObjectPooler.GetPooledGameObject();

            // mandatory checks
            if (nextGameObject == null) { return null; }
            if (nextGameObject.GetComponent<MMPoolableObject>() == null)
            {
                throw new Exception(gameObject.name + " is trying to spawn objects that don't have a PoolableObject component.");
            }
            // we position the object
            nextGameObject.transform.position = spawnPosition;
            if (_projectileSpawnTransform != null && !useMultipleSpawnPosition)
            {
                Debug.Log("Force");
                nextGameObject.transform.position = _projectileSpawnTransform.position;
            }
            // we set its direction

            Projectile projectile = nextGameObject.GetComponent<Projectile>();
            if (projectile != null)
            {
                projectile.SetWeapon(this);
                if (Owner != null)
                {
                    projectile.SetOwner(Owner.gameObject);
                }
            }
            // we activate the object
            nextGameObject.gameObject.SetActive(true);

            if (projectile != null)
            {
                if (RandomSpread)
                {
                    _randomSpreadDirection.x = UnityEngine.Random.Range(-Spread.x, Spread.x);
                    _randomSpreadDirection.y = UnityEngine.Random.Range(-Spread.y, Spread.y);
                    _randomSpreadDirection.z = UnityEngine.Random.Range(-Spread.z, Spread.z);
                }
                else
                {
                    if (totalProjectiles > 1)
                    {
                        _randomSpreadDirection.x = MMMaths.Remap(projectileIndex, 0, totalProjectiles - 1, -Spread.x, Spread.x);
                        _randomSpreadDirection.y = MMMaths.Remap(projectileIndex, 0, totalProjectiles - 1, -Spread.y, Spread.y);
                        _randomSpreadDirection.z = MMMaths.Remap(projectileIndex, 0, totalProjectiles - 1, -Spread.z, Spread.z);
                    }
                    else
                    {
                        _randomSpreadDirection = Vector3.zero;
                    }
                }

                Quaternion spread = Quaternion.Euler(_randomSpreadDirection);

                if (Owner == null)
                {
                    projectile.SetDirection(spread * transform.forward, transform.rotation, true);
                }
                else
                {
                    if (Owner.CharacterDimension == Character.CharacterDimensions.Type3D)
                    {
                        projectile.SetDirection(spread * transform.forward, transform.rotation, true);
                    }
                    else
                    {
                        Vector3 newDirection = (spread * transform.right) * (Flipped ? -1 : 1);
                        if (Owner.Orientation2D != null)
                        {
                            projectile.SetDirection(newDirection, transform.rotation, Owner.Orientation2D.IsFacingRight);
                        }
                        else
                        {
                            projectile.SetDirection(newDirection, transform.rotation, true);
                        }
                    }
                }                

                if (RotateWeaponOnSpread)
                {
                    this.transform.rotation = this.transform.rotation * spread;
                }
            }

            if (triggerObjectActivation)
            {
                if (nextGameObject.GetComponent<MMPoolableObject>() != null)
                {
                    nextGameObject.GetComponent<MMPoolableObject>().TriggerOnSpawnComplete();
                }
            }
            return (nextGameObject);
    }

    /*protected override void TurnWeaponOn()
    {
        base.TurnWeaponOn();
        if (Owner is ExCharacter exCharacter)
        {
            timeAfterLastUse = 0;
            exCharacter.actionState.ChangeState(CharacterActionState.Shooting);
        }
    }*/

    public override void DetermineSpawnPosition()
    {
        base.DetermineSpawnPosition();
        if (useMultipleSpawnPosition && multipleSpawnPos.Length>0)
        {
            if (curMultipleSpawnPos >= multipleSpawnPos.Length)
            {
                curMultipleSpawnPos = 0;
            }

            SpawnPosition = multipleSpawnPos[curMultipleSpawnPos].position;
            ++curMultipleSpawnPos;
        }
    }

    public override void CaseWeaponUse()
    {
        base.CaseWeaponUse();
        if (Owner is ExCharacter exCharacter)
        {
            timeAfterLastUse = 0;
            exCharacter.actionState.ChangeState(CharacterActionState.Shooting);
        }
    }

    /* public override void TurnWeaponOff()
    {
        base.TurnWeaponOff();
        if (Owner is ExCharacter exCharacter)
        {
            exCharacter.actionState.ChangeState(CharacterActionState.Aiming);
        }
    }*/

    public override void CaseWeaponReloadNeeded()
    {
        base.CaseWeaponReloadNeeded();
        if (Owner is ExCharacter exCharacter)
        {
            exCharacter.actionState.ChangeState(CharacterActionState.Aiming);
        }
    }
}
