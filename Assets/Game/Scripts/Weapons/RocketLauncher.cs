﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;

public class RocketLauncher : ExtendedProjWeapon
{
    [MMInspectorGroup("Properties", true, 2)]
    public float baseDamage;
    public float baseProjectileSpeed;
    public int baseHomingType;
    public float baseRocketRotationSpeed;
    
    protected ObjectProperty damageProperty;
    protected ObjectProperty fireRateProperty;
    protected ObjectProperty clipSizeProperty;
    protected ObjectProperty reloadSpeed;
    protected ObjectProperty projectileSpeed;
    protected ObjectProperty rocketRotationSpeed;
    
     public override void Initialization()
    {
        base.Initialization();
        if (Item == null)
        {
            damageProperty = propertyManager.AddProperty("PhysicalDamage", baseDamage);
            fireRateProperty = propertyManager.AddProperty("FireRate", 1f / TimeBetweenUses);
            clipSizeProperty = propertyManager.AddProperty("ClipSize", MagazineSize);
            reloadSpeed = propertyManager.AddProperty("ReloadSpeed", (1000f) / ReloadTime);
            projectileSpeed = propertyManager.AddProperty("ProjectileSpeed",baseProjectileSpeed);
            rocketRotationSpeed = propertyManager.AddProperty("ProjectileRotationSpeed", baseRocketRotationSpeed);
            clipSizeProperty.RegisterChangeCallback(OnChangeClipSize);
        }
        else
        {
            for (int i = 0; i < Item.Properties.Length; i++)
            {
                var cur = Item.Properties[i];
                propertyManager.AddProperty(cur.PropertyDescription,cur.BaseValue,cur.CurValue);
            }
            damageProperty = propertyManager.GetPropertyById(4);
            fireRateProperty = propertyManager.GetPropertyById(6);
            clipSizeProperty = propertyManager.GetPropertyById(9);
            reloadSpeed = propertyManager.GetPropertyById(10);
            projectileSpeed = propertyManager.GetPropertyById(18);
            rocketRotationSpeed = propertyManager.GetPropertyById(19);


            ///Инициализация соотвествующих параметров
            TimeBetweenUses = 1f /fireRateProperty.GetCurValue();
            MagazineSize = Mathf.RoundToInt(clipSizeProperty.Value);
            //MagazineSize = Mathf.FloorToInt(magazineCapacity.Value/ammoSize.Value);
            CurrentAmmoLoaded = Mathf.Min(CurrentAmmoLoaded, MagazineSize);
            ReloadTime = (1000f) / reloadSpeed.GetCurValue();
            //magazineCapacity.RegisterChangeCallback(OnChangeMagazineCapacity);
            clipSizeProperty.RegisterChangeCallback(OnChangeClipSize);
        }
        fireRateProperty.RegisterChangeCallback(OnChangeFireRate);
        reloadSpeed.RegisterChangeCallback(OnChangeReloadSpeed);


        if (Item != null)
        {
            if (Item.AmmoCount < 0)
            {
                CurrentAmmoLoaded = MagazineSize;
            }
            else
            {
                CurrentAmmoLoaded = Item.AmmoCount;
            }
            
            Item.CalculateModuleBonuses();
            propertyManager.SetEquipmentBonuses(Item.EquipmentType,Item.ModuleBonuses);
        }
    }
     
     
     protected override void Update()
     {
         base.Update();
         
         if(Item!=null)
             Item.AmmoCount = CurrentAmmoLoaded;
     }
     
     
     public override GameObject SpawnProjectile(Vector3 spawnPosition, int projectileIndex, int totalProjectiles,
         bool triggerObjectActivation = true)
     {
         GameObject proj=base.SpawnProjectile(spawnPosition, projectileIndex, totalProjectiles, triggerObjectActivation);
         proj.GetComponent<Projectile>().Speed = projectileSpeed.GetCurValue();
         EffectOnTouch effectOnTouch = proj.GetComponent<EffectOnTouch>();
         //Effect dmg=new DamageEffect(damageProperty.GetCurValue());
         Effect dmg=new Effect("PhysicalDamage",propertyManager);
         //Effect poison=new PoisoningEffect(5,3,1f);
         Effect chemicalBurn=new Effect("ChemicalBurn",propertyManager);
         effectOnTouch.AddEffect(dmg);
         effectOnTouch.AddEffect(chemicalBurn);

         HomingMissile missile = proj.GetComponent<HomingMissile>();
         missile.RotationSpeed = rocketRotationSpeed.GetCurValue();

         ExplosiveMissile explosiveMissile = proj.GetComponent<ExplosiveMissile>();
         explosiveMissile.AddEffect(dmg);

         return proj;
     }
     
     protected virtual void OnChangeFireRate(float oldCurValue,float newCurValue,float oldValue,float newValue)
     {
         TimeBetweenUses = 1f / newCurValue;
     }
    
     protected virtual void OnChangeClipSize(float oldCurValue,float newCurValue,float oldValue,float newValue)
     {
         MagazineSize = Mathf.FloorToInt(newCurValue);
         CurrentAmmoLoaded = Mathf.Min(CurrentAmmoLoaded, MagazineSize);
     }
    
     protected virtual void OnChangeReloadSpeed(float oldCurValue,float newCurValue,float oldValue,float newValue)
     {
         ReloadTime = (1000f) / newCurValue;
     }

}
