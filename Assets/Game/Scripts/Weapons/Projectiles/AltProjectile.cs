﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;
/// <summary>
/// Альтернативный скрипт для летящих снарядов
/// Пулинг пока не поддерживает, а может поддерживать никогда и не будет
/// </summary>
public class AltProjectile : MonoBehaviour
{
    protected Rigidbody2D rigidbody;
    protected Health health;
    [Tooltip("Средняя скорость с которой будет лететь снаряд")]
    [SerializeField]
    protected float awerageSpeed;
    [Header("Target position using")]
    [Tooltip("Следует ли рассчитывать максимальное возможное расстояние на основе точки, которую направлена пуля")]
    [SerializeField] protected bool useTargetPositionToLimitRange;
    [Tooltip("На сколько увеличить или уменьшить рассчитанное расстояние")]
    [SerializeField] protected float additionalDistance;
    [SerializeField] protected LayerMask bounceMask;
    [Tooltip("Количество рикошетов")]
    [SerializeField] protected int bounceCount;
    /// <summary>
    /// Точка, в которую был напрален прицел игрока во время выстрела
    /// </summary>
    protected Vector2 targetPosition;
    /// <summary>
    /// Расстояние от стартовой позиции до целевой позиции
    /// </summary>
    protected float targetPositionDistance;
    /// <summary>
    /// Ожидаемое время до прибытия в целевую позицию со старта
    /// </summary>
    protected float expectedTime;
    /// <summary>
    /// Пройденное расстояние
    /// </summary>
    protected float traveledDistance;
    /// <summary>
    /// Текущее направление движения
    /// </summary>
    protected Vector2 direction;
    protected Vector2 lastFramePosition;
    protected Vector2 raycastDirection;

    protected bool shouldMove;
    /// <summary>
    /// Количество оставшихся отскакиваний
    /// </summary>
    protected int _bouncesLeft;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        health = GetComponent<Health>();
        direction = transform.right.normalized;
        shouldMove = true;
        SetTargetPosition(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        traveledDistance = 0;
        lastFramePosition = rigidbody.position;
        _bouncesLeft = bounceCount;
        //Invoke("DestroySelf",expectedTime);
    }

    void Start()
    {
        //rigidbody.velocity = transform.right * awerageSpeed;
        //rigidbody.MovePosition(playerTargetPosition);
    }
    
    void Update()
    {
        //traveledDistance += Vector2.Distance(lastFramePosition, transform.position);
        //if(traveledDistance>=targetPositionDistance) DestroySelf();
        //lastFramePosition = transform.position;
    }
    
    public void SetTargetPosition(Vector2 newTargetPosition, Vector2 newDirection = default)
    {
        if(newDirection != default)
            direction = newDirection.normalized;
        transform.right = newDirection.normalized;
        targetPosition = newTargetPosition;
        targetPositionDistance = Vector2.Distance(targetPosition, rigidbody.position);
        expectedTime = targetPositionDistance / awerageSpeed;
    }

    private void FixedUpdate()
    {
        if (shouldMove)
        {
            rigidbody.MovePosition((Vector2) transform.position + direction * awerageSpeed * Time.fixedDeltaTime);
            traveledDistance += Vector2.Distance(lastFramePosition, rigidbody.position);
            if (useTargetPositionToLimitRange)
            {
                if (traveledDistance >= targetPositionDistance+additionalDistance) DestroySelf();
            }
            lastFramePosition = rigidbody.position;
        }
    }

    private void LateUpdate()
    {
        
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        raycastDirection = (Vector2)transform.position - lastFramePosition;
        RaycastHit2D hit = MMDebug.RayCast(lastFramePosition, direction.normalized, 2, bounceMask, MMColors.DarkOrange, true);
        //Debug.Log("Trigger");
        if (hit.collider!=null)
        {
            //Debug.Log("Hit "+hit.collider.name);
            if (_bouncesLeft > 0)
            {
                //Debug.Log(hit.normal);
                Debug.DrawLine(hit.point,hit.point+hit.normal,Color.magenta,5);
                var reflectedDirection = Vector3.Reflect(raycastDirection, hit.normal);
                direction = reflectedDirection.normalized;
                transform.right = direction;
                _bouncesLeft--;
            }
            else
            {
                health.Kill();
            }
        }
    }


    protected void OnDeath()
    {
        rigidbody.velocity=Vector2.zero;
        shouldMove = false;
    }

    protected void DestroySelf()
    {
        health.Kill();
    }

    private void OnEnable()
    {
        if (health != null)
        {
            health.OnDeath += OnDeath;
        }
    }

    private void OnDisable()
    {
        if (health != null)
        {
            health.OnDeath -= OnDeath;
        }
    }
}
