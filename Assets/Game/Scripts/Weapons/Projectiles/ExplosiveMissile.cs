﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Feedbacks;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;

public class ExplosiveMissile : MonoBehaviour
{
   [SerializeField]
   protected float explosionRadius;
   [Tooltip("Урон по объектам, которые имеют Health но не имеют PropertyManager в том числе пулям")]
   [SerializeField] protected int nonPropertyDamage=100;

   [Tooltip("Объекты каких слоев будут задеты взрывом")]
   [SerializeField] 
   protected LayerMask explosionMask;
   [SerializeField]
   protected MMFeedbacks explodeFeedback;
   
   /// <summary>
   /// список эффектов, которыебудут наложены при взрыве
   /// </summary>
   protected List<Effect> explosionEffects;

   protected Health health;
   private void Awake()
   {
      explosionEffects=new List<Effect>();
      health=GetComponent<Health>();
      health.OnDeath += Explode;
   }

   /// <summary>
   /// Функция подрыва
   /// </summary>
   public void Detonate()
   {
      health.Kill();
   }
   /// <summary>
   /// Функция взрыва
   /// </summary>
   protected void Explode()
   {
      explodeFeedback?.PlayFeedbacks(transform.position);
      var colliders = Physics2D.OverlapCircleAll(transform.position,explosionRadius,explosionMask);
      MMDebug.DrawPoint(transform.position,Color.red,explosionRadius);

      foreach (var collider in colliders)
      {
         if (collider.attachedRigidbody != null)
         {
            var propertyManager = collider.attachedRigidbody.GetComponent<PropertyManager>();
            var h = collider.attachedRigidbody.GetComponent<Health>();
            if (propertyManager != null)
            {
               for (int i = 0; i < explosionEffects.Count; i++)
               {
                  propertyManager.AddEffect(explosionEffects[i]);
               }
            }
            else if(h!=null)
            {
               h.Damage(nonPropertyDamage,null,0,0,Vector3.zero);
            }
         }
      }

   }

   public void AddEffect(Effect effect)
   {
      explosionEffects.Add(effect);
      Debug.Log(effect.getImpacts()[0].Value);
   }

   private void OnDisable()
   {
      explosionEffects.Clear();
   }

  
}
