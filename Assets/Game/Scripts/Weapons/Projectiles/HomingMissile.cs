﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;

[RequireComponent(typeof(Projectile))]
public class HomingMissile : MonoBehaviour
{
    
    [SerializeField]
    protected HomingType homingType;
    [SerializeField]
    protected LayerMask detectionMask;
    /// <summary>
    /// Скорость поворота снаряда
    /// </summary>
    [SerializeField]
    protected float rotationSpeed=200f;
    [SerializeField]
    protected float detectionRange=20;
    [Tooltip("Расстояние после прохождения которого наведение ракеты сбивается")]
    [SerializeField]
    protected float maxDistance=50;

    protected float traveledDistance = 0;

    protected Vector2 lastPosition;

    protected HomingState state=HomingState.Detection;

    protected Projectile projectile;
    [MMReadOnly]
    public Transform target;

    protected MMConeOfVision2D coneOfVision;

    /// <summary>
    /// Скорость поворота снаряда
    /// </summary>
    public float RotationSpeed
    {
        get => rotationSpeed;
        set => rotationSpeed = value;
    }

    private void Awake()
    {
        projectile = GetComponent<Projectile>();
        coneOfVision = GetComponent<MMConeOfVision2D>();
        lastPosition = transform.position;
    }

    private void FixedUpdate()
    {
        if (state == HomingState.Detection)
        {
            Detection();
        }
        else if(state==HomingState.Pursuit)
        {
            Pursuit();
        }
        
        traveledDistance += Vector2.Distance(transform.position,lastPosition);
        lastPosition = transform.position;
        if (traveledDistance > maxDistance)
        {
            KnockDownHoming();
        }
    }

    protected void Detection()
    {
        if (homingType == HomingType.LaserDetection)
        {
            var hit = Physics2D.Raycast(transform.position,projectile.Direction,detectionRange,detectionMask);
            if (hit.collider != null && hit.collider.attachedRigidbody!=null 
                                     && hit.collider.attachedRigidbody.GetComponent<ExtendedHealth>()!=null)
            {
                SetTarget(hit.collider.attachedRigidbody.transform);
            }
        }
        else if(homingType==HomingType.RangeDetection)
        {
            var colliders = Physics2D.OverlapCircleAll(transform.position,detectionRange,detectionMask);
            int minInd = findClosestCollider(colliders);
            if (minInd != -1)
            {
                SetTarget(colliders[minInd].attachedRigidbody.transform);
            }

        }
        else if(homingType==HomingType.ConeOfVision)
        {
            var objs = coneOfVision.VisibleTargets;
            //Debug.Log(objs.Count);
            var colliders = objs.Select(x => x.GetComponent<Collider2D>()).ToArray();
            int minInd = findClosestCollider(colliders.ToArray());
            if (minInd != -1)
            {
                SetTarget(colliders[minInd].attachedRigidbody.transform);
            }
        }
    }

    protected int findClosestCollider(Collider2D[] colliders)
    {
        int minInd = -1;
        float min = float.MaxValue; 
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].attachedRigidbody!=null
                &&colliders[i].attachedRigidbody.GetComponent<ExtendedHealth>() != null)
            {
                var dist = Vector2.SqrMagnitude(colliders[i].attachedRigidbody.position -
                                                (Vector2) transform.position);
                if (dist < min)
                {
                    min = dist;
                    minInd = i;
                }
            }
        }

        return minInd;
    }

    protected void Pursuit()
    {
        Vector2 direction = projectile.Direction;
        var rotation = transform.rotation.eulerAngles;
        var rot = Vector2.SignedAngle(direction,target.position-transform.position);
        var angleDelta = Math.Sign(rot) * rotationSpeed * Time.deltaTime;
        direction = direction.MMRotate(angleDelta);
        projectile.SetDirection(direction,
            Quaternion.Euler(rotation.MMSetZ(rotation.z+angleDelta)));
    }

    public void KnockDownHoming()
    {
        state = HomingState.OutOfHoming;
        target = null;
    }

    public void SetTarget(Transform t)
    {
        target = t;
        state = HomingState.Pursuit;
    }

    private void OnDisable()
    {
        state = HomingState.Detection;
        traveledDistance = 0;
    }

    private void OnEnable()
    {
        lastPosition = transform.position;
    }
}
/// <summary>
/// Тип самонаведения
/// NoHoming - ракета не наводится а летит, как обычная пуля
/// LaserDetection - ракета рэйкастом проверяет подходящие цели перед собой
/// RangeDetection - ракета проверяет все цели в определенном радиусе от себя
/// </summary>
public enum HomingType
{
    NoHoming,LaserDetection,RangeDetection,ConeOfVision
}
/// <summary>
/// Состояние наведения ракеты
/// Detection - ракета ищет цель
/// Pursuit - ракета преследует уже найденную цель
/// OutOfHoming - наведение ракеты сбито и она уже не может его взять (например кончилось топливо или сработало ЭМИ)
/// </summary>
public enum HomingState
{
    Detection, Pursuit, OutOfHoming
}
