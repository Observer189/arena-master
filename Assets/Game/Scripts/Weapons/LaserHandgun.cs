﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using UnityEngine;

public class LaserHandgun : LaserWeapon
{
    [MMInspectorGroup("Properties", true, 2)]
    public float baseDamage;
    public float baseShieldDamage;
    
    protected ObjectProperty damageProperty;
    protected ObjectProperty fireRateProperty;
    protected ObjectProperty clipSizeProperty;
    protected ObjectProperty reloadSpeed;
    protected ObjectProperty shieldDamage;
    protected ObjectProperty projPerShot;
    protected ObjectProperty bounce;
    protected ObjectProperty projectileSpeed;
    
     public override void Initialization()
    {
        base.Initialization();
        if (Item == null)
        {
            damageProperty = PropertyManager.AddProperty("EnergyDamage", baseDamage);
            fireRateProperty = PropertyManager.AddProperty("FireRate", 1f / TimeBetweenUses);
            clipSizeProperty = PropertyManager.AddProperty("ClipSize", MagazineSize);
            reloadSpeed = PropertyManager.AddProperty("ReloadSpeed", (1000f) / ReloadTime);
            shieldDamage = PropertyManager.AddProperty("ShieldDamage", baseShieldDamage);
            projPerShot = PropertyManager.AddProperty("ProjectilesPerShot", raysCount);
            bounce = PropertyManager.AddProperty("Bounce", bounceCount);
            clipSizeProperty.RegisterChangeCallback(OnChangeClipSize);
        }
        else
        {
            for (int i = 0; i < Item.Properties.Length; i++)
            {
                var cur = Item.Properties[i];
                propertyManager.AddProperty(cur.PropertyDescription,cur.BaseValue,cur.CurValue);
            }
            damageProperty = propertyManager.GetPropertyById(4);
            fireRateProperty = propertyManager.GetPropertyById(6);
            clipSizeProperty = propertyManager.GetPropertyById(9);
            reloadSpeed = propertyManager.GetPropertyById(10);
            shieldDamage = propertyManager.GetPropertyById(31);
            projPerShot = propertyManager.GetPropertyById(11);
            bounce = propertyManager.GetPropertyById(32);


            ///Инициализация соотвествующих параметров
            TimeBetweenUses = 1f /fireRateProperty.GetCurValue();
            MagazineSize = Mathf.RoundToInt(clipSizeProperty.Value);
            //MagazineSize = Mathf.FloorToInt(magazineCapacity.Value/ammoSize.Value);
            CurrentAmmoLoaded = Mathf.Min(CurrentAmmoLoaded, MagazineSize);
            ReloadTime = (1000f) / reloadSpeed.GetCurValue();
            damageToShield = shieldDamage.GetCurValue();
            raysCount = Mathf.RoundToInt(projPerShot.GetCurValue());
            bounceCount = Mathf.RoundToInt(bounce.GetCurValue());
            //magazineCapacity.RegisterChangeCallback(OnChangeMagazineCapacity);
            clipSizeProperty.RegisterChangeCallback(OnChangeClipSize);
        }
        fireRateProperty.RegisterChangeCallback(OnChangeFireRate);
        reloadSpeed.RegisterChangeCallback(OnChangeReloadSpeed);
        shieldDamage.RegisterChangeCallback(OnChangeShieldDamage);
        projPerShot.RegisterChangeCallback(OnChangeProjPerShot);
        bounce.RegisterChangeCallback(OnChangeBounce);
        
        if (Item != null)
        {
            if (Item.AmmoCount < 0)
            {
                CurrentAmmoLoaded = MagazineSize;
            }
            else
            {
                CurrentAmmoLoaded = Item.AmmoCount;
            }
            
            Item.CalculateModuleBonuses();
            propertyManager.SetEquipmentBonuses(Item.EquipmentType,Item.ModuleBonuses);
        }
    }
     
     protected virtual void OnChangeFireRate(float oldCurValue,float newCurValue,float oldValue,float newValue)
     {
         TimeBetweenUses = 1f / newCurValue;
     }
    
     protected virtual void OnChangeClipSize(float oldCurValue,float newCurValue,float oldValue,float newValue)
     {
         MagazineSize = Mathf.FloorToInt(newCurValue);
         CurrentAmmoLoaded = Mathf.Min(CurrentAmmoLoaded, MagazineSize);
     }
    
     protected virtual void OnChangeReloadSpeed(float oldCurValue,float newCurValue,float oldValue,float newValue)
     {
         ReloadTime = (1000f) / newCurValue;
     }
     
     protected virtual void OnChangeShieldDamage(float oldCurValue,float newCurValue,float oldValue,float newValue)
     {
         damageToShield = newCurValue;
     }
     
     protected virtual void OnChangeProjPerShot(float oldCurValue,float newCurValue,float oldValue,float newValue)
     {
         raysCount = Mathf.RoundToInt(newCurValue);
     }
     
     protected virtual void OnChangeBounce(float oldCurValue,float newCurValue,float oldValue,float newValue)
     {
         bounceCount = Mathf.RoundToInt(newCurValue);
     }
    
}
