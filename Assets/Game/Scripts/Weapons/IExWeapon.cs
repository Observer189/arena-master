﻿
    using UnityEngine;

    public interface IExWeapon
    {
        PropertyManager PropertyManager
        {
            get;
        }

        WeaponItem Item
        {
            get;
            set;
        }

        Sprite DisplayImage
        {
            get;
        }

        WeaponAnimationType WeaponAnimationType { get; }

        bool NeedAimToShoot { get; }

    }

    public enum WeaponAnimationType
    {
        Barehanded, Melee, Handgun, Rifle,Other
    }