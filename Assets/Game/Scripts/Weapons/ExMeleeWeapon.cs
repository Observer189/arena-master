﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;

public class ExMeleeWeapon : MeleeWeapon, IExWeapon
{
    protected PropertyManager propertyManager;
    
    public PropertyManager PropertyManager { get; }

    public Sprite DisplayImage => displayImage;
    public WeaponItem Item { get; set; }

    public WeaponAnimationType WeaponAnimationType => weaponAnimationType;

    protected EffectOnTouch _effectOnTouch;
    [MMInspectorGroup("Additional animation", true, 10)]
    [SerializeField]
    protected bool UseOwnerAnimator=false;
    
    [SerializeField]
    protected string ExShootingAnimationParameter="ExShooting";

    [Tooltip("Время после выстрела в течении, которого считается, что персонаж находится в состоянии стрельбы")]
    [SerializeField]
    protected float afterUseShootingStateDuration = 0.1f;
    
    [SerializeField]
    protected Sprite displayImage;
    
    [SerializeField] 
    protected WeaponAnimationType weaponAnimationType;
    public bool NeedAimToShoot => false;
    
    
    protected float timeAfterLastUse;

    protected int _exShootingAnimationParameter;

    public override void Initialization()
    {
        base.Initialization();
        propertyManager = Owner.GetComponent<PropertyManager>();
        if (propertyManager == null)//гарантия того что объекты с компонентом имеют PropertyManager
        {
            propertyManager = gameObject.AddComponent<PropertyManager>();
        }
    }

    protected override void AddParametersToAnimator(Animator animator, HashSet<int> list)
    {
        base.AddParametersToAnimator(animator, list);
        MMAnimatorExtensions.AddAnimatorParameterIfExists(animator, ExShootingAnimationParameter, out _exShootingAnimationParameter, AnimatorControllerParameterType.Bool, list);
    }
    
    protected override void UpdateAnimator(Animator animator, HashSet<int> list)
    {
        base.UpdateAnimator(animator, list);
        if(Owner is ExCharacter exCharacter)
            MMAnimatorExtensions.UpdateAnimatorBool(animator, _exShootingAnimationParameter,exCharacter.actionState.CurrentState==CharacterActionState.Shooting , list);
    }
    
    public override void CaseWeaponUse()
    {
        base.CaseWeaponUse();
        if (Owner is ExCharacter exCharacter)
        {
            timeAfterLastUse = 0;
            exCharacter.actionState.ChangeState(CharacterActionState.Shooting);
        }
    }
    
    protected override void Update()
    {
        base.Update();
        if (Owner is ExCharacter exCharacter)
        {
            timeAfterLastUse += Time.deltaTime;
            if (timeAfterLastUse > afterUseShootingStateDuration && exCharacter.actionState.CurrentState==CharacterActionState.Shooting)
            {
                exCharacter.actionState.ChangeState(CharacterActionState.Idle);
            }
        }
        
        if(Item!=null)
            Item.AmmoCount = CurrentAmmoLoaded;
    }

    protected override void CreateDamageArea()
    {
        _damageArea = new GameObject();
            _damageArea.name = this.name + "DamageArea";
            _damageArea.transform.position = this.transform.position;
            _damageArea.transform.rotation = this.transform.rotation;
            _damageArea.transform.SetParent(this.transform);
            _damageArea.layer = this.gameObject.layer;
            
            if (DamageAreaShape == MeleeDamageAreaShapes.Rectangle)
            {
                _boxCollider2D = _damageArea.AddComponent<BoxCollider2D>();
                _boxCollider2D.offset = AreaOffset;
                _boxCollider2D.size = AreaSize;
                _damageAreaCollider2D = _boxCollider2D;
                _damageAreaCollider2D.isTrigger = true;
            }
            if (DamageAreaShape == MeleeDamageAreaShapes.Circle)
            {
                _circleCollider2D = _damageArea.AddComponent<CircleCollider2D>();
                _circleCollider2D.transform.position = this.transform.position + this.transform.rotation * AreaOffset;
                _circleCollider2D.radius = AreaSize.x / 2;
                _damageAreaCollider2D = _circleCollider2D;
                _damageAreaCollider2D.isTrigger = true;
            }

            if ((DamageAreaShape == MeleeDamageAreaShapes.Rectangle) || (DamageAreaShape == MeleeDamageAreaShapes.Circle))
            {
                Rigidbody2D rigidBody = _damageArea.AddComponent<Rigidbody2D>();
                rigidBody.isKinematic = true;
                rigidBody.sleepMode = RigidbodySleepMode2D.NeverSleep;
            }            

            if (DamageAreaShape == MeleeDamageAreaShapes.Box)
            {
                _boxCollider = _damageArea.AddComponent<BoxCollider>();
                _boxCollider.center = AreaOffset;
                _boxCollider.size = AreaSize;
                _damageAreaCollider = _boxCollider;
                _damageAreaCollider.isTrigger = true;
            }
            if (DamageAreaShape == MeleeDamageAreaShapes.Sphere)
            {
                _sphereCollider = _damageArea.AddComponent<SphereCollider>();
                _sphereCollider.transform.position = this.transform.position + this.transform.rotation * AreaOffset;
                _sphereCollider.radius = AreaSize.x / 2;
                _damageAreaCollider = _sphereCollider;
                _damageAreaCollider.isTrigger = true;
            }

            if ((DamageAreaShape == MeleeDamageAreaShapes.Box) || (DamageAreaShape == MeleeDamageAreaShapes.Sphere))
            {
                Rigidbody rigidBody = _damageArea.AddComponent<Rigidbody>();
                rigidBody.isKinematic = true;
            }
            ///Вместо damageOnTouch создаем EffectOnTouch
            _damageOnTouch = _damageArea.AddComponent<EffectOnTouch>();
            _damageOnTouch.SetGizmoSize(AreaSize);
            _damageOnTouch.SetGizmoOffset(AreaOffset);
            _damageOnTouch.TargetLayerMask = TargetLayerMask;
            _damageOnTouch.DamageCaused = DamageCaused;
            _damageOnTouch.DamageCausedKnockbackType = Knockback;
            _damageOnTouch.DamageCausedKnockbackForce = KnockbackForce;
            _damageOnTouch.InvincibilityDuration = InvincibilityDuration;
            _damageOnTouch.HitDamageableFeedback = HitDamageableFeedback;
            _damageOnTouch.HitNonDamageableFeedback = HitNonDamageableFeedback;
            _effectOnTouch = (EffectOnTouch) _damageOnTouch;


            if (!CanDamageOwner && (Owner != null))
            {
                _damageOnTouch.IgnoreGameObject(Owner.gameObject);    
            }
    }
    /// <summary>
    /// Устанавливаем эффекты при активации зоны урона
    /// </summary>
    protected override void EnableDamageArea()
    {
        base.EnableDamageArea();
        OnInitializeEffectOnTouch(_effectOnTouch);
    }
    /// <summary>
    /// Функция в которой надо устанавливать эффекты оружия
    /// </summary>
    /// <param name="effectOnTouch"></param>
    protected virtual void OnInitializeEffectOnTouch(EffectOnTouch effectOnTouch)
    {
        
    }
    /// <summary>
    /// Очищаем эффекты, чтобы они не стакались
    /// </summary>
    protected override void DisableDamageArea()
    {
        base.DisableDamageArea();
        _effectOnTouch.ClearEffects();
    }
    
}
