﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;

public class PlasmaGun : ExtendedProjWeapon
{
    [MMInspectorGroup("Properties", true, 2)]
    public float baseDamage;
    //public float baseRecoilControl;
    public float piercing;
    public float baseProjSpeed;
    [Tooltip("Максимальный угол разброса(при точности равной 0)")]
    [Range(1,180)]
    [SerializeField]
    private float maxSpreadAngle;
    
    
    protected ObjectProperty damageProperty;
    protected ObjectProperty fireRateProperty;
    protected ObjectProperty clipSizeProperty;
    protected ObjectProperty reloadSpeed;
    protected ObjectProperty projectileCount;
    protected ObjectProperty accuracyProperty;
    protected ObjectProperty projectileSpeed;
    //protected ObjectProperty recoilControl;
    protected ObjectProperty pierceCount;
    protected ObjectProperty ammoSize;
    protected ObjectProperty magazineCapacity;

    public override void Initialization()
    {
        base.Initialization();
        if (Item == null)
        {
            damageProperty = propertyManager.AddProperty("PlasmaDamage", baseDamage);
            fireRateProperty = propertyManager.AddProperty("FireRate", 1f / TimeBetweenUses);
            clipSizeProperty = propertyManager.AddProperty("ClipSize", MagazineSize);
            reloadSpeed = propertyManager.AddProperty("ReloadSpeed", (1000f) / ReloadTime);
            projectileCount = propertyManager.AddProperty("ProjectilesPerShot", ProjectilesPerShot);
            accuracyProperty = propertyManager.AddProperty("Accuracy", AccuracyConverter.SpreadToAccuracyParabolical(Spread.z));
            //recoilControl = propertyManager.AddProperty("RecoilControl", baseRecoilControl);
            projectileSpeed = propertyManager.AddProperty("ProjectileSpeed",baseProjSpeed);
            pierceCount = propertyManager.AddProperty("PierceCount", piercing);
            clipSizeProperty.RegisterChangeCallback(OnChangeClipSize);
        }
        else
        {
            for (int i = 0; i < Item.Properties.Length; i++)
            {
                var cur = Item.Properties[i];
                propertyManager.AddProperty(cur.PropertyDescription,cur.BaseValue,cur.CurValue);
            }
            damageProperty = propertyManager.GetPropertyById(29);
            fireRateProperty = propertyManager.GetPropertyById(6);
            clipSizeProperty = propertyManager.GetPropertyById(9);
            reloadSpeed = propertyManager.GetPropertyById(10);
            projectileCount = propertyManager.GetPropertyById(11);
            accuracyProperty = propertyManager.GetPropertyById(12);
            projectileSpeed = propertyManager.GetPropertyById(18);
            //recoilControl= propertyManager.GetPropertyById(13);
            pierceCount = propertyManager.GetPropertyById(7);
            magazineCapacity = propertyManager.GetPropertyById(15);
            ammoSize = propertyManager.GetPropertyById(16);
            

            ///Инициализация соотвествующих параметров
            ProjectilesPerShot = (int) projectileCount.GetCurValue();
            TimeBetweenUses = 1f /fireRateProperty.GetCurValue();
            MagazineSize = Mathf.RoundToInt(clipSizeProperty.Value);
            //MagazineSize = Mathf.FloorToInt(magazineCapacity.Value/ammoSize.Value);
            CurrentAmmoLoaded = Mathf.Min(CurrentAmmoLoaded, MagazineSize);
            ReloadTime = (1000f) / reloadSpeed.GetCurValue();
            float newSpread=(accuracyProperty.Value>=100)?100:accuracyProperty.Value;
            Spread = new Vector3(Spread.x,Spread.y,AccuracyConverter.AccuracyToSpreadParabolical(newSpread));
            ammoSize.RegisterChangeCallback(OnChangeAmmoSize);
            //magazineCapacity.RegisterChangeCallback(OnChangeMagazineCapacity);
            clipSizeProperty.RegisterChangeCallback(OnChangeClipSize);
            
            AmmoConsumedPerShot = Mathf.RoundToInt(ammoSize.GetCurValue());
        }
        fireRateProperty.RegisterChangeCallback(OnChangeFireRate);
        reloadSpeed.RegisterChangeCallback(OnChangeReloadSpeed);
        projectileCount.RegisterChangeCallback(OnChangeProjectileCount);
        accuracyProperty.RegisterChangeCallback(OnChangeAccuracy);
        

        if (Item != null)
        {
            if (Item.AmmoCount < 0)
            {
                CurrentAmmoLoaded = MagazineSize;
            }
            else
            {
                CurrentAmmoLoaded = Item.AmmoCount;
            }
            
            Item.CalculateModuleBonuses();
            propertyManager.SetEquipmentBonuses(Item.EquipmentType,Item.ModuleBonuses);
        }
    }

    public override void WeaponUse()
    {
        base.WeaponUse();
        //Debug.Log(recoilControl.GetCurValue());
        //accuracyProperty.ChangeCurValue(-(Consts.baseRecoil-(Consts.baseRecoil*recoilControl.GetCurValue())));
        //Debug.Log(accuracyProperty.GetCurValue());
    }

    public override GameObject SpawnProjectile(Vector3 spawnPosition, int projectileIndex, int totalProjectiles,
        bool triggerObjectActivation = true)
    {
        GameObject proj=base.SpawnProjectile(spawnPosition, projectileIndex, totalProjectiles, triggerObjectActivation);
        EffectOnTouch effectOnTouch = proj.GetComponent<EffectOnTouch>();
        //Effect dmg=new DamageEffect(damageProperty.GetCurValue());
        Effect dmg=new Effect("PlasmaChargeEffect",propertyManager);
        //Effect poison=new PoisoningEffect(5,3,1f);
        Effect chemicalBurn=new Effect("ChemicalBurn",propertyManager);
        effectOnTouch.AddEffect(dmg);
        effectOnTouch.AddEffect(chemicalBurn);
        proj.GetComponent<Health>().GetHealth(Mathf.FloorToInt(pierceCount.GetCurValue()),gameObject);
        proj.GetComponent<Projectile>().Speed = projectileSpeed.GetCurValue();
        
        return proj;
    }

    protected override void Update()
    {
        base.Update();
        //accuracyProperty.ChangeCurValue(((accuracyProperty.Value-accuracyProperty.GetCurValue())*Consts.SpreadReducingCoef+Consts.SpreadReducingBaseSpeed)
        //                                *Time.deltaTime);
    }

    private void OnDrawGizmos()
    {
        Vector3 dir = transform.right;
        var angle = Mathf.Deg2Rad*Spread.z;
        var x = dir.x * Mathf.Cos(angle) - dir.y * Mathf.Sin(angle);
        var y = dir.x * Mathf.Sin(angle) + dir.y * Mathf.Cos(angle);
        dir=new Vector3(x,y);
        var sp = GetSpawnPosition();
        Gizmos.DrawRay(sp,dir*100);
        dir = transform.right;
        angle = -Mathf.Deg2Rad*Spread.z;
        x = dir.x * Mathf.Cos(angle) - dir.y * Mathf.Sin(angle);
        y = dir.x * Mathf.Sin(angle) + dir.y * Mathf.Cos(angle);
        dir=new Vector3(x,y);
        Gizmos.DrawRay(sp,dir*100);
    }
    

    protected virtual void OnChangeFireRate(float oldCurValue,float newCurValue,float oldValue,float newValue)
    {
        TimeBetweenUses = 1f / newCurValue;
    }
    
   protected virtual void OnChangeClipSize(float oldCurValue,float newCurValue,float oldValue,float newValue)
    {
        MagazineSize = Mathf.FloorToInt(newCurValue);
        CurrentAmmoLoaded = Mathf.Min(CurrentAmmoLoaded, MagazineSize);
    }
    
    protected virtual void OnChangeReloadSpeed(float oldCurValue,float newCurValue,float oldValue,float newValue)
    {
        ReloadTime = (1000f) / newCurValue;
    }
    
    protected virtual void OnChangeProjectileCount(float oldCurValue,float newCurValue,float oldValue,float newValue)
    {
        ProjectilesPerShot = Mathf.FloorToInt(newCurValue);
    }

    protected virtual void OnChangeAccuracy(float oldCurValue, float newCurValue, float oldValue, float newValue)
    {
        float newSpread=(newCurValue>=100)?100:newValue;
        Spread = new Vector3(Spread.x,Spread.y,AccuracyConverter.AccuracyToSpreadParabolical(newSpread));
       // Debug.Log($"Spread = {Spread}");
       // Debug.Log(AccuracyToSpreadParabolical(SpreadToAccuracyParabolical(Spread.z)));
    }

    protected virtual void OnChangeMagazineCapacity(float oldCurValue, float newCurValue, float oldValue, float newValue)
    {
        MagazineSize = Mathf.FloorToInt(newValue / ammoSize.Value);
        CurrentAmmoLoaded = MagazineSize;
    }
    
    protected virtual void OnChangeAmmoSize(float oldCurValue, float newCurValue, float oldValue, float newValue)
    {
        //MagazineSize = Mathf.FloorToInt(magazineCapacity.Value/newValue);
        //CurrentAmmoLoaded = MagazineSize;
        AmmoConsumedPerShot = Mathf.RoundToInt(newValue);
    }
    
    protected float SpreadToAccuracy(float spread)
    {
        if (spread > 180) spread = 180;
        return 100-Mathf.Sqrt((180 -(180- spread)) / 0.018f);
    }

    protected float AccuracyToSpread(float accuracy)
    {
        if (accuracy > 100) accuracy = 100;
        return 180-(-0.018f * Mathf.Pow(100-accuracy,2) + 180);
    }
    

}
