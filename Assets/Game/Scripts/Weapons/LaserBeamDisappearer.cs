﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBeamDisappearer : MonoBehaviour
{
    private LineRenderer renderer;

    private float beamDisappearTime;

    private bool isDisappearInitialized;
    private void Awake()
    {
        renderer = GetComponent<LineRenderer>();
    }

    public void InitializeDisappear (float beamDisappearTime)
    {
        this.beamDisappearTime = beamDisappearTime;
        isDisappearInitialized = true;
    }

    private void Update()
    {
        if (isDisappearInitialized)
        {
            ProceedDisappear();
        }
    }

    private void ProceedDisappear()
    {
        var sc = renderer.startColor;
        var alpha = sc.a;
        if (alpha > 0)
        {
            var newAlpha = alpha - Time.deltaTime/beamDisappearTime;
            Color c1= new Color(sc.r,sc.g,sc.b,newAlpha/*255*Time.deltaTime/beamDisappearTime*/);
            Color c2=new Color(renderer.endColor.r,renderer.endColor.g,renderer.endColor.b,newAlpha);
            renderer.startColor = c1;
            renderer.endColor = c2;
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
