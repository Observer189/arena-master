﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using InventorySystem;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class ExWeaponAim2D : WeaponAim2D
{
    public Transform projectileStartPos;
    [Tooltip("Стоит ли использовать в качетсве точки рассчета начальной позиции использовать эту точку из CharacterHandleWeapon")]
    public bool useChwStartPos;
    [Tooltip("Использует ли character rotation вместо orientation?")]
    [SerializeField]
    protected bool IsCharacterRotation;

    /// <summary>
    /// Ссылка на компонент вращения чара, если он существует
    /// </summary>
    protected TopDownController2D characterController;
    
    protected Collider2D weaponCollider;
    /// <summary>
    /// Актуальный трансформ из которого вылетают пули
    /// Можно либо указать в редакторе, либо будет подхвачен из CharacterHandleWeapon
    /// </summary>
    protected Transform actualStartPos;
    
    
    protected override void Initialization()
    {
        base.Initialization();
        weaponCollider = GetComponent<Collider2D>();
        if (IsCharacterRotation)
        {
            characterController = _weapon.Owner.GetComponent<TopDownController2D>();
        }

        Transform chwSpawn=null;
        if(useChwStartPos)
        chwSpawn = _weapon.Owner.GetComponent<CharacterHandleWeapon>().ProjectileSpawn;
        actualStartPos = projectileStartPos;
        if (chwSpawn != null) actualStartPos = chwSpawn;
    }

    public override void GetScriptAim()
    {
        if (IsCharacterRotation)
        {
            if (characterController != null)
            {
                _currentAimAbsolute = _currentAim;
                _currentAim = characterController.CurrentDirection;
                _direction = -(transform.position - _currentAim);
            }
        }
        else
        {
            base.GetScriptAim();
        }
    }

    public override void SetCurrentAim(Vector3 newAim)
    {
        base.SetCurrentAim(newAim);
        if(AimControl != AimControls.Script)
        GetScriptAim();
    }

    public override void GetMouseAim()
    {
        _mousePosition = Input.mousePosition;
        _mousePosition.z = 10;

        _direction = _mainCamera.ScreenToWorldPoint(_mousePosition);
        _direction.z = _weapon.Owner.transform.position.z;

        _reticlePosition = _direction;

        //var curPos = (actualStartPos == null) ? transform.position : actualStartPos.position;
        var curPos = _weapon.Owner.transform.position;

        if (weaponCollider != null)
        {
            if (!weaponCollider.OverlapPoint(_direction))
            {
                _currentAimAbsolute = _direction - curPos;
                _currentAim = _currentAimAbsolute;
            }
        }
        else
        {
            if (Vector2.Distance(_direction, curPos) > MouseDeadZoneRadius)
            {
                _currentAimAbsolute = _direction - curPos;
                _currentAim = _currentAimAbsolute;
            }
        }


        if (_weapon.Owner.Orientation2D != null)
        {
            if (_weapon.Owner.Orientation2D.IsFacingRight)
            {
                _currentAim = _direction - _weapon.Owner.transform.position;
                _currentAimAbsolute = _currentAim;
            }
            else
            {
                _currentAim = _weapon.Owner.transform.position - _direction;
            }
        }            
    }

    protected override void InitializeReticle()
    {
        if (_weapon.Owner == null) { return; }
        if (Reticle == null) { return; }
        if (ReticleType == ReticleTypes.None) { return; }

        if (ReticleType == ReticleTypes.Scene)
        {
            _reticle = (GameObject)Instantiate(Reticle);

            if (!ReticleAtMousePosition)
            {
                if (_weapon.Owner != null)
                {
                    _reticle.transform.SetParent(_weapon.transform);
                    _reticle.transform.localPosition = ReticleDistance * Vector3.right;
                }
            }                
        }

        if (ReticleType == ReticleTypes.UI)
        {
            _reticle = (GameObject)Instantiate(Reticle);
            _reticle.transform.SetParent(GUIManager.Instance.MainCanvas.transform);
            _reticle.transform.localScale = Vector3.one;
            if (_reticle.gameObject.MMGetComponentNoAlloc<UIFollowMouse>() != null)
            {
                var fm = _reticle.gameObject.MMGetComponentNoAlloc<UIFollowMouse>();
                fm.TargetCanvas = GUIManager.Instance.MainCanvas;
                fm.offset = _weapon.Owner.transform.position - _weapon.transform.position;
                fm.isReticle = true;
                fm.playerPos = _weapon.Owner.transform;
                fm.weaponPos = _weapon.transform;
            }
            
        }
    }
}
