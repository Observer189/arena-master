﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class Knife : ExMeleeWeapon
{
    [Header("Properties")]
    public float baseDamage;


    private ObjectProperty damageProperty;
    public override void Initialization()
    {
        base.Initialization();
        if (Item == null)
        {
            damageProperty = propertyManager.AddProperty("PhysicalDamage", baseDamage);
            propertyManager.RemoveEquipmentBonuses("Weapon");
        }
        else
        {
            for (int i = 0; i < Item.Properties.Length; i++)
            {
                var cur = Item.Properties[i];
                propertyManager.AddProperty(cur.PropertyDescription,cur.BaseValue,cur.CurValue);
            }
            damageProperty = propertyManager.GetPropertyById(4);
        }
    }

    protected override void OnInitializeEffectOnTouch(EffectOnTouch effectOnTouch)
    {
        base.OnInitializeEffectOnTouch(effectOnTouch);
        Effect dmg=new Effect("PhysicalDamage",propertyManager);
        effectOnTouch.AddEffect(dmg);
    }
}
