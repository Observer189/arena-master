﻿
    using InventorySystem;

    public interface IAmmoBasedWeapon
    {
       bool IsAmmoInventoryBased { get; }

       int AmmoId { get; }

       int AvailableAmmo { get; }

       Inventory RelatedInventory { get; }

       void UpdateAvailableAmmo();
    }
