﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using InventorySystem;
using MoreMountains.Tools;
using Newtonsoft.Json;
using TMPro;
using UnityEngine;
/// <summary>
/// Класс, позволяющий получить основную информацию об игроке
/// </summary>
[DataContract]
public class PlayerData : MonoBehaviour,ISavableComponent,MMEventListener<SceneLoadEvent>
{
    
    public static PlayerData instance;
    
    public Inventory inventory { get;  set; }
    
    public SpecialInventory equipment { get; private set; }

    private PropertyManager propertyManager;
    [JsonProperty]
    private HashSet<string> storyMarks;
    [SerializeField]
    private long componentId;

    public PropertyManager PropertyManager => propertyManager;
    
    public long ComponentId => componentId;
    private void Awake()
    {
        instance = this;
        Debug.Log("Instantiate");
        storyMarks=new HashSet<string>();
        propertyManager = GetComponent<PropertyManager>();
    }

    void Start()
    {
        /*foreach (var invCont in GetComponents<InventoryController>())
        {
            var inv = invCont.Inventory;
            if (inv != null)
            {
                if (inv.VisualizerType==VisualizerType.PlayerInventory)
                {
                    inventory = inv;
                }
                else if(inv.VisualizerType==VisualizerType.PlayerEquipment)
                {
                    equipment = (SpecialInventory) inv;
                }
            }
        }*/
        BindInventory();
    }

    private void BindInventory()
    {
        var invConts = GetComponents<InventoryController>();
        foreach (var invCont in invConts)
        {
            var inv = invCont.Inventory;
            Debug.Log($"Полученный инвентарь: {inv}");
            if (inv != null)
            {
                if (inv.VisualizerType==VisualizerType.PlayerInventory)
                {
                    inventory = inv;
                    InventoryManager.instance.RegisterPlayerInventory(inventory);
                }
                else if(inv.VisualizerType==VisualizerType.PlayerEquipment)
                {
                    equipment = (SpecialInventory) inv;
                    InventoryManager.instance.RegisterPlayerEquipment(equipment);
                }
            }
        }
    }

    /// <summary>
    /// Возвращает true если игрок имеет ВСЕ сюжетные марки, переданные в параметре
    /// </summary>
    public bool HasStoryMarks(string[] marks)
    {
        return storyMarks.IsSupersetOf(marks) || marks==null || marks.Length==0;
    }

    public bool HasStoryMark(string mark)
    {
        return storyMarks.Contains(mark);
    }
    /// <summary>
    /// Возваращает true, если содержит хотя бы одну отметку из тех, что подается на вход
    /// </summary>
    /// <param name="marks"></param>
    /// <returns></returns>
    public bool HasAtLeastOneMark(string[] marks)
    {
        foreach (var mark in marks)
        {
            if (HasStoryMark(mark))
            {
                return true;
            }
        }

        return false;
    }

    public void AddStoryMark(string mark)
    {
        storyMarks.Add(mark);
    }

    public void AddStoryMarks(string[] marks)
    {
        storyMarks.UnionWith(marks);
        PrintMarks();
    }

    public void RemoveStoryMark(string mark)
    {
        if(storyMarks.Contains(mark))
        storyMarks.Remove(mark);
    }

    public void RemoveStoryMarks(string[] marks)
    {
        foreach (var mark in marks)
        {
            RemoveStoryMark(mark);
        }
    }
    
    

    protected void PrintMarks()
    {
        StringBuilder output=new StringBuilder($"Всего отметок: {storyMarks.Count}");
        foreach (var mark in storyMarks)
        {
            output.AppendLine("\n"+mark);
        }
        Debug.Log(output);
    }

    public void OnMMEvent(SceneLoadEvent eventType)
    {
        if (eventType.eventType == SceneLoadEventType.LoadEnd)
        {
            BindInventory();
            //MMGameEvent.Trigger("PlayerInventoryUpdate");
        }
    }

    private void OnEnable()
    {
        this.MMEventStartListening<SceneLoadEvent> ();
    }
    
    private void OnDisable()
    {
        this.MMEventStopListening<SceneLoadEvent> ();
    }
}
