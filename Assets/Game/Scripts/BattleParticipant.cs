﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.TopDownEngine;
using UnityEngine;
[RequireComponent(typeof(Health))]
public class BattleParticipant : MonoBehaviour
{
    [Tooltip("Очки которые дают при убийстве объекта")]
    [SerializeField]
    private int deathPoints = 1;

    private bool participateInBattle;

    private Health health;

    public int DeathPoints => deathPoints;
    private void Awake()
    {
        health = GetComponent<Health>();
        health.OnDeath += OnKill;
    }

    public void InitiateParticipation()
    {
        Debug.Log("Initiate participation");
        participateInBattle = true;
        GetComponent<AiOnOffBrain>().OnBrain();
    }

    public void InterruptParticipation()
    {
        participateInBattle = false;
        GetComponent<AiOnOffBrain>().OffBrain();
    }

    public void OnKill()
    {
        if (participateInBattle)
        {
            BattleEvent.Trigger(BattleEventType.BattlePointsChange,DeathPoints);
        }
    }
}
