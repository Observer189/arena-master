﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SaveView : MonoBehaviour,IPointerDownHandler
{
    protected FileInfo file;
    protected LoadWindow loadWindow;
    

    [SerializeField] 
    protected Text nameText;
    [SerializeField]
    protected Text timeText;

    private float lastClickTime=0;
    private Vector2 lastClickPosition;
    
    public void Initialize(FileInfo file, LoadWindow loadWindow)
    {
        nameText.text = Path.GetFileNameWithoutExtension(file.FullName);
        timeText.text = file.LastWriteTime.ToString();
        this.file = file;
        this.loadWindow = loadWindow;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (lastClickTime != 0)
        {
            ///Отлавливание дабл клика
            if (Time.time - lastClickTime < 0.2f && (lastClickPosition-eventData.position).magnitude<10)
            {
                loadWindow.OnDoubleClickSave(file);
            }
        }

        lastClickTime = Time.time;
        lastClickPosition = eventData.position;
    }

    public void DeleteRequest()
    {
        loadWindow.OnDeleteSave(file);
    }
}
