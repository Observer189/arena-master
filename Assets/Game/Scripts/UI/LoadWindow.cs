﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class LoadWindow : MonoBehaviour
{
    private CanvasGroup hider;
    
    [Tooltip("Ссылка на родительский объект для всех элементов списка")] 
    [SerializeField]
    protected Transform content;
    [SerializeField]
    protected GameObject saveViewPrefab;

    protected List<SaveView> saveList;

    private void Awake()
    {
        hider = GetComponent<CanvasGroup>();
        saveList = new List<SaveView>();
    }

    void Start()
    {
   
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnCloseButton()
    {
        Hide();
        gameObject.SetActive(false);
    }

    public void OnDoubleClickSave(FileInfo file)
    {
        SaveLoadManager.Instance.LoadGame(file.Name);
    }
    
    public void OnDeleteSave(FileInfo file)
    {
        SaveLoadManager.Instance.DeleteGameSave(file.Name);
        FillSaveList(SaveLoadManager.Instance.GetAllGameSaves());
    }

    public void Hide()
    {
        hider.alpha = 0;
        ClearSaveList();
    }

    public void Show()
    {
        hider.alpha = 1;
        FillSaveList(SaveLoadManager.Instance.GetAllGameSaves());
    }

    protected void FillSaveList(List<FileInfo> files)
    {
        while (saveList.Count > files.Count)
        {
            Destroy(saveList[saveList.Count-1].gameObject);
            saveList.RemoveAt(saveList.Count-1);
        }

        for (int i = 0; i < saveList.Count; i++)
        {
            saveList[i].Initialize(files[i],this);
        }
        
        for (int i = saveList.Count; i < files.Count; i++)
        {
            var pref = Instantiate(saveViewPrefab,content);
            var saveView = pref.GetComponent<SaveView>();
            saveList.Add(saveView);
            saveView.Initialize(files[i],this);
        }
    }

    protected void ClearSaveList()
    {
        if (saveList != null)
        {
            for (int i = 0; i < saveList.Count; i++)
            {
                Destroy(saveList[i].gameObject);
            }

            saveList.Clear();
        }
    }
}
