﻿using System.Collections;
using System.Collections.Generic;
using Lean.Localization;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PropertyLine : MonoBehaviour,IPointerEnterHandler,IPointerDownHandler,IPointerUpHandler
{
    [SerializeField]
    private Text property;
    [SerializeField]
    private Text addition;
    [SerializeField] 
    private Image iconImg;

    private ObjectPropertyDescription propertyDescription;

    private ContextInfoPanel infoPanel;

    private float value;
   
    /// <summary>
    /// Инициализирует свойство, загружая иконку и сохраняя имя
    /// </summary>
    /// <param name="property"></param>
    public void InitializeProperty(ObjectPropertyDescription property,ContextInfoPanel infoPanel)
    {
        propertyDescription = property;
        this.infoPanel = infoPanel;
        iconImg.sprite = Resources.Load<Sprite>($"PropertyIcons/{property.ImageName}");
    }
    /// <summary>
    /// Устанавливает значение свойства и дополнения
    /// Если дополнение = null, то оно скрывается
    /// </summary>
    public void SetValue(float value,float? addition)
    {
        var transName =
            LeanLocalization.GetTranslationText(propertyDescription.ShortName, propertyDescription.ShortName);
        property.text = $"{transName}: {Mathf.FloorToInt(value)}";
        if (addition.HasValue)
        {
            this.addition.text = $"+{addition}";
        }
        else
        {
            this.addition.text = "";
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            var transName = LeanLocalization.GetTranslationText(propertyDescription.Name,propertyDescription.Name);
            var transDesc = LeanLocalization.GetTranslationText(propertyDescription.Name+"Desc",propertyDescription.Description);
            infoPanel.gameObject.SetActive(true);
            infoPanel.Place(transform.position,transName,transDesc);
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        infoPanel.transform.position=new Vector3(10000,10000);
        infoPanel.gameObject.SetActive(false);
    }
}
