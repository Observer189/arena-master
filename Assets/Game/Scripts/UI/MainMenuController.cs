﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Principal;
using MoreMountains.Tools;
using MoreMountains.Feedbacks;
using MoreMountains.TopDownEngine;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    [Tooltip("Название сцены, которая загружается при начале новой игры")]
    [SerializeField]
    private string newGameScene; 
    
    [Tooltip("Название сцены, которая служит экраном загрузки")]
    [SerializeField]
    private string loadingScreenScene;

    [Tooltip("Ссылка на окно настроек")]
    [SerializeField]
    private SettingsWindow settingsWindow;
    
    [Tooltip("Ссылка на окно настроек")]
    [SerializeField]
    private LoadWindow loadWindow;


    [Tooltip("Уровень с которого начинается новая игра")]
    [SerializeField]
    private Level newGameLevel;

    public MMFeedbacks clickSoundFeedback;


    private void Awake()
    {
    }

    private void Start()
    {
        settingsWindow.Hide();
        settingsWindow.gameObject.SetActive(false);
        
        loadWindow.Hide();
        loadWindow.gameObject.SetActive(false);
        ///Вызываем событие показывающее, что мы перешли в главное меню
        /// Главным образом нужно для переключения музыки
        MMGameEvent.Trigger("MainMenuLoad");
    }

    public void StartNewGameClick()
    {
        clickSoundFeedback?.PlayFeedbacks();
        GlobalLevelManager.Instance.NewGame();
        GlobalLevelManager.Instance.GoToLevel(newGameLevel,LoadMode.IgnoreSceneSave);
        MMSceneLoadingManager.LoadScene(newGameLevel.LevelScene,loadingScreenScene);

    }

    public void ContinueGameClick()
    {
        clickSoundFeedback?.PlayFeedbacks();
        //MMSceneLoadingManager.LoadScene("GameSceneLoad",loadingScreenScene);
        SaveLoadManager.Instance.LoadLastSave();
    }

    public void LoadGameClick()
    {
        clickSoundFeedback?.PlayFeedbacks();
        loadWindow.gameObject.SetActive(true);
        loadWindow.Show();
    }

    public void OpenSettingsClick()
    {
        clickSoundFeedback?.PlayFeedbacks();
        for (int i = 0; i < Screen.resolutions.Length; i++)
        {
            Debug.Log(Screen.resolutions[i]);
        }
        settingsWindow.gameObject.SetActive(true);
        settingsWindow.Show();
    }

    private void OnEnable()
    {
        Cursor.visible = true;
    }


    public void ExitGameClick()
    {
        clickSoundFeedback?.PlayFeedbacks();
        Application.Quit();
    }
}
