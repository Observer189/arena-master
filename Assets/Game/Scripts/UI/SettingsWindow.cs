﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Lean.Localization;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;
using UnityEngine.UI;
using MoreMountains.Tools;

public class SettingsWindow : MonoBehaviour
{
    private CanvasGroup hider;
    [Tooltip("Ссылка на отображение настроек видео")]
    [SerializeField]
    private RectTransform videoGroup;
    
    [Tooltip("Ссылка на отображение настроек аудио")]
    [SerializeField]
    private RectTransform audioGroup;
    
    [Tooltip("Ссылка на отображение настроек игры")]
    [SerializeField]
    private RectTransform gameGroup;
    
    
    [SerializeField]
    private Dropdown resolutionDropdown;
    [SerializeField]
    private Dropdown screenModeDropDown;
    [SerializeField]
    private SoundSettingsSliderController[] soundSliders;

    [SerializeField] 
    private Dropdown languageDropdown;

    private FullScreenMode[] modes;
    
    /// <summary>
    /// Словарь языков
    /// Ключ - то как язык пишется в выпадающем списке
    /// Значение - то как язык называется в системе LeanLocalization 
    /// </summary>
    private Dictionary<string, string> languages;
    
   
    private RectTransform currentGroup;
    private void Awake()
    {
        hider = GetComponent<CanvasGroup>();
        modes=new FullScreenMode[4];
        modes[0] = FullScreenMode.Windowed;
        modes[1] = FullScreenMode.MaximizedWindow;
        modes[2] = FullScreenMode.ExclusiveFullScreen;
        modes[3] = FullScreenMode.FullScreenWindow;
        
        languages=new Dictionary<string, string>();
        languages.Add("English","English");
        languages.Add("Русский","Russian");
        Debug.Log(videoGroup);
    }

    void Start()
    {
        InitializeUI();
        LoadSettings(true);
    }

    private void InitializeUI()
    {
        resolutionDropdown.ClearOptions();
        resolutionDropdown.AddOptions(Screen.resolutions.Select(r=>r.ToString()).ToList());
        languageDropdown.ClearOptions();
        languageDropdown.AddOptions(languages.Select(l=>l.Key).ToList());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            CloseButtonClick();
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            OnApply();
        }
        
    }

    public void VideoGroupSelect()
    {
        ShowNewGroup(videoGroup);

    }
    public void AudioGroupSelect()
    {
       ShowNewGroup(audioGroup);
    }
    public void GameGroupSelect()
    {
        ShowNewGroup(gameGroup);
    }
    
    public void CloseButtonClick()
    {
        Hide();
        LoadSettings(false);
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Сменяет отображаемую группу на указанную
    /// </summary>
    private void ShowNewGroup(RectTransform newGroup)
    {
        if (currentGroup != newGroup)
        {
            currentGroup.gameObject.SetActive(false);
            currentGroup = newGroup;
            currentGroup.gameObject.SetActive(true);
        }
    }

    public void Show()
    {
        currentGroup = videoGroup;
        currentGroup.gameObject.SetActive(true);
        audioGroup.gameObject.SetActive(false);
        gameGroup.gameObject.SetActive(false);
        hider.alpha = 1;
    }

    public void Hide()
    {
        hider.alpha = 0;
    }
    /// <summary>
    /// Вызывается при нажатии на кнопку применить
    /// </summary>
    public void OnApply()
    {
        ApplySettings(resolutionDropdown.value,modes[screenModeDropDown.value],languages[languageDropdown.captionText.text]);
        SaveSettings();
    }


    /// <summary>
    /// Применение всех настроек
    /// </summary>
    private void ApplySettings(int resolutionNum, FullScreenMode mode, string language)
    {
        //Чтобы resolutionNum не мог быть слишком большим
        resolutionNum = Mathf.Min(Screen.resolutions.Length,resolutionNum);
        var resolution = Screen.resolutions[resolutionNum];
        bool isFullscreen = (mode == FullScreenMode.FullScreenWindow || mode == FullScreenMode.ExclusiveFullScreen);
        Screen.SetResolution(resolution.width,resolution.height,isFullscreen,resolution.refreshRate);
        Screen.fullScreenMode = mode;
        

        for (int i = 0; i < soundSliders.Length; i++)
        {
            soundSliders[i].ApplySetting();
        }
        
        LeanLocalization.SetCurrentLanguageAll(language);
    }

    private void SaveSettings()
    {
        PlayerPrefs.SetInt("ResolutionNum",resolutionDropdown.value);
        PlayerPrefs.SetInt("ScreenModeNum",screenModeDropDown.value);
        MMSoundManagerEvent.Trigger(MMSoundManagerEventTypes.SaveSettings);
        PlayerPrefs.Save();
    }
   /// <summary>
   /// Загружает настройки из памяти
   /// Если сохранения нет, то создает
   /// </summary>
   /// <param name="apply">нужно ли применить изменения или же просто правильно выставить UI</param>
    private void LoadSettings(bool apply)
    {
        int resolutionNum = PlayerPrefs.GetInt("ResolutionNum",-1);
        if (resolutionNum == -1)
        {
            SaveSettings();
            LoadSettings(true);
            return;
        }
        int screenMode = PlayerPrefs.GetInt("ScreenModeNum",0);

        resolutionDropdown.value = resolutionNum;
        screenModeDropDown.value = screenMode;

        MMSoundManagerEvent.Trigger(MMSoundManagerEventTypes.LoadSettings);
        for (int i = 0; i < soundSliders.Length; i++)
        {
            soundSliders[i].Load();
        }
        
        ///Находим номер элемента выпадающего списка с нужным языком
        int j = 0;
        foreach (var language in languages)
        {
            if(language.Value==LeanLocalization.GetFirstCurrentLanguage()) break;
            j++;
        }

        languageDropdown.value = j;
        if(apply)
        ApplySettings(resolutionNum,modes[screenMode],LeanLocalization.GetFirstCurrentLanguage());
    }

   [ContextMenu("DeleteSave")]
   public void DeleteSave()
   {
       PlayerPrefs.DeleteAll();
   }



}
