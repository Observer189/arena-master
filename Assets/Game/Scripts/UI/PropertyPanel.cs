﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropertyPanel : MonoBehaviour
{
    [Tooltip("Объект в иерархии внутри которого лежат все PropertyLines")]
    [SerializeField]
    private Transform content;
    [Tooltip("Префаб PropertyLine")]
    [SerializeField]
    private GameObject propertyPrefab;
    [Tooltip("Ссылка на контесктную панель для свойств")]
    [SerializeField]
    private ContextInfoPanel infoPanel;

    private EquipmentItem eqItem;

    private List<PropertyLine> lines;

   
    private void Awake()
    {
        lines=new List<PropertyLine>();
    }
    /// <summary>
    /// Началаьная инициализация
    /// Здесь создаются все PropertyLines, дальше они будут лишь обновляться
    /// </summary>
    public void PrimaryInitialization(EquipmentItem w)
    {
        eqItem = w;
        Clear();
        foreach (var property in eqItem.Properties)
        {
            var line = Instantiate(propertyPrefab, content).GetComponent<PropertyLine>();
            line.InitializeProperty(property.PropertyDescription,infoPanel);
            line.SetValue(property.BaseValue,null);
            lines.Add(line);
        }
    }
   /// <summary>
   /// Обновляет значения свойства в соответствии с значениями в WeaponItem
   /// </summary>
    public void UpdateValues()
    {
        for (int i = 0; i < eqItem.Properties.Length; i++)
        {
            var propertyInfo = eqItem.Properties[i];
            bool bonusExists = eqItem.ModuleBonuses.ContainsKey(propertyInfo.PropertyDescription.Id);
            if (bonusExists)
            {
                var bonus = eqItem.ModuleBonuses[propertyInfo.PropertyDescription.Id];
                Debug.Log($"bonus = {bonus}");
                lines[i].SetValue(propertyInfo.BaseValue + propertyInfo.BaseValue * bonus.Item2 + bonus.Item1, null);
            }
            else
            {
                lines[i].SetValue(propertyInfo.BaseValue,null);
            }
        }
    }

    /// <summary>
    /// Удаление всех PropertyLines
    /// </summary>
    public void Clear()
    {
        for (int i = 0; i < content.childCount; i++)
        {
            Destroy(content.GetChild(i).gameObject);
        }
        lines.Clear();
        
    }
    
    
}
