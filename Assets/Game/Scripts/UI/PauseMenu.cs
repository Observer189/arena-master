﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;
using UnityEngine.EventSystems;

public class PauseMenu : MonoBehaviour
{
   [SerializeField]
   private string MainMenuScene = "MainMenu";
   
   [SerializeField]
   private string TestScene = "TestScene";
   
   [SerializeField]
   private string GameScene = "GameScene";
   
   [Tooltip("Название сцены, которая служит экраном загрузки")]
   [SerializeField]
   private string loadingScreenScene = "MyLoadingScreen";
   
   [Tooltip("Ссылка на окно настроек")]
   [SerializeField]
   private SettingsWindow settingsWindow;

   private void Awake()
   {
      
   }

   public void OnExitToMainMenu()
   {
      GameManager.Instance.UnPause();
      MMSceneLoadingManager.LoadScene(MainMenuScene,loadingScreenScene);
   }

   public void OnLoadTestScene()
   {
      GameManager.Instance.UnPause();
      MMSceneLoadingManager.LoadScene(TestScene,loadingScreenScene);
   }

   public void OnLoadCheckpoint()
   {
      GameManager.Instance.UnPause();
      //MMSceneLoadingManager.LoadScene(GameScene,loadingScreenScene);
      SaveLoadManager.Instance.LoadLastSave();
   }

   public void OnOpenSettings()
   {
      settingsWindow.gameObject.SetActive(true);
      settingsWindow.Show();
   }

   private void OnEnable()
   {
      Cursor.visible = true;
   }

   private void OnDisable()
   {
      Cursor.visible = false;
      if (settingsWindow.gameObject.activeSelf)
      {
         settingsWindow.Hide();
         settingsWindow.gameObject.SetActive(false);
      }
   }
}
