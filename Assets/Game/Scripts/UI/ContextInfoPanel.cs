﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContextInfoPanel : MonoBehaviour
{
    [SerializeField] private Text nameText;
    [SerializeField] private Text mainText;
    [Tooltip("Смещение относительно элемента, к которому привязывается")]
    [SerializeField] private Vector2 offset;

    [SerializeField] private Camera camera;
    
    /// <summary>
    /// Позиция элемента, к которому привязан в глобальных координатах
    /// </summary>
    private Vector3 pos;

    public Vector2 Offset
    {
        get => offset;
        set => offset = value;
    }
    

    public void Place(Vector3 pos, string name, string mainText)
    {
        nameText.text = name;
        this.mainText.text = mainText;
        this.pos = pos;
        StartCoroutine(ClampPosition());
    }

    /// <summary>
    /// Размещаем панель так чтобы она не вылезала за экран
    /// Делаем это на следующий кадр, чтобы она успела перестроиться
    /// </summary>
    private IEnumerator ClampPosition()
    {
        yield return new WaitForEndOfFrame();
        transform.position = pos + new Vector3(offset.x,offset.y);
        var rect = (RectTransform) transform;
        RectTransformUtility.ScreenPointToLocalPointInRectangle((RectTransform) transform.parent, new Vector2(0, 0), camera,
            out Vector2 bottomLeft);
        RectTransformUtility.ScreenPointToLocalPointInRectangle((RectTransform) transform.parent, new Vector2(Screen.width, Screen.height), camera,
            out Vector2 topRight);
        
        transform.localPosition = new Vector3(Mathf.Clamp(transform.localPosition.x,
                bottomLeft.x+rect.sizeDelta.x*rect.pivot.x,topRight.x-rect.sizeDelta.x*rect.pivot.x),
            Mathf.Clamp(transform.localPosition.y,bottomLeft.y+rect.sizeDelta.y*rect.pivot.y,
                topRight.y-rect.sizeDelta.y*rect.pivot.y),0);
    }
    
    
}
