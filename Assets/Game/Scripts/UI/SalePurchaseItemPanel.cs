﻿using System;
using System.Collections;
using System.Collections.Generic;
using InventorySystem;
using Lean.Localization;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Image = UnityEngine.UI.Image;

public class SalePurchaseItemPanel : MonoBehaviour
{
    private CanvasGroup hider;
    [SerializeField]
    private Text panelName;
    [SerializeField]
    private Text itemName;
    [SerializeField]
    private Text itemType;
    [SerializeField]
    private Text itemDescription;
    [SerializeField]
    private Image itemIcon;
    [SerializeField]
    private Text itemPrice;
    [SerializeField]
    private InputField operationQuantity;
    [SerializeField]
    private Text totalCost;
    [SerializeField]
    private Color totalCostNormalColor;
    [SerializeField]
    private Color totalCostImpossibleColor;

    private Item curIt;
    /// <summary>
    /// Максимальная стоимость на которую можно совершить операцию
    /// по сути представляет из себя количество денег покупателя
    /// </summary>
    private int maxCost;
   /// <summary>
   /// Максимальное количество предметов, которые можно купить/продать
   /// </summary>
    private int maxQToOperate;
   /// <summary>
   /// Текущее количество предметов, которое хочет купить/продать игрок
   /// </summary>
   private int resQuantity;
    /// <summary>
    /// Становится true, когда нажимаем кнопку завершения покупки или отмены
    /// Является условием выхода из окна 
    /// </summary>
    private bool isButtonPressed=false;

    private bool wasCanceled = false;

    private bool isPurchase;
    private void Awake()
    {
        hider = GetComponent<CanvasGroup>();
    }

    private void Start()
    {
        //Hide();
    }

    /// <summary>
    /// Инициализирует операцию покупки/продажи
    /// </summary>
    /// <param name="item">Предмет, который хоти купить</param>
    /// <param name="buyerMoney">количество денег покупателя</param>
    /// <param name="quantityToOperate">количество, которое пользователь решил по итогу купить/продать</param>
    /// <param name="isPurchase">true-покупка false-продажа</param>
    /// <returns></returns>
    public IEnumerator InitializeOperation(Item item,int buyerMoney, Ref<int> quantityToOperate, bool isPurchase)
    {
        curIt = item;
        maxCost = buyerMoney;
        Debug.Log($"buyer money = {buyerMoney}");
        if (isPurchase)
        {
            panelName.text = LeanLocalization.GetTranslationText("Purchase","Purchase");
        }
        else
        {
            panelName.text = LeanLocalization.GetTranslationText("Sale","Sale");
        }
        maxQToOperate = Mathf.Min(item.Quantity, Mathf.FloorToInt(maxCost / item.EstimatedCost));
        this.isPurchase = isPurchase;
        resQuantity = 1;
        Debug.Log("Operate!");
        quantityToOperate.value = 0;
        UpdateItemInfoUi();
        UpdateDynamicUI();
        Debug.Log("Update UI");
        Show();
        Debug.Log("Show purchase panel!");
        yield return new WaitUntil(()=>isButtonPressed);

        if (!wasCanceled)
        {
            quantityToOperate.value = resQuantity;
        }
        Hide();

    }

    public void IncQuantity()
    {
        resQuantity++;
        resQuantity = Mathf.Clamp(resQuantity,1, curIt.Quantity);
        UpdateDynamicUI();
    }

    public void OnEditQ()
    {
        var f = int.TryParse(operationQuantity.text, out int parsedValue);
        if (!f)
        {
            operationQuantity.text = resQuantity.ToString();
            return;
        }

        resQuantity = Mathf.Clamp(parsedValue, 1, curIt.Quantity);
        UpdateDynamicUI();
    }

    public void DecQuantity()
    {
        resQuantity--;
        resQuantity = Mathf.Clamp(resQuantity,1, curIt.Quantity);
        UpdateDynamicUI();
    }

    public void OnPressOk()
    {
        var tCost = Mathf.CeilToInt(resQuantity * curIt.EstimatedCost);
        if(tCost<=maxCost)
        isButtonPressed = true;
    }

    public void CancelOperation()
    {
        wasCanceled = true;
        isButtonPressed = true;
    }

    /// <summary>
    /// Выставляет в UI информацию о текущем предмете
    /// </summary>
    private void UpdateItemInfoUi()
    {
       
        itemName.text = LeanLocalization.GetTranslationText(curIt.Name,curIt.Name);
        itemType.text = LeanLocalization.GetTranslationText(curIt.Type.ToString(), curIt.Type.ToString());
        itemDescription.text = LeanLocalization.GetTranslationText(curIt.Name+"Desc",curIt.Description);
        itemPrice.text = curIt.EstimatedCost.ToString();
        itemIcon.sprite = curIt.Icon;
    }

    private void UpdateDynamicUI()
    {
        operationQuantity.text = resQuantity.ToString();
        var tCost = Mathf.CeilToInt(resQuantity * curIt.EstimatedCost);
        totalCost.text = tCost.ToString();
        if (tCost > maxCost)
        {
            totalCost.color = totalCostImpossibleColor;
        }
        else
        {
            totalCost.color = totalCostNormalColor;
        }
    }

    public void Show()
    {
        hider.alpha = 1;
        isButtonPressed = false;
        wasCanceled = false;
    }

    public void Hide()
    {
        hider.alpha = 0;
        isButtonPressed = false;
        wasCanceled = false;
    }
}
