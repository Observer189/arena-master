﻿using System;
using System.Collections;
using System.Collections.Generic;
using InventorySystem;
using Lean.Localization;
using MoreMountains.Tools;
using UnityEngine;
using UnityEngine.Assertions.Must;
using UnityEngine.UI;

public class Notifier : MonoBehaviour
{
    /// <summary>
    /// Синглтон паттерн
    /// </summary>
    public static Notifier instance;
    [SerializeField]
    private RectTransform NotificationObj;
    
    private CanvasGroup hider;
    [SerializeField]
    private Image eventImage;
    [SerializeField]
    private Text eventText;

    [Header("Images")] 
    [SerializeField] private Sprite okImage;
    [SerializeField] private Sprite errorImage;
    [SerializeField] private Sprite infoImage;
    [SerializeField] private Sprite saveImage;

    [Header("Translated string names")] 
    [SerializeField]
    private string receivedItem = "ReceivedItem";
    [SerializeField]
    private string lostItem = "LostItem";
    [SerializeField]
    private string inTheAmount = "InTheAmount";
    /// <summary>
    /// базвоая длительность вывода сообщения
    /// </summary>
    [SerializeField]
    private float defaultDuration;

    /// <summary>
    /// Общее кол-во запрошенных уведомлений
    /// Используется для отслеживания стэка уведомлений
    /// </summary>
    private int totalRequestedNotifications=0;
    /// <summary>
    /// Общее количество уведомлений, которые были полность показаны
    /// Используется для отслеживания стэка уведомлений
    /// </summary>
    private int totalShowedNotifications=0;
    
    private void Awake()
    {
        instance = this;
        hider = NotificationObj.GetComponent<CanvasGroup>();
    }

    private void Start()
    {
        hider.alpha = 0;
        NotificationObj.gameObject.SetActive(false);
    }

    public void ShowNotification(string notificationText,NotificationType type)
    {
        Sprite img=null;
        switch (type)
        {
            case NotificationType.Info:
                img = infoImage;
                break;
            case NotificationType.Ok:
                img = okImage;
                break;
            case NotificationType.Error:
                img = errorImage;
                break;
            case NotificationType.Save:
                img = saveImage;
                break;
        }
        StartCoroutine(ShowNotificationCor(notificationText,img,totalRequestedNotifications));
        totalRequestedNotifications++;
    }
    /// <summary>
    /// Передаем количество так, как один предмет не может содержать больше чем MaxStackCount,
    /// а добавляться столько может
    /// </summary>
    public void ShowAddOrRemoveItemNotification(Item item,int quantity,bool isAdd)
    {
        string strToShow = $"{((isAdd)?LeanLocalization.GetTranslationText(receivedItem):LeanLocalization.GetTranslationText(lostItem))} " +
                           $"{LeanLocalization.GetTranslationText(item.Name,item.Name)}" +
                           $"\n{LeanLocalization.GetTranslationText(inTheAmount)} {quantity}";
        StartCoroutine(ShowNotificationCor(strToShow,item.Icon,totalRequestedNotifications));
        totalRequestedNotifications++;
    }
   
    public IEnumerator ShowNotificationCor(string notificationText,Sprite image, int num)
    {
        yield return new WaitUntil(()=>totalShowedNotifications==num);
        NotificationObj.gameObject.SetActive(true);
        eventText.text = notificationText;
        eventImage.sprite = image;
        Debug.Log("Notify");
        yield return StartCoroutine(MMFade.FadeCanvasGroup(hider, 0.2f, 1f));
        yield return new WaitForSecondsRealtime(defaultDuration);
        yield return StartCoroutine(MMFade.FadeCanvasGroup(hider, 0.5f, 0f));
        NotificationObj.gameObject.SetActive(false);
        totalShowedNotifications++;
    }
}

public enum NotificationType
{
    Info,Error,Ok,Save
}
