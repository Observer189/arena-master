﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageNotifier : MonoBehaviour
{
    [SerializeField]
    private GameObject guiPrefab;
 
    public Vector2 GUIOffset;
   
    private Text guiText;

    [Tooltip("Время в течение которого параметр, к которому привязан notifier" +
             "должен оставаться неизменным, чтобы он деактивировался")]
    [SerializeField]
    private float deactivationTime=1.5f;
    [Tooltip("Время через которое должен быть уничтожен/выключен UI объект после разрушения/отключения данного объекта")]
    [SerializeField]
    private float TimeBeforeDestruction=1.5f;
    
    [SerializeField]
    private Color negChangeColor=Color.red;
    [SerializeField]
    private Color posChangeColor=Color.green;
    [SerializeField]
    private Color neutralChangeColor=Color.yellow;

    private float timeWithoutChange=0;
    
    //Текущее значение параметра, к которому привязан notifier
    private int value;
    /// <summary>
    /// Общее изменение параметра с последней активации
    /// </summary>
    private int activeChange;

    private bool isActive;

    private bool isInitialized = false;

    private void Awake()
    {
        InitializeGUI();
        Deactivate();
    }

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (isInitialized && isActive)
        {
            timeWithoutChange += Time.deltaTime;
            if (timeWithoutChange > deactivationTime)
            {
                Deactivate();
            }
        }
    }

    private void LateUpdate()
    {
        if (isInitialized && isActive)
        {
            UpdateUIPosition();
        }
    }

    /// <summary>
    /// Устанавливает начальное значение параметра
    /// </summary>
    /// <param name="param"></param>
    public void InitializeParameter(int param)
    {
        value = param;
        isInitialized = true;
        //Deactivate();
    }

    /// <summary>
    /// Через эту функцию происходит изменение параметра, к которому привязан motifier
    /// Соотвественно это изменение и отображает notifier 
    /// </summary>
    /// <param name="newValue"></param>
    public void UpdateValue(int newValue)
    {
        if(!isInitialized) InitializeParameter(newValue);

        int change = newValue - value;
        
        if(change==0) return;
        
        if(!isActive) Activate();
        activeChange += change;
        value = newValue;
        timeWithoutChange = 0;
        UpdateUIValues();
    }

    private void Activate()
    {
        isActive = true;
        guiText.gameObject.SetActive(true);
    }

    private void Deactivate()
    {
        isActive = false;
        activeChange = 0;
        timeWithoutChange = 0;
        guiText.gameObject.SetActive(false);
    }

    private void UpdateUIValues()
    {
        if (activeChange > 0)
        {
            guiText.color = posChangeColor;
        }
        else if (activeChange < 0)
        {
            guiText.color = negChangeColor;
        }
        
        else guiText.color = neutralChangeColor;

        guiText.text = activeChange.ToString();
    }

    private void UpdateUIPosition()
    {
        guiText.gameObject.transform.position=(Vector2)transform.position + GUIOffset;
    }

    private void InitializeGUI()
    {
        var ui = Instantiate(guiPrefab);
        guiText = ui.GetComponent<Text>();
        ui.transform.position = (Vector2)transform.position + GUIOffset;
        ui.transform.SetParent(GameObject.Find("WorldCanvas").transform,true);
    }
    

    private void OnDisable()
    {
        if(guiText!=null && guiText.gameObject!=null)
        UpdateUIPosition();
        Invoke("Deactivate",TimeBeforeDestruction);
    }

    void OnDestroy()
    {
        if(guiText!=null)
        Destroy(guiText.gameObject,TimeBeforeDestruction);
    }
}
