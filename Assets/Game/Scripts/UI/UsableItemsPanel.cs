﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean.Localization;
using UnityEngine;
using UnityEngine.UI;

public class UsableItemsPanel : MonoBehaviour
{
    [Tooltip("Иконка выбранного предмета")]
    [SerializeField]
    protected Image itemIcon;
    [Tooltip("Название выбранного предмета")]
    [SerializeField]
    protected Text itemName;
    [Tooltip("Количество выбранного предмета в инвентаре")]
    [SerializeField] 
    protected Text itemCount;
    
    public void UpdateItemInfo(UsableItem item, int quantity)
    {
        if (quantity == 0 || item == null)
        {
            itemIcon.gameObject.SetActive(false);
            itemName.text = "";
            itemCount.text = "";
            return;
        }
        itemIcon.gameObject.SetActive(true);
        itemIcon.sprite = item.Icon;
        
        itemName.text = LeanLocalization.GetTranslationText(item.Name,item.Name);
        itemCount.text = ""+quantity;
    }
    
}
