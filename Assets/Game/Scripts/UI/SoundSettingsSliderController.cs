﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using UnityEngine;
using UnityEngine.UI;

public class SoundSettingsSliderController : MonoBehaviour
{
    [SerializeField]
    private MMSoundManager.MMSoundManagerTracks trackType;

    [SerializeField] 
    private Slider slider;

    [SerializeField] 
    private Text volumeText;
    [SerializeField]
    private float maxTrackVolume;
    [SerializeField]
    private int maxSliderValue =100;

    public void ApplySetting()
    {
        MMSoundManagerTrackEvent.Trigger(MMSoundManagerTrackEventTypes.SetVolumeTrack,trackType,
            (slider.value/maxSliderValue)*maxTrackVolume);
    }
    /// <summary>
    /// Использует уже загруженные данные о звуке, чтобы выставить UI в нужное положение
    /// </summary>
    public void Load()
    {
         var volume=MMSoundManager.Instance.settingsSo.GetTrackVolume(trackType);
         slider.value = volume / maxTrackVolume * maxSliderValue;
         OnSlide(slider.value);
    }

    public void OnSlide(float value)
    {
        volumeText.text = value.ToString();
    }
}
