﻿using System.Collections;
using System.Collections.Generic;
using SaveLoad;
using UnityEngine;

public struct GameObjectSave
{
    public string name;
    /// <summary>
    /// Тип компонента,сам компонент
    /// </summary>
    public List<(string,ComponentSave)> components;
    /// <summary>
    /// Активен ли объект
    /// </summary>
    public bool isActive;

    public TransformCompSave transform;

    public ColliderSave collider;

    public PropertyManagerSave propertyManagerSave;
}
