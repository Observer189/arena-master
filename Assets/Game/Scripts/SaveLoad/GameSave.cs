﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Структура глобального сохранения игры
/// </summary>
[Serializable]
public class GameSave
{
    public int dayNum;
    public string[] str;
    /// <summary>
    /// Название уровня, который загружен в данный момент
    /// </summary>
    public string loadedLevelName;
    /// <summary>
    /// Словарь сохранений всех сценических объектов для локаций(сцен)
    /// </summary>
    public Dictionary<string,List<GameObjectSave>> sceneGameObjects;
    /// <summary>
    /// Сохранение объектов конкретного текущего уровня
    /// </summary>
    public List<GameObjectSave> currentLevelObjects;
    /// <summary>
    /// Сохранение персонажей, за которых может играть игрок
    /// </summary>
    public Dictionary<string, GameObjectSave> savedCharacters;
    //public PlayerData PlayerData;
}
