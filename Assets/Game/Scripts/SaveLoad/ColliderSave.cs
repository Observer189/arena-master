﻿using Newtonsoft.Json;

namespace SaveLoad
{
    public class ColliderSave
    {
        public bool isEnabled;
        
        [JsonConstructor]
        public ColliderSave(bool isEnabled)
        {
            this.isEnabled = isEnabled;
        }
    }
}