﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

public class TransformCompSave
{
   public Vector3 position;
   public Vector3 rotation;
   public Vector3 scale;
   [JsonConstructor]
   public TransformCompSave(Vector3 position, Vector3 rotation, Vector3 scale)
   {
      this.position = position;
      this.rotation = rotation;
      this.scale = scale;
   }

   public TransformCompSave(Transform t)
   {
      position = t.position;
      rotation = t.rotation.eulerAngles;
      scale = t.localScale;
   }
}
