﻿using System.Collections;
using System.Collections.Generic;
using Lean.Localization;
using UnityEngine;

public class SaveController : MonoBehaviour
{
    
    public void AutoSave()
    {
        SaveLoadManager.Instance.AutoSave();
        var notText = LeanLocalization.GetTranslationText("Game saved!","Game saved!");
        Notifier.instance.ShowNotification(notText,NotificationType.Save);
    }
}
