﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Feedbacks;
using MoreMountains.Tools;
using UnityEngine;

namespace SaveLoad
{
    public class LevelSwitcher : MonoBehaviour
    {
        [Tooltip("Уровень на который будет совершён переход при взаимодействии")]
        [SerializeField]
        protected Level nextLevel;
        [Tooltip("Режим загрузки объектов, используемый при переходе" +
                 "Если загружается уровень на той же сцене, то рекомендуется использовать IgnoreSceneSave")]
        [SerializeField]
        protected LoadMode loadMode;
        [Tooltip("Точка входа, в которой заспавнится игрок на следующем уровне" +
                 "Отрицательное значение укажет использовать настройки в уровне")]
        [SerializeField]
        protected int pointOfEntryIndex=-1;

        public void SwitchToNewLevel()
        {
            SaveLoadManager.Instance.AutoSave();
            GlobalLevelManager.Instance.GoToLevel(nextLevel,loadMode,pointOfEntryIndex);
            MMAdditiveSceneLoadingManager.LoadScene(nextLevel.LevelScene);
        }
    }   
}
