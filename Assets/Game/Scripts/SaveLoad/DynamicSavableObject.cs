﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using UnityEngine;

public class DynamicSavableObject : MonoBehaviour,MMEventListener<SceneLoadEvent>
{
    public ObjectSaveType saveType;

    private bool isAdded = false;
   

    public void OnMMEvent(SceneLoadEvent eventType)
    {
        if (eventType.eventType == SceneLoadEventType.StartLoad || eventType.eventType == SceneLoadEventType.StartSave)
        {
            if (!isAdded)
            {
                if (saveType == ObjectSaveType.Scene)
                {
                    SaveLoadManager.Instance.CurrentLevelManager.sceneObjectsToSave.Add(gameObject);
                    isAdded = true;
                }
            }
        }
    }
    
    protected void OnEnable()
    {
        this.MMEventStartListening<SceneLoadEvent>();
    }

    protected void OnDisable()
    { 
        this.MMEventStopListening<SceneLoadEvent> ();
    }
}

public enum ObjectSaveType
{
    Scene, Dynamic
}
