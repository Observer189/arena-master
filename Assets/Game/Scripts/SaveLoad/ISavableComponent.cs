﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISavableComponent
{
    long ComponentId
    {
        get;
    }
}
