﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct ComponentSave
{
    public bool isEnabled;
    /// <summary>
    /// Идентификатор, позволяющий различать разные компоненты одного типа
    /// </summary>
    public long componentId;
    /// <summary>
    /// Имя поля, само поле
    /// </summary>
    public Dictionary<string, object> fields;
}
