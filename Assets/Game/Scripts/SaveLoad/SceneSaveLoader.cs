﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Serialization;
using MoreMountains.Tools;
using Newtonsoft.Json;
using UnityEngine;
using Object = System.Object;

public class SceneSaveLoader : MonoBehaviour
{
    public List<GameObject> sceneObjectsToSave;

    private void Awake()
    {
        //SaveLoadManager.Instance.CurrentSceneLoader = this;
    }

    void Start()
    {
        //Debug.Log((HashSet<string>)PlayerData.instance.GetType().GetField("storyMarks").GetValue(PlayerData.instance));
        Debug.Log("Start loader!");
        //LoadScene(GlobalLevelManager.Instance.CurrentGameSave);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /*public GameSave SaveScene()
    {
        SceneLoadEvent.Trigger(SceneLoadEventType.StartSave);
        GameSave save = new GameSave
        {
            str = new[] {"123g,", "bhjdfg", "we324"},
            sceneGameObjects = new List<GameObjectSave>()
        };

        foreach (var sceneObject in sceneObjectsToSave)
        {
            var curObj = SaveGameObject(sceneObject);
            //Debug.Log($"Add {curObj.ToString()}");
            save.sceneGameObjects.Add(curObj);
        }
        SceneLoadEvent.Trigger(SceneLoadEventType.EndSave);
        return save;
    }

    public void LoadScene(GameSave save)
    {
        SceneLoadEvent.Trigger(SceneLoadEventType.StartLoad);
        if (save.sceneGameObjects.Count != sceneObjectsToSave.Count)
        {
            Debug.LogError($"Ошибка! Количество загружаемых сценических объектов({save.sceneGameObjects.Count}) " +
                           $"не равно числу сохраненых({sceneObjectsToSave.Count})!");
        }
        for (int i = 0; i < sceneObjectsToSave.Count; i++)
        {
            LoadGameObjectData(sceneObjectsToSave[i],save.sceneGameObjects[i]);
        }
        SceneLoadEvent.Trigger(SceneLoadEventType.LoadEnd);
        //MMGameEvent.Trigger("inventoryCloses");
    }
    /// <summary>
    /// Преобразует данные gameObject'а в GameObjectSave
    /// </summary>
    protected GameObjectSave SaveGameObject(GameObject gameObject)
    {
        GameObjectSave objectSave = new GameObjectSave
        {
            components = new List<(string, ComponentSave)>(),
            isActive = gameObject.activeSelf,
            transform = new TransformCompSave(gameObject.transform),
            propertyManagerSave = null
        };

        var propManager = gameObject.GetComponent<PropertyManager>();
        if (propManager != null)
        {
            objectSave.propertyManagerSave = new PropertyManagerSave(propManager);
        }

        var comps = gameObject.GetComponents<ISavableComponent>();
        foreach (var comp in comps)
        {
            var mb = (MonoBehaviour) comp;
                
            ComponentSave componentSave = new ComponentSave
            {
                isEnabled = mb.enabled,
                componentId = comp.ComponentId,
                fields = new Dictionary<string, object>()
            };
                    
            var fields = comp.GetType().GetFields(
                BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            foreach (var f in fields)
            {
                if (f.IsDefined(typeof(JsonPropertyAttribute)))
                {
                    //Debug.Log(componentSave.fields);
                    componentSave.fields.Add(f.Name,f.GetValue(comp));
                }
            }
            objectSave.components.Add((comp.GetType().ToString(),componentSave));
                
            //Debug.Log(comp.GetType());
        }

        return objectSave;
    }

   /// <summary>
   /// Загружыет все данные из сохранения в уже существующий GameObject
   /// </summary>
   /// <param name="objectToLoad"></param>
   /// <param name="data"></param>
    protected void LoadGameObjectData(GameObject objectToLoad, GameObjectSave data)
    {
        var savableComps = objectToLoad.GetComponents<ISavableComponent>();
            foreach (var kvPair in data.components)
            {
                foreach (var comp in savableComps)
                {
                    if (comp.GetType().ToString() == kvPair.Item1 && comp.ComponentId == kvPair.Item2.componentId)
                    {
                        var mb = (MonoBehaviour) comp;
                        mb.enabled = kvPair.Item2.isEnabled;

                        foreach (var fieldPair in kvPair.Item2.fields)
                        {
                            //Debug.Log($"Загружаю поле {fieldPair.Key}");
                            if (fieldPair.Value is long temp)
                            {
                                Debug.Log("Ага!");
                                object value;
                                
                                if (temp <= Byte.MaxValue && temp >= Byte.MinValue)
                                    value = Convert.ToByte(fieldPair.Value);
                                else if (temp >= Int16.MinValue && temp <= Int16.MaxValue)
                                    value = Convert.ToInt16(fieldPair.Value);
                                else if (temp >= Int32.MinValue && temp <= Int32.MaxValue)
                                    value = Convert.ToInt32(fieldPair.Value);
                                else
                                    value = temp;
                                
                                comp.GetType()
                                    .GetField(fieldPair.Key,
                                        BindingFlags.Public | 
                                        BindingFlags.NonPublic | 
                                        BindingFlags.Instance)
                                    .SetValue(comp,value);
                            }
                            else
                            {
                                comp.GetType()
                                    .GetField(fieldPair.Key,
                                        BindingFlags.Public | 
                                        BindingFlags.NonPublic | 
                                        BindingFlags.Instance)
                                    .SetValue(comp,fieldPair.Value);   
                            }
                        }
                    }
                }
            }
            //Debug.Log($"Set active {sceneObjectsToSave[i].name} to {save.gameObjects[i].isActive}");
            objectToLoad.transform.position = data.transform.position;
            objectToLoad.transform.rotation = Quaternion.Euler(data.transform.rotation);
            objectToLoad.transform.localScale = data.transform.scale;

            var propManager = objectToLoad.GetComponent<PropertyManager>();
            if (propManager != null)
            {
                propManager.Load(data.propertyManagerSave);
            }

            objectToLoad.SetActive(data.isActive);
    }*/
}
