﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

public class PropertyManagerSave 
{
    public ObjectProperty[] properties;
    [JsonConstructor]
    public PropertyManagerSave(ObjectProperty[] properties)
    {
        this.properties = properties;
    }

    public PropertyManagerSave(PropertyManager propertyManager)
    {
        properties = propertyManager.GetProperties();
    }

    public void Load(PropertyManager propertyManager)
    {
        propertyManager.Load(this);
    }
}
