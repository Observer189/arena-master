﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;
using XNode;

namespace DialogueSystem
{
    public class DialogueTrigger : MonoBehaviour, MMEventListener<DialogueEvent>
    {
        
        [SerializeField] private DialogueGraph dialogue;
  
        private bool allowStartDialog = true;
        [Tooltip("Ссылка на соответсвующий сценический диалог")]
        [SerializeField]
        private DialogueSceneGraph sceneGraph;
        [Tooltip("Следует ли использовать сценический диалог либо обычный")]
        [SerializeField]
        private bool UseSceneGraph=false;

        public void TriggerDialogue()
        {
            if (allowStartDialog)
            {
                if (!UseSceneGraph)
                {
                    DialogueManager.instance.StartDialogue(dialogue);
                }
                else
                {
                    DialogueManager.instance.StartDialogue(sceneGraph.graph);
                }
            }
        }

        public void OnMMEvent(DialogueEvent dEvent)
        {
            switch (dEvent.eventType)
            {
                case DialogueEventType.DialogueOpen:
                   // allowStartDialog = false;
                    break;
                case DialogueEventType.DialogueClose:
                    //allowStartDialog = true;
                    break;
            }
            
        }

        private void OnEnable()
        {
            this.MMEventStartListening<DialogueEvent>();
        }

        private void OnDisable()
        {
            this.MMEventStopListening<DialogueEvent>();
        }
    }
}
