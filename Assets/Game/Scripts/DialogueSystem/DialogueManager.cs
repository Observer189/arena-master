﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Lean.Localization;
using MoreMountains.Feedbacks;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;
using UnityEngine.UI;
using XNode;


namespace DialogueSystem
{
    public class DialogueManager : MonoBehaviour
    {

        /// <summary>
        /// Синглтон паттерн
        /// </summary>
        public static DialogueManager instance;

        [SerializeField] private RectTransform dialogueView;

        [Tooltip("Ссылка на поле, куда выводятся все произносимые фразы")] [SerializeField]
        private Text messageText;
        [Tooltip("Ссылка на текстовое поле имени правого собеседника")]
        [SerializeField]
        protected Text rightActorName;
        [Tooltip("Ссылка на аватарку правого собеседника")]
        [SerializeField]
        protected Image rightActorAvatar;
        
        [Tooltip("Ссылка на текстовое поле имени левого собеседника")]
        [SerializeField]
        protected Text leftActorName;
        [Tooltip("Ссылка на аватарку левого собеседника")]
        [SerializeField]
        protected Image leftActorAvatar;

        [Tooltip("Ссылка на панель ответов")] 
        [SerializeField]
        protected AnswersPanel answersPanel;
        
        [Tooltip("Актер игрока. Всегда неявно инициализируется в диалоге")]
        [SerializeField]
        protected Actor playerActor;

        [Header("TypeWriter settings")] [Tooltip("Скорость распечатки сообщений")] [SerializeField]
        private float typeWriterSpeed = 20f;

        [Tooltip("Ожидание в секундах перед началом печати")] [SerializeField]
        protected float startDelay = 0.5f;

        [Tooltip("Задержка между завершением печати и возможностью перейти к следующему сообщению")] [SerializeField]
        protected float preEndDelay = 0.5f;

        [Tooltip("Задержка после окончания написания сообщения")] [SerializeField]
        protected float endDelay = 1f;

        /// <summary>
        /// Нода, обрабатываемая в данный момент
        /// </summary>
        protected Node currentNode;

        /// <summary>
        /// Нода, которую надо обработать следующей
        /// </summary>
        protected Node nextNode;
        
        /// <summary>
        /// Список актеров в текущем диалоге
        /// </summary>
        protected List<Actor> actors;
        /// <summary>
        /// Варианты ответов, закэшированные через CashAnswerNode
        /// </summary>
        protected List<(Node, Answer)> cashedAnswers;

        protected CanvasGroup dialogueHider;

        protected bool isDialogueOpen;

        public bool IsDialogueOpen => isDialogueOpen;

        private void Awake()
        {
            instance = this;
            dialogueHider = GetComponent<CanvasGroup>();
            cashedAnswers=new List<(Node, Answer)>();
            HideDialogueView();
            actors=new List<Actor>();
            actors.Add(playerActor);
        }

        void Start()
        {
          
        }

        // Update is called once per frame
        void Update()
        {

        }

        /// <summary>
        /// Инициирует диалог
        /// </summary>
        /// <param name="dialogue"></param>
        public void StartDialogue(DialogueGraph dialogue)
        {
            ShowDialogueView();


            StartCoroutine(ProcessTheDialogue(dialogue));
        }

        protected IEnumerator ProcessTheDialogue(DialogueGraph dialogue)
        {
            currentNode = dialogue.GetFirstNode();
            while (currentNode != null)
            {
                yield return StartCoroutine(ProcessCurrentNode());
                currentNode = nextNode;
            }

            HideDialogueView();
        }

        protected IEnumerator ProcessCurrentNode()
        {
            switch (currentNode)
            {
                case MessageNode msg:
                    yield return StartCoroutine(ProcessMessageNode(msg));
                    break;
                case InitializeActorsNode node:
                    yield return StartCoroutine(ProcessInitializeActorsNode(node));
                    break;
                case ChoiceNode node:
                    yield return StartCoroutine(ProcessChoiceNode(node));
                    break;
                case StoryMarksNode node:
                    yield return StartCoroutine(ProcessStoryMarksNode(node));
                    break;
                case CheckNode node:
                    yield return StartCoroutine(ProcessCheckNode(node));
                    break;
                case RemoveItemsNode node:
                    yield return StartCoroutine(ProcessRemoveItemsNode(node));
                    break;
                case EffectsNode node:
                    yield return StartCoroutine(ProcessEffectsNode(node));
                    break;
                case PlayFeedbackNode node:
                    yield return StartCoroutine(ProcessPlayFeedbackNode(node));
                    break;
                case CashAnswerNode node:
                    yield return StartCoroutine(ProcessCashAnswerNode(node));
                    break;
                case SaveNode node:
                    yield return StartCoroutine(ProcessSaveNode(node));
                    break;
                case PlayFeedbackByTag node:
                    yield return StartCoroutine(ProcessPlayFeedbackByTagNode(node));
                    break;
            }
        }

        protected IEnumerator ProcessMessageNode(MessageNode node)
        {
            nextNode = node.GetNextNode();
            var actor = GetActorByName(node.ActorName);
            if(actor==null) Debug.LogError($"Актера с именем *{node.ActorName}* не существует");

            string actorName = actor.nameTrans.GetTranslation(LeanLocalization.GetFirstCurrentLanguage());
            if (actorName == null) actorName = actor.name;
            leftActorName.text = actorName;
            leftActorAvatar.sprite = actor.avatar;

            var str = node.trans.GetTranslation(LeanLocalization.GetFirstCurrentLanguage());
            if (str == null) str = node.message;
            
            dialogueHider.alpha = 1;
            
            yield return StartCoroutine(TypeMessage(str));
            
            yield return new WaitForSeconds(preEndDelay);

            float timer = 0f;
            while (!(Input.GetAxis("Submit") > 0) && timer < endDelay)
            {
                timer += Time.deltaTime;
                yield return null;
            }
        }

        protected IEnumerator ProcessInitializeActorsNode(InitializeActorsNode node)
        {
            Debug.Log(node.initializableActors.Length);
            for (int i = 0; i < node.initializableActors.Length; i++)
            {
                actors.Add(node.initializableActors[i]);
                Debug.Log($"{node.initializableActors[i].name} added");
            }

            nextNode = node.GetNextNode();
            yield break;
        }

        protected IEnumerator ProcessChoiceNode(ChoiceNode node)
        {
            List<Answer> tempAnswerList = new List<Answer>(node.answers);
            tempAnswerList.AddRange(cashedAnswers.ConvertAll(t=>t.Item2));
            answersPanel.SetAnswers(tempAnswerList);
            yield return new WaitWhile(()=>answersPanel.IsActive);
            int choice = answersPanel.SelectionPointer;
            //Debug.Log($"Choice = {choice}");
            if (choice >= node.answers.Count)
            {
                nextNode = cashedAnswers[choice - node.answers.Count].Item1;
            }
            else
            {
                nextNode=node.GetNodeByAnswer(choice);   
            }
            cashedAnswers.Clear();
        }

        protected IEnumerator ProcessCashAnswerNode(CashAnswerNode node)
        {
            cashedAnswers.Add((node.GetAnswerNode(),node.answer));
            nextNode = node.GetNextNode();
            yield break;
        }

        protected IEnumerator ProcessStoryMarksNode(StoryMarksNode node)
        {
            PlayerData.instance.AddStoryMarks(node.MarksToAdd);
            PlayerData.instance.RemoveStoryMarks(node.MarksToRemove);
            nextNode = node.GetNextNode();
            yield break;
        }

        protected IEnumerator ProcessCheckNode(CheckNode node)
        {
            bool flag = true;
            switch (node)
            {
                case StoryMarksCheckNode marksNode:
                    flag = !PlayerData.instance.HasAtLeastOneMark(marksNode.ForbiddenMarks) &&
                           PlayerData.instance.HasStoryMarks(marksNode.RequiredMarks);
                    break;
                case InventoryCheckNode checkNode:
                    flag = PlayerData.instance.inventory.HasItems(checkNode.requiredItems);
                    break;
                case AddItemsNode addNode:
                    flag = PlayerData.instance.inventory.CheckPossibilityToAddItems(addNode.itemsToAdd,
                        addNode.AddIfPossible);
                    MMGameEvent.Trigger("PlayerInventoryUpdate");
                    if (addNode.AddIfPossible && addNode.ShowNotification)
                    {
                        foreach (var pair in addNode.itemsToAdd)
                        {
                            var it = pair.item.initializeItem();
                            Notifier.instance.ShowAddOrRemoveItemNotification(it,pair.quantity,true);
                        }
                    }
                    break;
                case PropertyCheckNode checkNode:
                    var property = PlayerData.instance.PropertyManager.GetPropertyById(checkNode.PropertyId);
                    var propertyValue = (checkNode.CompareToCurrent) ? property.GetCurValue() : property.Value;
                    switch (checkNode.comparisonMode)
                    {
                        case AIDecisionHealth.ComparisonModes.StrictlyLowerThan:
                            flag = propertyValue < checkNode.Value;
                            break;
                        case AIDecisionHealth.ComparisonModes.LowerThan:
                            flag = propertyValue <= checkNode.Value;
                            break;
                        case AIDecisionHealth.ComparisonModes.Equals:
                            flag = Mathf.Approximately(propertyValue, checkNode.Value);
                            break;
                        case AIDecisionHealth.ComparisonModes.GreatherThan:
                            flag = propertyValue >= checkNode.Value;
                            break;
                        case AIDecisionHealth.ComparisonModes.StrictlyGreaterThan:
                            flag = propertyValue > checkNode.Value;
                            break;
                        
                    }
                    break;
            }

            nextNode = node.GetNextNodeByCheck(flag);
            yield break;
        }

        protected IEnumerator ProcessRemoveItemsNode(RemoveItemsNode node)
        {
            if (node.showNotification)
            {
                int[] q=new int[node.itemsToRemove.Length];
                for (int i = 0; i < q.Length; i++)
                {
                    q[i] = PlayerData.instance.inventory.GetItemQuantity(node.itemsToRemove[i].item.initializeItem().Id);
                }
                PlayerData.instance.inventory.RemoveItems(node.itemsToRemove);   
                for (int i = 0; i < q.Length; i++)
                {
                    var it = node.itemsToRemove[i].item.initializeItem();
                    int n = PlayerData.instance.inventory.GetItemQuantity(it.Id);
                    Notifier.instance.ShowAddOrRemoveItemNotification(it,q[i]-n,false);
                }
            }
            else
            {
                PlayerData.instance.inventory.RemoveItems(node.itemsToRemove);   
            }
            MMGameEvent.Trigger("PlayerInventoryUpdate");
            nextNode = node.GetNextNode();
            yield break;
        }

        protected IEnumerator ProcessEffectsNode(EffectsNode node)
        {
            for (int i = 0; i < node.effects.Length; i++)
            {
                PlayerData.instance.PropertyManager.AddEffect(new Effect(node.effects[i]));
            }
            nextNode = node.GetNextNode();
            yield break;
        }

        protected IEnumerator ProcessPlayFeedbackNode(PlayFeedbackNode node)
        {
            node.Feedback.PlayFeedbacks();
            yield return new WaitForSeconds(node.WaitingDuration);
            nextNode = node.GetNextNode();
        }
        
        protected IEnumerator ProcessPlayFeedbackByTagNode(PlayFeedbackByTag node)
        {
            var objs = SearchSystem.FindGameObjectsWithTag(node.tag);
            foreach (var obj in objs)
            {
                obj.GetComponent<MMFeedbacks>().PlayFeedbacks();
            }
            yield return new WaitForSeconds(node.WaitingDuration);
            nextNode = node.GetNextNode();
        }

        protected IEnumerator TypeMessage(string message)
        {
            messageText.text = "";
            yield return new WaitForSeconds(startDelay);
            float t = 0;
            int charIndex = 0;

            while (charIndex < message.Length && !(Input.GetAxis("Submit") > 0))
            {
                t += Time.deltaTime * typeWriterSpeed;

                charIndex = Mathf.FloorToInt(t);

                messageText.text = message.Substring(0, charIndex);

                yield return null;
            }

            messageText.text = message;

        }

        protected IEnumerator ProcessSaveNode(SaveNode node)
        {
            if (node.isAutosave)
            {
                SaveLoadManager.Instance.AutoSave();
            }
            else
            {
                SaveLoadManager.Instance.SaveGame(node.SaveName);
            }
            var notText = LeanLocalization.GetTranslationText("Game saved!","Game saved!");
            Notifier.instance.ShowNotification(notText,NotificationType.Save);
            nextNode = node.GetNextNode();
            yield break;
        }

        protected void ShowDialogueView()
        {
            dialogueView.gameObject.SetActive(true);
            //dialogueHider.alpha = 1;
            isDialogueOpen = true;
            InputManager.Instance.InputDetectionActive = false;
            DialogueEvent.Trigger(DialogueEventType.DialogueOpen);
        }

        protected void HideDialogueView()
        {
            dialogueHider.alpha = 0;
            isDialogueOpen = false;
            cashedAnswers.Clear();
            dialogueView.gameObject.SetActive(false);
            InputManager.Instance.InputDetectionActive = true;
            DialogueEvent.Trigger(DialogueEventType.DialogueClose);
        }

        protected Actor GetActorByName(string name)
        {
            for (int i = 0; i < actors.Count; i++)
            {
                if (actors[i].name == name)
                {
                    return actors[i];
                }
            }

            return null;
        }
        
    }
}
