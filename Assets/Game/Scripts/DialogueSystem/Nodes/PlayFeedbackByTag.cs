﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayFeedbackByTag : DialogueBaseNode
{
    [Tooltip("Тэг, объекта, зарегистрированного в SearchSystem, на котором лежит нужный фидбэк")]
    public string tag;

    public float WaitingDuration;
}
