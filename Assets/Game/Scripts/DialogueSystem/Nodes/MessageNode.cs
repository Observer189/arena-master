﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean.Localization;
using MoreMountains.Tools;
using UnityEngine;
using UnityEngine.Serialization;
using XNode;
[NodeTint("#00C12B")]
public class MessageNode : DialogueBaseNode
{
	[Tooltip("Имя актера, который произносит текущее сообщение")]
	public string ActorName;

	[TextArea(5,5)]
	public string message;

	[FormerlySerializedAs("str")] public LocalizedString trans;
	
}