﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Feedbacks;
using UnityEngine;

public class PlayFeedbackNode : DialogueBaseNode
{
    [Tooltip("Feedback, который будет проигран в диалоге")]
    public MMFeedbacks Feedback;
    [Tooltip("Сколько секунд нужно ждать на этой ноде")]
    public float WaitingDuration;
}
