﻿using System.Collections;
using System.Collections.Generic;
using InventorySystem;
using UnityEngine;
[CreateNodeMenu("Checks/InventoryCheck")]
[NodeTint("#F9DA6B")]
public class InventoryCheckNode : CheckNode
{
   [Tooltip("Предметы необходимые для прохождения проверки")]
   public InventoryController.ItemQuantityPair[] requiredItems;
}
