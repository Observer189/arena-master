﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.TopDownEngine;
using UnityEngine;
[CreateNodeMenu("Checks/PropertyCheck")]
[NodeTint("#21702f")]
public class PropertyCheckNode : CheckNode
{
   [Tooltip("Id свойства которое требуется сравнить")]
   public int PropertyId;
   [Tooltip("Способ сравнения с значением")]
   public AIDecisionHealth.ComparisonModes comparisonMode;
   [Tooltip("Значение с которым будет проводиться сравнение")]
   public float Value;
   [Tooltip("Должно ли сравнение производиться с обычным значением свойства или с текущим")]
   public bool CompareToCurrent;
}


