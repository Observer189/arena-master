﻿using System.Collections;
using System.Collections.Generic;
using InventorySystem;
using UnityEngine;
[CreateNodeMenu("InventoryActions/AddItems")]
public class AddItemsNode : CheckNode
{
    [Tooltip("Предметы, которые нужно добавить в инвентарь")]
    public InventoryController.ItemQuantityPair[] itemsToAdd;
    [Tooltip("Должны ли эти предметы быть добавлены если это возможно или же стоит просто проверить возможность" +
             "их добавления")]
    public bool AddIfPossible=true;
    [Tooltip("Показать уведомление о добавлении предметов, если оно произошло?")]
    public bool ShowNotification;
}
