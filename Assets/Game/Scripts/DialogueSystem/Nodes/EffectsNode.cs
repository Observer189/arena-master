﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[NodeTint("#eb0c0c")]
public class EffectsNode : DialogueBaseNode
{
    /// <summary>
    /// Список эффектов, которые будут наложены на игрока
    /// </summary>
    public EffectDescription[] effects;
}
