﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[NodeTint("#5b00c1")]
public class InitializeActorsNode : DialogueBaseNode
{
    public Actor[] initializableActors;
}
