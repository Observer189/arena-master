﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[NodeTint("#FFFFFF")]
public class SaveNode : DialogueBaseNode
{
    [Tooltip("Название сохранения")]
    public string SaveName;
    
    public bool isAutosave;

}
