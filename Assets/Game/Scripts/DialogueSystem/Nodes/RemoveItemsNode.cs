﻿using System.Collections;
using System.Collections.Generic;
using InventorySystem;
using UnityEngine;
[CreateNodeMenu("InventoryActions/RemoveItems")]
[NodeTint("#E5284E")]
public class RemoveItemsNode : DialogueBaseNode
{
    [Tooltip("Список предметов на удаление")]
    public InventoryController.ItemQuantityPair[] itemsToRemove;
    [Tooltip("Показывать ли уведомление о получении предмета?")]
    public bool showNotification;
    
}
