﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.TopDownEngine;
using UnityEngine;
using MoreMountains.Feedbacks;

public class Shield : MonoBehaviour
{
    public Character Owner;
    public AITeam Team;
    private ObjectProperty shieldPower;

    private SpriteRenderer renderer;
    private Collider2D collider;

    public MMFeedbacks shieldDestruction;
    public MMFeedbacks hitShield;
    public MMFeedbacks chargeShield;

    private void Awake()
    {
        renderer = GetComponent<SpriteRenderer>();
        collider = GetComponent<Collider2D>();
    }

    public void Initialize(Character owner, ObjectProperty shieldPowerProp)
    {
        renderer = GetComponent<SpriteRenderer>();
        collider = GetComponent<Collider2D>();
        Owner = owner;
        shieldPower = shieldPowerProp;
        Team = owner.GetComponent<AITeam>();
        shieldPower.RegisterChangeCallback(OnShieldPowerChanged);

        UpdateShield(shieldPower.GetCurValue());
    }

    public void HitShield(float damage)
    {
        if(shieldPower==null) Debug.LogError("Ошибка! Щит не инициализирован!");

        hitShield?.PlayFeedbacks();
        shieldPower.ChangeCurValue(-damage);
    }

    protected void OnShieldPowerChanged(float oldCurValue, float newCurValue, float oldValue, float newValue)
    {
        if (oldCurValue <= 0 && newCurValue > 0)
        {
            chargeShield?.PlayFeedbacks();
        }
        if (oldCurValue > 0 && newCurValue <= 0)
        {
            shieldDestruction?.PlayFeedbacks();
        }
        UpdateShield(newCurValue);
    }

    private void UpdateShield(float shieldCurValue)
    {
        if (shieldCurValue <= 0)
        {         
            renderer.enabled = false;
            collider.enabled = false;
         
        }
        else
        {          
            renderer.enabled = true;
            collider.enabled = true;
        }
    }

    private void OnEnable()
    {
        shieldPower?.RegisterChangeCallback(OnShieldPowerChanged);
    }

    private void OnDisable()
    {
        shieldPower?.UnRegisterCallback(OnShieldPowerChanged);
    }
}
