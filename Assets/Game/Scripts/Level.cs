﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Level", menuName = "Level", order = 10)]
public class Level : ScriptableObject
{ 
    [Tooltip("Имя уровня, отображаемое в игре")]
    [SerializeField]
    protected string levelName;
   [Tooltip("Имя сцены, на которой проходит уровень")]
   [SerializeField]
   protected string levelScene;

   [Tooltip("Позиция в которой спавнится игрок на уровне")] 
   [SerializeField]
   protected int startPointOfEntryIndex;
   [Tooltip("Если установлен этот флаг, то для спавна будет взята позиция указанная на сцене," +
            "а не здесь")]
   [SerializeField]
   protected bool useSceneSpawnPosition;
   [Tooltip("Префаб персонажа игрока для уровня")]
   [SerializeField]
   protected GameObject playerCharacterPrefab;
   [Tooltip("Если этот флаг установлен, то будет использоваться персонаж заданный на сцене")]
   [SerializeField]
   protected bool useSceneCharacter;
   [Tooltip("Музыка для данного уровня")]
   [SerializeField]
   protected AudioClip levelMusic;
   [Tooltip("Музыка играемая после боя")]
   [SerializeField]
   protected AudioClip afterBattleMusic;

   public string LevelName => levelName;

   public string LevelScene => levelScene;

   public int StartPointOfEntryIndex => startPointOfEntryIndex;

   public bool UseSceneCharacter => useSceneCharacter;

   public bool UseSceneSpawnPosition => useSceneSpawnPosition;

   public GameObject PlayerCharacterPrefab => playerCharacterPrefab;
}
