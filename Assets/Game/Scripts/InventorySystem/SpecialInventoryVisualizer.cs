﻿using System.Collections;
using System.Collections.Generic;
using InventorySystem;
using MoreMountains.Tools;
using UnityEngine;
using UnityEngine.UI;

public class SpecialInventoryVisualizer : InventoryVisualizer
{
    /// <summary>
    /// Ссылка на объект отображающий название инвентаря
    /// </summary>
    [SerializeField] private Text title;
    
    /// <summary>
    /// Canvas group этого объекта, используется для сокрытия инвентаря
    /// </summary>
    protected CanvasGroup canvasGroup;
    
    /// <summary>
    /// Список слотов
    /// </summary>
    [SerializeField]
    protected List<SlotVisualizer> slotList;

    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        canvasGroup.alpha = 0f;
    }
    
    public override void Visualize(Inventory inventory)
    {
        StartCoroutine(MMFade.FadeCanvasGroup(canvasGroup, 0.2f, 1f));
        SetInventory(inventory);
        RebuildInventory();
    }

    public override void RebuildInventory()
    {
        base.RebuildInventory();
        title.text = Lean.Localization.LeanLocalization.GetTranslationText(inventory.Name,inventory.Name);
        for (int i = 0; i < inventory.Size; i++)
        {
            slotList[i].Initialize(this,i);
            slotList[i].SetItem(inventory.GetItem(i));
        }
    }
    
    /// <summary>
    /// Отправляет запрос в InventoryManager на то, чтобы все предметы из контейнера были перемещены в инвентарь игрока
    /// </summary>
    public void TakeAll()
    {
        InventoryManager.instance.TakeAll(this);
    }

    public override void Close()
    {
        StartCoroutine(MMFade.FadeCanvasGroup(canvasGroup, 0.2f, 0f));
        InventoryManager.instance.CloseVisualizer(this);
    }
}
