﻿using System;
using System.Collections;
using System.Collections.Generic;
using InventorySystem;
using JetBrains.Annotations;
using Lean.Localization;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(CanvasGroup))]
public class ItemDivisionPanel : MonoBehaviour
{
    [SerializeField] private Text itemNameText;

    [SerializeField] private InputField editQuantity1;
    [SerializeField] private InputField editQuantity2;

    [SerializeField] private Slider slider;

    private CanvasGroup hider;
    /// <summary>
    /// Становится true, когда нажимаем кнопку завершения деления
    /// Является условием выхода из окна деления
    /// </summary>
    private bool isButtonPressed=false;

    private bool wasCanceled = false;

    private Item curIt;
    
    private int q1;

    private int q2;

    private int totalQ;
    // Start is called before the first frame update
    private void Awake()
    {
        hider = GetComponent<CanvasGroup>();
        slider.minValue = 0;
    }
    /// <summary>
    /// Реакция на ввод количества 1-ого предмета
    /// </summary>
    public void WriteQ1()
    {
        var f = int.TryParse(editQuantity1.text, out int parsedValue);
        if (!f)
        {
            editQuantity1.text = q1.ToString();
            return;
        }

        int max = curIt.MaxStackCount;
        int min = max - Mathf.RoundToInt(slider.maxValue);
        slider.value= max-Mathf.Clamp(parsedValue, min, max);
        UpdateUI();
    }
    /// <summary>
    /// Реакция на ввод количества 2-ого предмета
    /// </summary>
    public void WriteQ2()
    {
        var f = int.TryParse(editQuantity1.text, out int parsedValue);
        if (!f)
        {
            editQuantity1.text = q1.ToString();
            return;
        }

        int max = curIt.MaxStackCount;
        int min = max - Mathf.RoundToInt(slider.maxValue);
        slider.value= Mathf.Clamp(parsedValue, min, max)-min;
        UpdateUI();
    }

    public void OnSlide()
    {
        q1 = Mathf.Min(curIt.MaxStackCount,totalQ) - Mathf.RoundToInt(slider.value);
        q2 = totalQ - q1;
        UpdateUI();
    }

    /// <summary>
    /// Вызывается при нажатии клавиши ok
    /// </summary>
    public void OnPressOk()
    {
        isButtonPressed = true;
    }

    public IEnumerator InitializeDivision(Item item1,Item item2,Ref<int> q1,Ref<int> q2)
    {
        curIt = item1;
        this.q1 = item1.Quantity;
        this.q2= item2?.Quantity ?? 0;
        q1.value = this.q1;
        q2.value = this.q2;
        totalQ = this.q1 + this.q2;
//        Debug.Log($"TotalQ= {totalQ}");
        this.q1 = totalQ / 2;
        this.q2 = totalQ - this.q1;
        int stepCount = ((totalQ >= item1.MaxStackCount) ? 2 * item1.MaxStackCount - totalQ : totalQ) + 1;
        slider.maxValue = stepCount-1;
        slider.value = stepCount / 2;
        UpdateUI();
        Show();
        yield return new WaitUntil(()=>isButtonPressed);
        if (!wasCanceled)
        {
            q1.value = this.q1;
            q2.value = this.q2;
        }

        Hide();
    }

    private void UpdateUI()
    {
        itemNameText.text = LeanLocalization.GetTranslationText(curIt.Name,curIt.Name);
        editQuantity1.text = this.q1.ToString();
        editQuantity2.text = this.q2.ToString();
    }
    /// <summary>
    /// Отменяет разделение
    /// </summary>
    public void CancelDivision()
    {
        wasCanceled = true;
        isButtonPressed = true;
    }

    public void Show()
    {
        hider.alpha = 1;
        isButtonPressed = false;
        wasCanceled = false;
    }

    public void Hide()
    {
        hider.alpha = 0;
        isButtonPressed = false;
        wasCanceled = false;
    }
}
