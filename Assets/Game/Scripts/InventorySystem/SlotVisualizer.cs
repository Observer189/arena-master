﻿using System;
using System.Collections;
using System.Collections.Generic;
using InventorySystem;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SlotVisualizer : MonoBehaviour,IPointerEnterHandler,
    IPointerExitHandler,IPointerDownHandler,
    IPointerUpHandler,IBeginDragHandler,
    IEndDragHandler,IDragHandler,IPointerClickHandler
{
   /// <summary>
   /// Картинка в обычном состоянии
   /// </summary>
   [SerializeField]
    private Sprite defaultImage;
   /// <summary>
   /// Картинка при наведенном курсоре
   /// </summary>
   [SerializeField] 
   private Sprite hoveredImage;
   
   /// <summary>
   /// Картинка при передвигаемом предмете
   /// </summary>
   [SerializeField] 
   private Sprite movedImage;
   /// <summary>
   /// Картинка, которая используется, когда к этому слоту подходит выбранный предмет,
   /// например если мы выбрали модуль на оружие то в визуализаторе оружия это будет отображаться
   /// </summary>
   [SerializeField]
   private Sprite compareImage;
   
   /// <summary>
   /// Изображение самого предмета
   /// </summary>
   [SerializeField]
   private Image itemImage;
   /// <summary>
   /// Компонент отображения количества итемов в слоте
   /// </summary>
   [SerializeField] 
   private Text itemCountText;
 /// <summary>
 /// Звук исполняемый при наведении курсора на слот
 /// </summary>
   [SerializeField] 
   private AudioClip onHoverSound;
    /// <summary>
    /// Компонент image на данном объекте
    /// </summary>
    private Image image;
    /// <summary>
    /// Текущее состояние
    /// </summary>
    private InputState state;
    
   
    /// <summary>
    /// Сам предмет
    /// </summary>
    private Item item;

    public Item Item => item;
    /// <summary>
    /// Ссылка на визуализатор к которому онтносится слот
    /// </summary>
    private InventoryVisualizer visualizer; 
    /// <summary>
    /// Номер item'a в инвентаре
    /// </summary>
    private int pos;
    private void Awake()
    {
        image = GetComponent<Image>();
        if (image == null)
        {
            image = gameObject.AddComponent<Image>();
        }
        itemImage.gameObject.SetActive(false);
        itemCountText.gameObject.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        image.sprite = defaultImage;
        
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void UpdateState()
    {
        if (state == InputState.Hovered)
        {
            if(hoveredImage!=null)
            image.sprite = hoveredImage;
        }
        else if(state == InputState.Moved)
        {
            if(movedImage!=null)
            image.sprite = movedImage;
        }
        else
        {
            image.sprite = defaultImage;
        }
    }
    /// <summary>
    /// Когда слот подходит для установки модуля мы его подсвечиваеим
    /// </summary>
    public void StartCompare()
    {
        if (compareImage != null)
        {
            image.sprite = compareImage;
        }
        else if(hoveredImage!=null)
        {
            image.sprite = hoveredImage;
        }
    }
    /// <summary>
    /// Когда слот перестает подходить для сравнения с выбранным предметом
    /// </summary>
    public void EndCompare()
    {
        UpdateState();
    }

    public void SetItem(Item item)
    {
        this.item = item;
        DisplayCurItem();
    }
    /// <summary>
    /// Информация передаваемая в этом методе не будет меняться до вызова RebuildInventory
    /// </summary>
    /// <param name="visualizer"></param>
    public void Initialize(InventoryVisualizer visualizer,int pos)
    {
        this.visualizer = visualizer;
        this.pos = pos;
    }

    /// <summary>
    /// Отображает всю информацию связанную с текущим предметом
    /// </summary>
    void DisplayCurItem()
    {
        if (item != null)
        {
            itemImage.gameObject.SetActive(true);
            itemImage.sprite = item.Icon;
            if (item.IsStackable)
            {
                itemCountText.gameObject.SetActive(true);
                itemCountText.text = item.Quantity.ToString();
            }
        }
        else
        {
            itemImage.sprite = null;
            itemImage.gameObject.SetActive(false);
            itemCountText.gameObject.SetActive(false);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (state == InputState.Default)
        {
            state = InputState.Hovered;
            UpdateState();
            InventoryManager.instance.SetSelectedSlotData(pos,visualizer,transform.position);
            if (onHoverSound != null)
            {
                MMSoundManagerSoundPlayEvent.Trigger(onHoverSound, MMSoundManager.MMSoundManagerTracks.UI,
                    transform.position, false, 0.4f);
            }
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (state == InputState.Hovered)
        {
            state = InputState.Default;
            UpdateState();
            InventoryManager.instance.OnUnselectSlot();
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (item != null && state != InputState.Moved && eventData.button==PointerEventData.InputButton.Left)
        {
            state = InputState.Moved;
            UpdateState();
            InventoryManager.instance.OnDragBegin(item.Icon);
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (state == InputState.Moved)
        {
            if (eventData.hovered.Contains(gameObject))
            {
                state = InputState.Hovered;
            }
            else
            {
                state = InputState.Default;
                
            }
            StartCoroutine(InventoryManager.instance.OnDragEnd(pos,visualizer));
            UpdateState();
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
       
    }

    private void OnDisable()
    {
        state = InputState.Default;
        UpdateState();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            StartCoroutine(InventoryManager.instance.OnItemRightClick());
        }
    }
}
/// <summary>
/// default - состояние по умолчанию
/// hovered - когда курсор находится над слотом
/// moved - когда предмет слота перемещают
/// </summary>
enum InputState
{
    Default,Hovered,Moved
}
