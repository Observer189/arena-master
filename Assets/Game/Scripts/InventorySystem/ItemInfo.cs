﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using InventorySystem;
using UnityEngine;

namespace InventorySystem
{
        
        [CreateAssetMenu(fileName = "New Item", menuName = "Items/Item", order = 1)]
        public class ItemInfo : ScriptableObject
        {
                /// <summary>
                ///Имя предмета отображаемое в инвентаре 
                /// </summary>
                [SerializeField] protected string name;
                [Tooltip("Путь к info - файлу")]
                [SerializeField] protected string path;

                /// <summary>
                /// Уникальный идентификатор предмета 
                /// </summary>
                [SerializeField] protected int id;
                
                /// <summary>
                /// Иконка объекта в инвентаре
                /// </summary>
                [SerializeField] protected Sprite icon;

                /// <summary>
                /// Отображаемое описание объекта
                /// </summary>
                [TextArea] [SerializeField] protected string description;
                
                [Tooltip("Примерная стоимость предмета при покупке/продаже за единицу")]
                [SerializeField] protected float estimatedCost;

                /// <summary>
                /// Может ли предмет стакаться или является уникальным
                /// </summary>
                [SerializeField] protected bool isStackable;

                /// <summary>
                /// Максимальное количество одинаковых итемов, помещающихся в одну ячейку инвентаря
                /// </summary>
                [SerializeField] protected int maxStackCount;
                /// <summary>
                /// Список тэгов предмета
                /// Служат для проверки возможности установки итема в слот
                /// </summary>
                [SerializeField] protected string[] tags;

                /// <summary>
                /// Тип модуля
                /// </summary>
                [SerializeField] protected ItemType type;

                public string Name => name;

                public string Path => path;

                public int Id => id;

                public Sprite Icon => icon;

                public string Description => description;

                public float EstimatedCost => estimatedCost;

                public bool IsStackable => isStackable;

                public int MaxStackCount => maxStackCount;

                public string[] Tags => tags;

                public ItemType Type => type;
                public virtual Item initializeItem()
                {
                        return new Item(name, path,id, icon, description, type, isStackable,tags, estimatedCost,(isStackable)?maxStackCount:1);
                }

                [ContextMenu("GeneratePaths")]
                protected virtual void GeneratePaths()
                {
                        var infos = Resources.FindObjectsOfTypeAll<ItemInfo>();
                }
        }
}
