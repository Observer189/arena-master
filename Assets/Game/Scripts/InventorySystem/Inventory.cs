﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;

namespace InventorySystem
{
    [DataContract]
    public class Inventory
    {
        /// <summary>
        /// Название инвентаря
        /// </summary>
        [JsonProperty]
        protected string name;
        /// <summary>
        /// Максимальное количесвто ячеек инвентаря
        /// </summary>
        [JsonProperty]
        protected int size;
        /// <summary>
        /// Должен ли инвентарь автоматически расширятьсяЮ если в нем не хватает места
        /// на добавление предмета?
        /// </summary>
        [JsonProperty]
        protected bool isAutoExtendable;
        /// <summary>
        /// количество свободных ячеек
        /// </summary>
        protected int emptyCellCount;
        /// <summary>
        /// Массив всех элементов
        /// </summary>
        [JsonProperty]
        protected Item[] items;
        /// <summary>
        /// Насколько слотов нужно увеличивать инвентарь при автоматическом расширении
        /// </summary>
        private const int AutoExtensionSize = 4;
        /// <summary>
        /// Сколько минимум может быть свободных клеток до автоматического расширения
        /// </summary>
        private const int AutoExtensionCondition=2;
        /// <summary>
        /// тип визуализатора подходящий для этого инвентаря
        /// </summary>
        [JsonProperty]
        protected VisualizerType visualizerType;

        public string Name => name;
        public int Size => size;

        public VisualizerType VisualizerType => visualizerType;
        
        public Item[] Items => items;

        /*public Inventory()
        {
            
        }*/
        
        public Inventory(string name,int size,bool isAutoExtendable,VisualizerType type)
        {
            this.name = name;
            this.size = size;
            this.isAutoExtendable = isAutoExtendable;
            if(size<0) Debug.LogError("Нельзя создавать инвентарь с отрицательным размером");
            items=new Item[size];
            emptyCellCount = size;
            visualizerType = type;
        }
        /// <summary>
        /// Добавляет в инвентарь заданное число заданных item'ов
        /// </summary>
        /// <param name="item">Предмет который будет добавлен</param>
        /// <param name="quantity">количество добавленных предметов</param>
        /// <returns>возвращает количество предметов которых не удалось добавить</returns>
        public virtual int AddItem(Item item,int quantity)
        {
            if (item == null || quantity<=0) return -1;
            int rq = quantity;
            if (item.IsStackable && item.MaxStackCount > 1)
            {
                List<int> slots = GetSlotsById(item.Id);
                for (int i = 0; i < slots.Count; i++)
                {
                    Item cur = items[slots[i]];
                    int restCur = cur.MaxStackCount - cur.Quantity; //количество свободного места в стаке
                    if (rq > restCur)
                    {
                        cur.SetQuantity(cur.MaxStackCount);
                        rq -= restCur;
                    }
                    else
                    {
                        cur.SetQuantity(cur.Quantity + rq);
                        return 0;
                    }
                }
            }
            while (rq>0)
            {
                int cell = GetEmptyCell();
                if (cell == -1)
                {
                    if (isAutoExtendable)
                    {
                        Debug.Log("Extend");
                        Extend(4);
                        continue;
                    }
                    return rq;
                }
                Item newItem = item.getCopy();
                var curQ = Mathf.Min(rq, newItem.MaxStackCount);
                newItem.SetQuantity(curQ);
                items[cell] = newItem;
                ReduceEmptyCellCount(1);
                rq -= curQ;
            }

            return 0;
           
        }
        /// <summary>
        /// Проверяет возможность добавления набора предметов в инвентарь
        /// </summary>
        /// <param name="pairs">набор предметов</param>
        /// <param name="addIfPossible">Если true, то по итогу выполнения функции предметы будут добавлены
        /// если это таки возможно</param>
        /// <returns></returns>
        public bool CheckPossibilityToAddItems(InventoryController.ItemQuantityPair[] pairs, bool addIfPossible=false)
        {
            List<Item> itemsToAdd=new List<Item>(pairs.Length);
            for (int i = 0; i < pairs.Length; i++)
            {
                int restQ = pairs[i].quantity;
                var it = pairs[i].item.initializeItem();
                while (it.MaxStackCount < restQ)
                {
                    var curIt = it.getCopy();
                    curIt.SetQuantity(it.MaxStackCount);
                    itemsToAdd.Add(curIt);
                    restQ -= it.MaxStackCount;
                }
                
                var curIt2 = it.getCopy();
                curIt2.SetQuantity(restQ);
                itemsToAdd.Add(curIt2);
            }

            return CheckPossibilityToAddItems(itemsToAdd.ToArray(), addIfPossible);
        }

        public bool CheckPossibilityToAddItems(Item[] itemsToAdd, bool addIfPossible = false)
        {
            bool res;
            if (!isAutoExtendable)
            {
                ///Количество слотов необходимых нам, чтобы добавить набор предметов
                int slotsNeeded = 0;
                ///Количество слотов в, которых уже лежат предметы подобные тем, что мы добавляем
                int slotsReused = 0;
                ///Ключ - id однотипных предметов, а значение их суммарное количество
                Dictionary<int, int> itemsQuantities = new Dictionary<int, int>();

                List<int> stackSizes = new List<int>();
                foreach (var it in itemsToAdd)
                {
                    int id = it.Id;
                    Debug.Log($"{it.Name}: {it.Quantity}");
                    if (!itemsQuantities.ContainsKey(id))
                    {
                        itemsQuantities.Add(id, it.Quantity);
                        var slots = GetSlotsById(id);
                        slotsReused += slots.Count;
                        foreach (var slot in slots)
                        {
                            itemsQuantities[id] += items[slot].Quantity;
                        }

                        int size = 1;
                        if (it.MaxStackCount > 0)
                            size = it.MaxStackCount;

                        stackSizes.Add(size);

                    }
                    else
                    {
                        itemsQuantities[id] += it.Quantity;
                    }
                }

                int i = 0;
                foreach (var kvPair in itemsQuantities)
                {
                    int sn = Mathf.CeilToInt((float) kvPair.Value / stackSizes[i]);
                    slotsNeeded += sn;
                    i++;
                }

                res = emptyCellCount >= slotsNeeded - slotsReused;
                Debug.Log($"Нужно слотов: {slotsNeeded}");
                Debug.Log($"Переиспользуемых слотов: {slotsReused}");
                Debug.Log($"Свободных ячеек: {emptyCellCount}");
                Debug.Log(res);
            }
            else res = true;
            if (res && addIfPossible)
            {
                foreach (var it in itemsToAdd)
                {
                    AddItem(it.getCopy(), it.Quantity);
                }
            }
            return res;
        }

        /// <summary>
        /// Удаляет перечисленные в параметрах предметы в указанном количестве
        /// </summary>
        /// <param name="pairs"></param>
        public void RemoveItems(InventoryController.ItemQuantityPair[] pairs)
        {
            foreach (var pair in pairs)
            {
                RemoveItem(pair.item.initializeItem().Id,pair.quantity);
            }
        }
        /// <summary>
        /// Удаляет заданное количество предметов с указзаным id из инвентаря
        /// Если указанное количество больше чем есть в инвентаре, то удаляются все предметы с заданным id
        /// </summary>
        public void RemoveItem(int id,int quantity)
        {
            var removesNeeded = quantity;
            var slots = GetSlotsById(id);
            slots.Sort((x,y)=>items[x].Quantity.CompareTo(items[y].Quantity));

            foreach (var slot in slots)
            {
                var curQuantity = items[slot].Quantity;
                if (curQuantity < removesNeeded)
                {
                    SetQuantityToSlot(0,slot);
                    removesNeeded -= curQuantity;
                }
                else
                {
                    curQuantity -= removesNeeded;
                    SetQuantityToSlot(curQuantity,slot);
                    removesNeeded = 0;
                }
                
                if(removesNeeded==0) return;
            }
        }
        
        

        /// <summary>
        /// Добавляет зажанное количество предметов в слот если это возможно
        /// </summary>
        /// <param name="item">Если слот занят итемом другого типа, то ничего добавлено или заменено не будет!!!</param>
        /// <param name="slot">номер слота</param>>
        /// <returns>возвращает количество итемов которых не удалось добавить</returns>
        public virtual int AddItemToSlot(Item item,int slot)
        {
            Debug.Log("Adding");
            Item cur = items[slot];
            if (cur == null)
            {
                cur = item.getCopy();
                cur.SetQuantity(item.Quantity);
                items[slot] = cur;
                ReduceEmptyCellCount(1);
                return 0;
            }
            int rq = item.Quantity;
            int restCur = cur.MaxStackCount - cur.Quantity; //количество свободного места в стаке
            if (rq > restCur)
            {
                cur.SetQuantity(cur.MaxStackCount);
                rq -= restCur;
                items[slot] = cur;
                return rq;
            }
            else
            {
                cur.SetQuantity(cur.Quantity + rq);
                items[slot] = cur;
                return 0;
            }
            
        }
        /// <summary>
        /// Выставляет заданное количество предметов для заданного слота
        /// </summary>
        /// <param name="quantity">если==0 то предмет удаляется</param>
        public virtual void SetQuantityToSlot(int quantity,int slot)
        {
            if (quantity < 0 || items[slot]==null || items[slot].MaxStackCount<quantity)
            {
                Debug.LogError("Ошибка в выставлении количества или номера слота");
                return;
            }
            if (quantity == 0)
            {
                items[slot] = null;
                emptyCellCount++;
            }
            else
            {
                items[slot].SetQuantity(quantity);
            }
        }
        /// <summary>
        /// Заменяет итем в указанном слоте на новый
        /// </summary>
        /// <param name="item"></param>
        /// <param name="slot"></param>
        public virtual void ReplaceItem(Item item,int slot)
        {
            if(slot>=items.Length) Debug.LogError($"Выход за пределы инвентаря {name}");
            items[slot] = item;
        }

        /// <summary>
        /// Возвращает ближайшую к началу массива пустую ячейку или -1 если таких нет
        /// </summary>
        public int GetEmptyCell()
        {
            if (emptyCellCount == 0) return -1;

            for (int i = 0; i < items.Length; i++)
            {
                if (items[i] == null) return i;
            }

            return -1;
        }

        public virtual bool ItemCanBePlaced(Item item, int pos)
        {
            Debug.Log("Ver");
            if (item != null && pos < items.Length)
            {
                return true;
            }
            return false;
        }
        
        

        /// <summary>
        /// Возвращает номера всех слотов в которых хранятся item'ы с заданным id
        /// </summary>
        public List<int> GetSlotsById(int id)
        {
            var slots = new List<int>();
            for (int i = 0; i < items.Length; i++)
            {
                if (items[i] != null)
                {
                    if (items[i].Id == id)
                    {
                        slots.Add(i);
                    }
                }
            }

            return slots;
        }
        /// <summary>
        /// Увеличивает размер инвентаря на slotNum
        /// </summary>
        /// <param name="slotNum"></param>
        protected void Extend(int slotNum)
        {
            Array.Resize(ref items,items.Length+slotNum);
            size += slotNum;
            emptyCellCount += slotNum;
        }
        /// <summary>
        /// Уменьшает количество свободных ячеек вместе с проверкой на автоматическое расширений
        /// </summary>
        protected void ReduceEmptyCellCount(int red)
        {
            emptyCellCount -= red;
            if(isAutoExtendable && emptyCellCount<=AutoExtensionCondition) Extend(AutoExtensionSize);
        }

        public Item[] GetItemsOfType(ItemType type)
        {
            Debug.Log(items.Where(x => x.Type == type).First());
           return items.Where(x => x.Type == type).ToArray();
        }
        /// <summary>
        /// Возвращает количество предметов в инвентаре с указанным id
        /// </summary>
        public int GetItemQuantity(int id)
        {
            int res = 0;
            var slots = GetSlotsById(id);
            Debug.Log($"slotscount={slots.Count}");
            foreach (var slot in slots)
            {
                res += items[slot].Quantity;
            }
            return res;
        }
        /// <summary>
        /// Возвращает первый слот с предметом с указанным id или null если такого нет
        /// </summary>
        public Item GetItemById(int id)
        {
            for (int i = 0; i < items.Length; i++)
            {
                if (items[i]!=null && items[i].Id == id)
                {
                    return items[i];
                }
            }

            return null;
        }

        /// <summary>
        /// Содержит ли инвентарь перечисленные предметы в перечисленном количестве
        /// </summary>
        public bool HasItems(InventoryController.ItemQuantityPair[] pairs)
        {
            foreach (var pair in pairs)
            {
                if (pair.quantity > GetItemQuantity(pair.item.initializeItem().Id))
                    return false;
            }
            return true;
        }

        public Item GetItem(int num)
        {
            return items[num];
        }

        /// <summary>
        /// Позволяет получить отладочную информацию об инвентаре
        /// </summary>
        public override string ToString()
        {
            StringBuilder str=new StringBuilder($"В инвентаре {name} {size} ячеек из них {emptyCellCount} свободны:\n");
            for (int i = 0; i < items.Length; i++)
            {
                if (items[i] != null)
                {
                    str.AppendLine(i+": "+items[i].Name+" "+items[i].Quantity+" штук");
                }
                else
                {
                    str.AppendLine(i+": <пусто>");
                }
            }
            return str.ToString();
        }
    }
}
