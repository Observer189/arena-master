using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using MoreMountains.Feedbacks;
using UnityEngine;
using UnityEngine.UI;

namespace InventorySystem
{
    /// <summary>
    /// Визуализатор для ящиков, сундуков и т.д.
    /// </summary>
    public class ContainerVisualizer : InventoryVisualizer
    {
        
        
        
        /// <summary>
        /// Префаб одного слота в инвентаре
        /// </summary>
        [SerializeField]
        protected GameObject slotPrefab;

        /// <summary>
        /// Сетка в которой отображаются объекты
        /// </summary>
        
        /// Текст названия инвентаря
        [SerializeField]
        protected Text title;
        
        [SerializeField]
        protected GridLayoutGroup grid;
        /// <summary>
        /// Canvas group этого объекта, используется для сокрытия инвентаря
        /// </summary>
        protected CanvasGroup canvasGroup;

        private List<SlotVisualizer> slotList;

        public MMFeedbacks itemPutSound;

        private void Awake()
        {
            slotList=new List<SlotVisualizer>();
            canvasGroup = GetComponent<CanvasGroup>();
            canvasGroup.alpha = 0f;
        }

        public override void Visualize(Inventory inventory)
        {
            StartCoroutine(MMFade.FadeCanvasGroup(canvasGroup, 0.2f, 1f));
            SetInventory(inventory);
            RebuildInventory();
        }

        public override void SetInventory(Inventory inventory)
        {
            base.SetInventory(inventory);
        }

        public override void RebuildInventory()
        {
            ClearGrid();
            title.text = Lean.Localization.LeanLocalization.GetTranslationText(inventory.Name,inventory.Name);
            for (int i = 0; i < inventory.Size; i++)
            {
                var curItem = Instantiate(slotPrefab);
                curItem.transform.SetParent(grid.transform,false);
                var slotScript = curItem.GetComponent<SlotVisualizer>();
                slotScript.Initialize(this,i);
                slotScript.SetItem(inventory.GetItem(i));
                slotList.Add(slotScript);
            }
        }
        /// <summary>
        /// Очищает сетку инвентаря
        /// </summary>
        protected void ClearGrid()
        {
            for (int i = 0; i < slotList.Count; i++)
            {
                Destroy(slotList[i].gameObject);
            }

            slotList.Clear();
        }
        /// <summary>
        /// Отправляет запрос в InventoryManager на то, чтобы все предметы из контэйнера были перемещены в инвентарь игрока
        /// </summary>
        public void TakeAll()
        {
            InventoryManager.instance.TakeAll(this);
        }

        public override void Close()
        {
            StartCoroutine(MMFade.FadeCanvasGroup(canvasGroup, 0.2f, 0f));
            InventoryManager.instance.CloseVisualizer(this);
        }
    }
}
