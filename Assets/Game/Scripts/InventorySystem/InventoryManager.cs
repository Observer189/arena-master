﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using MoreMountains.Feedbacks;

namespace InventorySystem
{
    /// <summary>
    /// Класс отвечающий за состояниями визуализаторов инвентарей и передачей предметов между инвентарями
    /// связывает InventoryController с визуализаторами
    /// </summary>
    public class InventoryManager : MonoBehaviour
    {
        [Tooltip("Id предмета, отвечающего за деньги")]
        [SerializeField]
        private int moneyItemID=3;
        [Header("Canvas groups")]
        /// <summary>
        /// Ссылка на объект который будет затемнять экран при открытии инвентаря
        /// </summary>
        [SerializeField]
        private CanvasGroup overlay;
        /// <summary>
        /// Родительский объект для всех визуализаторов
        /// </summary>
        [SerializeField]
        private CanvasGroup visualizerContainer;
        [Header("Visualizers")]
        [SerializeField]
        private InventoryVisualizer containerVisualizer;
        
        [SerializeField]
        private InventoryVisualizer shopVisualizer;
        /// <summary>
        /// Визуализатор инвентаря игрока
        /// </summary>
        [SerializeField] private InventoryVisualizer playerInventoryVisualizer;
        /// <summary>
        /// Визуализатор окна снаряжения
        /// </summary>
        [SerializeField] private InventoryVisualizer playerEquipmentVisualizer;
        /// <summary>
        /// визуализатор оружейного верстака
        /// </summary>
        [SerializeField] private InventoryVisualizer weaponWorkbench;
        /// <summary>
        /// визуализатор для винтовки
        /// </summary>
        [SerializeField] private EquipmentItemVisualizer rifleVisualizer;
        /// <summary>
        /// визуализатор для плазменной винтовки
        /// </summary>
        [SerializeField] private EquipmentItemVisualizer plasmaGunVisualizer;
        /// <summary>
        /// визуализатор для ракетомёта 
        /// </summary>
        [SerializeField] private EquipmentItemVisualizer rocketLauncherVisualizer;
        /// <summary>
        /// визуализатор для лазерного пистолета
        /// </summary>
        [SerializeField] private EquipmentItemVisualizer laserHandgunVisualizer;
        /// <summary>
        /// Ссылка на панель для показа информации о предмете
        /// </summary>
        [SerializeField] private ItemInfoPanel _infoPanel;
        /// <summary>
        /// Ссылка на панель деления предметов
        /// </summary>
        [SerializeField] private ItemDivisionPanel itemDivisionPanel;
        /// <summary>
        /// Ссылка на панель покупки/продажи предметов
        /// </summary>
        [SerializeField] private SalePurchaseItemPanel salePurchaseItemPanel;
        [Header("So-cursor image")]
        /// <summary>
        /// Картинка которая появляется возле курсора во время перетаскивания
        /// </summary>
        [SerializeField] private GameObject itemImage;
        /// <summary>
        /// Смещение иконки итема относительно курсора
        /// </summary>
        [SerializeField] private Vector2 imageOffset;
        [Header("Input")]
        /// Кнопка для открытия или закрытия инвентаря
        public KeyCode ToggleInventoryKey = KeyCode.I;
        
        /// Кнопка для закрытия всех инвентарей
        public KeyCode CloseKey = KeyCode.I;
        [Tooltip("Кнопка. которую нужно зажимать, чтобы при перетаскивании поделить предметы")]
        public KeyCode divisionKey=KeyCode.LeftShift;
        /// <summary>
        /// Отслеживает состояние инвентаря
        /// </summary>
        private bool isInventoryOpen;
        /// <summary>
        /// визуализаторы открытые в данный момент
        /// </summary>
        private List<InventoryVisualizer> openedVisualizers;
        /// <summary>
        /// Ссылка на инвентарь игрока
        /// </summary>
        private Inventory playerInventory;
         /// <summary>
         /// Ссылка на снаряжение игрока
         /// </summary>
        private Inventory playerEquipment;
        /// <summary>
        /// Информация о слоте выбранном в данный момент
        /// item1 - позиция слота в инвентаре
        /// item2 - визуализатор к которому относится слот
        /// </summary>
        private (int, InventoryVisualizer) selectedSlotData;
        /// <summary>
        /// Предмет экипировки модифицируемый на верстаке в данный момент или null
        /// </summary>
        private EquipmentItem modifiedEquipment;

        public bool IsInventoryOpen => isInventoryOpen;
        public Inventory PlayerInventory => playerInventory;

        public Inventory PlayerEquipment => playerEquipment;
        /// <summary>
        /// Синглтон паттерн
        /// </summary>
        public static InventoryManager instance;

        public delegate void OnSelectItemSlot(Item itemSelected, InventoryVisualizer visualizer);
        /// <summary>
        /// Колбэк вызываемый когда игрок выбирает какой-либо слот с предметом
        /// в любом из визуализаторов
        /// Другие визуализаторы могут подписаться на него, чтобы как-то обработать
        /// Например виуализатор оружия может отобразить как изменятся свойства оружия
        /// если установить выбранный модуль
        /// Кроме того если item = null - значит игрок убрал слот из выделения
        /// </summary>
        private OnSelectItemSlot selectItemCallback;

        public MMFeedbacks openInventory;
        public MMFeedbacks closeInventory;

        private void Awake()
        {
            instance = this;
        }

        private void Start()
        {
            isInventoryOpen = false;
            openedVisualizers=new List<InventoryVisualizer>();
            containerVisualizer.gameObject.SetActive(false);
            shopVisualizer.gameObject.SetActive(false);
            playerInventoryVisualizer.gameObject.SetActive(false);
            playerEquipmentVisualizer.gameObject.SetActive(false);
            weaponWorkbench.gameObject.SetActive(false);
            rifleVisualizer.gameObject.SetActive(false);
            plasmaGunVisualizer.gameObject.SetActive(false);
            laserHandgunVisualizer.gameObject.SetActive(false);
            rocketLauncherVisualizer.gameObject.SetActive(false);
            _infoPanel.gameObject.SetActive(false);
            itemDivisionPanel.Hide();
            itemDivisionPanel.gameObject.SetActive(false);
            salePurchaseItemPanel.Hide();
            salePurchaseItemPanel.gameObject.SetActive(false);
            
            //Инициализация картинки следующей за курсором при перемещении
            itemImage=Instantiate(itemImage);
            itemImage.transform.SetParent(transform);
            itemImage.transform.localScale=Vector3.one;
            var FM=itemImage.GetComponent<UIFollowMouse>();
            FM.TargetCanvas = GetComponent<Canvas>();
            FM.offset = imageOffset;
            itemImage.SetActive(false);
        }

        private void Update()
        {
            //Нужна именно такая проверка, так как когда открыт инвентарь, считается, что игра на паузе
            if (!GUIManager.Instance.PauseScreen.activeSelf && InputManager.Instance.InputDetectionActive)
            {
                if (Input.GetKeyDown(ToggleInventoryKey))
                {
                    if (playerInventoryVisualizer.gameObject.activeSelf)
                    {
                        CloseVisualizer(playerInventoryVisualizer);
                    }
                    else
                    {
                        if (playerInventory != null)
                        {
                            OpenVisualizer(playerInventory, VisualizerType.PlayerInventory);
                        }
                        
                    }

                }
                else if (Input.GetKeyDown(CloseKey))
                {
                    if (isInventoryOpen)
                    {
                        if (itemDivisionPanel.gameObject.activeSelf)
                        {
                            itemDivisionPanel.CancelDivision();
                            itemDivisionPanel.gameObject.SetActive(false);
                        }
                        StartCoroutine(CloseAll());
                        
                    }
                }
            }

        }
        

        public void OpenVisualizer(Inventory inventory, VisualizerType type)
        {
            if(!isInventoryOpen) 
            OpenInventory();
            if (type == VisualizerType.Container)
            {
                containerVisualizer.gameObject.SetActive(true);
                containerVisualizer.Visualize(inventory);
                openedVisualizers.Add(containerVisualizer);
                if (!playerInventoryVisualizer.gameObject.activeSelf)
                {
                    OpenVisualizer(playerInventory,VisualizerType.PlayerInventory);
                }
            }
            else if(type == VisualizerType.PlayerInventory)
            {
                playerInventoryVisualizer.gameObject.SetActive(true);
                playerInventoryVisualizer.Visualize(inventory);
                openedVisualizers.Add(playerInventoryVisualizer);
                if (playerEquipment != null)
                {
                    if (!playerEquipmentVisualizer.gameObject.activeSelf)
                    {
                        OpenVisualizer(playerEquipment, VisualizerType.PlayerEquipment);
                    }
                }
            }
            else if (type == VisualizerType.PlayerEquipment)
            {
                playerEquipmentVisualizer.gameObject.SetActive(true);
                playerEquipmentVisualizer.Visualize(inventory);
                openedVisualizers.Add(playerEquipmentVisualizer);
            }
            else if(type == VisualizerType.WeaponWorkbench)
            {
                weaponWorkbench.gameObject.SetActive(true);
                weaponWorkbench.Visualize(inventory);
                openedVisualizers.Add(weaponWorkbench);
                if (!playerInventoryVisualizer.gameObject.activeSelf)
                {
                    OpenVisualizer(playerInventory,VisualizerType.PlayerInventory);
                }
            }
            else if (type == VisualizerType.RifleModules)
            {
                rifleVisualizer.gameObject.SetActive(true);
                ///Получаем ссылку на WeaponItem из инвентаря верстака
                rifleVisualizer.EqIt = (EquipmentItem)weaponWorkbench.Inventory.Items[0];
                rifleVisualizer.Visualize(inventory);
                openedVisualizers.Add(rifleVisualizer);
            }
            else if (type == VisualizerType.PlasmaGun)
            {
                plasmaGunVisualizer.gameObject.SetActive(true);
                ///Получаем ссылку на WeaponItem из инвентаря верстака
                plasmaGunVisualizer.EqIt = (EquipmentItem)weaponWorkbench.Inventory.Items[0];
                plasmaGunVisualizer.Visualize(inventory);
                openedVisualizers.Add(plasmaGunVisualizer);
            }
            else if (type == VisualizerType.RocketLauncher)
            {
                rocketLauncherVisualizer.gameObject.SetActive(true);
                ///Получаем ссылку на WeaponItem из инвентаря верстака
                rocketLauncherVisualizer.EqIt = (EquipmentItem)weaponWorkbench.Inventory.Items[0];
                rocketLauncherVisualizer.Visualize(inventory);
                openedVisualizers.Add(rocketLauncherVisualizer);
            }
            else if (type == VisualizerType.LaserHandgun)
            {
                laserHandgunVisualizer.gameObject.SetActive(true);
                ///Получаем ссылку на WeaponItem из инвентаря верстака
                laserHandgunVisualizer.EqIt = (EquipmentItem)weaponWorkbench.Inventory.Items[0];
                laserHandgunVisualizer.Visualize(inventory);
                openedVisualizers.Add(laserHandgunVisualizer);
            }
            else if(type==VisualizerType.Shop)
            {
                shopVisualizer.gameObject.SetActive(true);
                shopVisualizer.Visualize(inventory);
                openedVisualizers.Add(shopVisualizer);
                if (!playerInventoryVisualizer.gameObject.activeSelf)
                {
                    OpenVisualizer(playerInventory,VisualizerType.PlayerInventory);
                }
            }
        }

        private void OpenVisualizer(InventoryVisualizer visualizer,Inventory inventory)
        {
            if (!openedVisualizers.Contains(visualizer))
            {
                visualizer.gameObject.SetActive(true);
                visualizer.Visualize(inventory);
                openedVisualizers.Add(visualizer);
            }
        }

        private void CloseVisualizer(VisualizerType type)
        {
            switch (type)
            {
                case VisualizerType.Container:
                    CloseVisualizer(containerVisualizer);
                    break;
                case VisualizerType.PlayerInventory:
                    CloseVisualizer(playerInventoryVisualizer);
                    break;
                case VisualizerType.PlayerEquipment:
                    CloseVisualizer(playerEquipmentVisualizer);
                    break;
                case VisualizerType.RifleModules:
                    CloseVisualizer(rifleVisualizer);
                    break;
                case VisualizerType.WeaponWorkbench:
                    CloseVisualizer(weaponWorkbench);
                    break;
                case VisualizerType.Shop:
                    CloseVisualizer(shopVisualizer);
                    break;
                case VisualizerType.PlasmaGun:
                    CloseVisualizer(plasmaGunVisualizer);
                    break;
                case VisualizerType.LaserHandgun:
                    CloseVisualizer(laserHandgunVisualizer);
                    break;
                case VisualizerType.RocketLauncher:
                    CloseVisualizer(rocketLauncherVisualizer);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        public void CloseVisualizer(InventoryVisualizer visualizer)
        {
            if (openedVisualizers.Remove(visualizer))
            {
                visualizer.gameObject.SetActive(false);
                Debug.Log($"Открытых визуализаторов: {openedVisualizers.Count}");
                if (openedVisualizers.Count == 0) CloseInventory();
            }
        }
        /// <summary>
        /// Функция, вызываемая при открытии верстака
        /// Запоминает можифицируемы предмет, чтобы при закрытии верстака
        /// закрывался соответствующий визуализатор
        /// </summary>
        public void OpenWorkBench(EquipmentItem eqItem)
        {
            modifiedEquipment = eqItem;
            OpenVisualizer(eqItem.ModuleInventory, eqItem.ModuleInventory.VisualizerType);
        }

        /// <summary>
         /// Функция для закрытия открытых оружейный визуализаторов
         /// используется с верстака
         /// </summary>
        public void CloseWorkbench()
        {
            if (modifiedEquipment != null)
            {
                CloseVisualizer(modifiedEquipment.ModuleInventory.VisualizerType);
                modifiedEquipment = null;
            }
        }

        /// <summary>
        /// Создает затенение экрана и ставит игру на паузу
        /// Инвентарь считается открытым до тех пор пока открыт хотя бы один из визуализаторов
        /// </summary>
        protected void OpenInventory()
        {
            openInventory?.PlayFeedbacks();
            MMGameEvent.Trigger("inventoryOpens");
            //TopDownEngineEvent.Trigger(TopDownEngineEventTypes.Pause,null);
            isInventoryOpen = true;
            Cursor.visible = true;
            EventSystem.current.sendNavigationEvents = true;
            StartCoroutine(MMFade.FadeCanvasGroup(visualizerContainer, 0.2f, 1f));
            StartCoroutine(MMFade.FadeCanvasGroup(overlay, 0.2f, 0.85f));
            Debug.Log("Open");
        }
        /// <summary>
        /// снимает игру с паузы
        /// </summary>
        protected void CloseInventory()
        {
            closeInventory?.PlayFeedbacks();
            MMGameEvent.Trigger("inventoryCloses");
            MMGameEvent.Trigger("PlayerInventoryUpdate");
            //TopDownEngineEvent.Trigger(TopDownEngineEventTypes.UnPause,null);
            isInventoryOpen = false;
            Cursor.visible = false;
            _infoPanel.gameObject.SetActive(false);
            itemImage.SetActive(false);
            EventSystem.current.sendNavigationEvents = false;
            StartCoroutine(MMFade.FadeCanvasGroup(visualizerContainer, 0.2f, 0f));
            StartCoroutine(MMFade.FadeCanvasGroup(overlay, 0.2f, 0f));
            openedVisualizers.Clear();
            Debug.Log("Close");
        }

        protected IEnumerator CloseAll()
        {
            ///Это ожидание необходимо, чтобы
            /// сразу не сработала способность игрока по выставлению паузы
            ///Работает за счет того, что кнопка считается в состоянии buttonDown только один кадр
            yield return new WaitForEndOfFrame();
            for (int i = 0; i < openedVisualizers.Count;)
            {
                CloseVisualizer(openedVisualizers[i]);
            }
        }
        /// <summary>
        /// Перемещает все предметы из инвентаря соответсвующего визуализатора в инвентарь игрока
        /// </summary>
        /// <param name="visualizer"></param>
        public void TakeAll(InventoryVisualizer visualizer)
        {
            for (int i = 0; i < visualizer.Inventory.Size; i++)
            {
                var it = visualizer.Inventory.Items[i];
                if (it != null)
                {
                    var rest = playerInventory.AddItem(it,it.Quantity);
                    visualizer.Inventory.SetQuantityToSlot(rest,i);
                }
            }
            playerInventoryVisualizer.RebuildInventory();
            visualizer.RebuildInventory();
        }
        /// <summary>
        /// Действия выполняемы при кликом правой кнопкой мыши по слоту инвентаря
        /// </summary>
        public IEnumerator OnItemRightClick()
        {
            var it= selectedSlotData.Item2.Inventory.GetItem(selectedSlotData.Item1);
            if(it==null || selectedSlotData.Item2.receiveAccess==VisualizerAccessLevel.Private) yield break;
            if (selectedSlotData.Item2 != playerInventoryVisualizer)
            {
                if (playerInventoryVisualizer.gameObject.activeSelf)
                {
                    if (selectedSlotData.Item2.receiveAccess == VisualizerAccessLevel.Public)
                    {
                        var b = playerInventoryVisualizer.Inventory.CheckPossibilityToAddItems(new[] {it}, true);
                        if (b)
                        {
                            selectedSlotData.Item2.Inventory.SetQuantityToSlot(0, selectedSlotData.Item1);
                            selectedSlotData.Item2.RebuildInventory();
                            playerInventoryVisualizer.RebuildInventory();
                        }
                    }
                    else if(selectedSlotData.Item2.receiveAccess == VisualizerAccessLevel.Shop)
                    {
                        StartCoroutine(ProcessPurchase(it,selectedSlotData.Item2,selectedSlotData.Item1));
                    }
                }
            }
        }

        /// <summary>
        /// Когда игрок начинает перемещать какой-то item мы активируем изображение следующее за мышью
        /// </summary>
        public void OnDragBegin(Sprite itemIcon)
        {
            if(!selectedSlotData.Item2.internalAccess && selectedSlotData.Item2.receiveAccess==VisualizerAccessLevel.Private)
                return;
            itemImage.gameObject.SetActive(true);
            itemImage.GetComponent<Image>().sprite=itemIcon;
        }

        /// <summary>
        /// Когда игрок закончил перемещать item мы выполняем перемещения в инвентарях
        /// </summary>
        public IEnumerator OnDragEnd(int draggedPos,InventoryVisualizer draggedVisualizer)
        {
            itemImage.SetActive(false);
            //Ничего не делаем если итем перемещен сам на себя
            if (selectedSlotData.Item1 == draggedPos && selectedSlotData.Item2 == draggedVisualizer
            || selectedSlotData.Item2.putAccess==VisualizerAccessLevel.Private
            || draggedVisualizer.receiveAccess==VisualizerAccessLevel.Private)
                yield break;
            //Если в конечный слот нельзя разместить итем данного типа, то ничего не делаем
           /* if (!selectedSlotData.Item2.Inventory.ItemCanBePlaced(draggedVisualizer.Inventory.GetItem(draggedPos),selectedSlotData.Item1))
            {
                return;
            }*/
           var item1 = draggedVisualizer.Inventory.GetItem(draggedPos);
           var item2 = selectedSlotData.Item2.Inventory.GetItem(selectedSlotData.Item1);
           if (draggedVisualizer == selectedSlotData.Item2 ||
               (draggedVisualizer.receiveAccess == VisualizerAccessLevel.Public &&
                selectedSlotData.Item2.putAccess == VisualizerAccessLevel.Public))
           {
               //Если мы перемещаем на пустой слот ИЛИ перемещаем STACKABLE ОДИНАКОВЫЕ объекты, то перемещаем столько сколько возможно
               if (item2 == null || (item2.Id == item1.Id && item1.IsStackable))
               {
                   ///Инициализируем деление предметов, если надо
                   if (Input.GetKey(divisionKey))
                   {
                       itemDivisionPanel.gameObject.SetActive(true);
                       itemDivisionPanel.Show();
                       Ref<int> q1 = new Ref<int>();
                       Ref<int> q2 = new Ref<int>();
                       yield return StartCoroutine(itemDivisionPanel.InitializeDivision(item1, item2, q1, q2));
                       itemDivisionPanel.gameObject.SetActive(false);

                       if (q2.value != 0 && item2 == null)
                       {
                           selectedSlotData.Item2.Inventory.AddItemToSlot(item1.getCopy(), selectedSlotData.Item1);
                       }

                       draggedVisualizer.Inventory.SetQuantityToSlot(q1.value, draggedPos);
                       selectedSlotData.Item2.Inventory.SetQuantityToSlot(q2.value, selectedSlotData.Item1);
                       
                   }
                   else
                   {
                       var rq = selectedSlotData.Item2.Inventory.AddItemToSlot(item1, selectedSlotData.Item1);
                       //Debug.Log("Rq= "+rq);
                       draggedVisualizer.Inventory.SetQuantityToSlot(rq, draggedPos);
                   }

               }
               //Иначе делаем замену если возможно
               else if (selectedSlotData.Item2.Inventory.ItemCanBePlaced(
                            draggedVisualizer.Inventory.GetItem(draggedPos), selectedSlotData.Item1) &&
                        draggedVisualizer.Inventory.ItemCanBePlaced(
                            selectedSlotData.Item2.Inventory.GetItem(selectedSlotData.Item1), selectedSlotData.Item1))
               {
                   Debug.Log("Replacing");
                   var temp = draggedVisualizer.Inventory.GetItem(draggedPos);
                   draggedVisualizer.Inventory.ReplaceItem(
                       selectedSlotData.Item2.Inventory.GetItem(selectedSlotData.Item1), draggedPos);
                   selectedSlotData.Item2.Inventory.ReplaceItem(temp, selectedSlotData.Item1);
               }
           }
           else if (draggedVisualizer.receiveAccess==VisualizerAccessLevel.Shop &&
                    draggedVisualizer!=selectedSlotData.Item2)
           {
               StartCoroutine(ProcessPurchase(item1,draggedVisualizer,draggedPos));
           }


           draggedVisualizer.RebuildInventory();
           selectedSlotData.Item2.RebuildInventory();
            
        }
        /// <summary>
        /// Корутина, которая проводит покупку предмета itemToBuy, платит из него деньгами из PlayerInventory
        /// </summary>
        protected IEnumerator ProcessPurchase(Item itemToBuy,InventoryVisualizer itemVisualizer,int slot)
        {
            Ref<int> buyQuantity=new Ref<int>();
            int playerMoney = playerInventoryVisualizer.Inventory.GetItemQuantity(moneyItemID);
            Debug.Log($"PlayerMoney={playerMoney}");
            //yield return StartCoroutine(ProcessPurchase(buyQuantity,it,playerMoney,true));
            salePurchaseItemPanel.gameObject.SetActive(true);
            yield return StartCoroutine(salePurchaseItemPanel.InitializeOperation(itemToBuy, playerMoney, buyQuantity, true));
            salePurchaseItemPanel.gameObject.SetActive(false);
            if (buyQuantity.value>0)
            {
                var itemToAdd = itemToBuy.getCopy();
                itemToAdd.SetQuantity(buyQuantity.value);
                var b=playerInventory.CheckPossibilityToAddItems(new []{itemToAdd},true);
                if (b)
                {
                    int totalCost = Mathf.CeilToInt(buyQuantity.value * itemToBuy.EstimatedCost);
                    itemVisualizer.Inventory.
                        SetQuantityToSlot(itemToBuy.Quantity-buyQuantity.value,slot);
                    itemVisualizer.Inventory.AddItem(playerInventory.GetItemById(moneyItemID).getCopy(),totalCost);
                    playerInventory.RemoveItem(moneyItemID,totalCost);
                    itemVisualizer.RebuildInventory();
                    playerInventoryVisualizer.RebuildInventory();
                }
                else
                {
                    Notifier.instance.ShowNotification("The operation was canceled because there is not enough space in the inventory",NotificationType.Error);
                }
            }
        }

        /// <summary>
        /// Когда игрок выбирает новый слот мы обновляем информацию
        /// и показываем информацию о выбранном предмете
        /// </summary>
        public void SetSelectedSlotData(int pos,InventoryVisualizer visualizer,Vector3 position)
        {
            selectedSlotData = (pos, visualizer);
            Debug.Log(selectedSlotData.Item2);
            Debug.Log(selectedSlotData.Item2.Inventory);
            var item = selectedSlotData.Item2.Inventory.GetItem(pos);
            if (item != null)
            {
                _infoPanel.gameObject.SetActive(true);
                _infoPanel.transform.position = new Vector2(100000,10000);
                _infoPanel.SetItem(item,position);
                selectItemCallback?.Invoke(item,visualizer);
            }
            else
            {
                _infoPanel.gameObject.SetActive(false);
                selectItemCallback?.Invoke(null,null);
            }
        }
        /// <summary>
        /// Когда указатель выходит из зоны слота скрываем инфопанель
        /// </summary>
        public void OnUnselectSlot()
        {
            _infoPanel.gameObject.SetActive(false);
            selectItemCallback?.Invoke(null,null);
        }

        /// <summary>
        /// Через этот метод будет передан инвентарь игрока при загрузке сцены
        /// </summary>
        /// <param name="inventory"></param>
        public void RegisterPlayerInventory(Inventory inventory)
        {
            Debug.Log($"Register player inventory: {inventory}");
            playerInventory = inventory;
        }
        
        /// <summary>
        /// Через этот метод будет передано снаряжение игрока при загрузке сцены
        /// </summary>
        /// <param name="inventory"></param>
        public void RegisterPlayerEquipment(Inventory inventory)
        {
            Debug.Log($"Register player equipment: {inventory}");
            playerEquipment = inventory;
        }
        public void RegisterSelectSlotCallback(OnSelectItemSlot func)
        {
            selectItemCallback += func;
        }

        public void UnregisterSelectSlotCallback(OnSelectItemSlot func)
        {
            selectItemCallback -= func;
        }
    }
    
  

    public enum VisualizerType
    {
        Container,PlayerInventory,PlayerEquipment,RifleModules,WeaponWorkbench,Shop,PlasmaGun,LaserHandgun,RocketLauncher
    }
}
