﻿using System.Collections;
using System.Collections.Generic;
using InventorySystem;
using Newtonsoft.Json;
using UnityEngine;

public class UsableItem : Item
{
    protected ItemUseType useType;
    /// <summary>
    /// Определяет расходуется ли предмет при использовании
    /// </summary>
    protected bool isConsumable;
    /// <summary>
    /// Звук, воспроизводимый при использовании предмета
    /// </summary>
    protected AudioClip useSound;

    public ItemUseType UseType => useType;

    public bool IsConsumable => isConsumable;

    public AudioClip UseSound => useSound;
    [JsonConstructor]
    public UsableItem(string path,int quantity):base(path,quantity)
    {
        
    }

    protected override string DetermineLoadPath(string name)
    {
        return "Items/UsableItems/"+name;
    }

    protected override ItemInfo LoadInfo(string path)
    {
        return base.LoadInfo(path) as UsableItemInfo;
    }

    protected override ItemInfo InitializeFromInfo(ItemInfo info)
    {
        UsableItemInfo i = base.InitializeFromInfo(info) as UsableItemInfo;
        useType = i.UseType;
        isConsumable = i.IsConsumable;
        useSound = i.UseSound;
        return i;
    }

    public UsableItem(string name,string path,int id, Sprite icon, string description, ItemType type, 
        bool isStackable, string[] tags,ItemUseType useType,bool isConsumable ,
        AudioClip useSound,float estimatedCost,int maxStackCount = 1) 
        : base(name, path,id, icon, description, type, isStackable, tags, estimatedCost,maxStackCount)
    {
        this.useType = useType;
        this.isConsumable = isConsumable;
        this.useSound = useSound;
    }
    /// <summary>
    /// Способ использования предмета
    /// Throw - бросает предмет в сторону прицеливания
    /// Boost - накладывает какой-то эффект на персонажа
    /// </summary>
    public enum ItemUseType
    {
        Throw,Boost
    }

    public override Item getCopy()
    {
        return new UsableItem(name, path,id,icon,description,type,isStackable, tags,useType,isConsumable,useSound,estimatedCost,maxStackCount);
    }
}
