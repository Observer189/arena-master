﻿using System.Collections;
using System.Collections.Generic;
using InventorySystem;
using UnityEngine;
[CreateAssetMenu(fileName = "New Throwing item", menuName = "Items/Throwing item", order = 3)]
public class ThrowingItemInfo : UsableItemInfo
{
  [Tooltip("Ссылка на префаб метательного предмета")]
  [SerializeField]
  protected GameObject projectile;

  public GameObject Projectile => projectile;

  public override Item initializeItem()
  {
    return new ThrowingItem(name,path,id,icon,description,type,isStackable,tags,useType,isConsumable,useSound,projectile,estimatedCost,maxStackCount);
  }
}
