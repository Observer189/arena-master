﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using InventorySystem;
using UnityEngine;
[CreateAssetMenu(fileName = "New Armor", menuName = "Items/Armor", order = 2)]
public class ArmorItemInfo : EquipmentItemInfo
{
    [Tooltip("Активная способность брони")]
    public string ability;
    [Tooltip("Визуальное отображение частей экипировки в следующем порядке:" +
             "Туловище, Голова, левая рука от плеча до кисти, правая рука, левая нога, правая нога")]
    public Sprite[] armorSprites;
    public override Item initializeItem()
    {
        var kw = baseKeyWords.Select(k=>k.GenerateKeyWord()).ToArray();
        return new ArmorItem(name,path,id,icon,description,type,equipmentType,isStackable,tags,
            estimatedCost,properties,kw,(SpecialInventory)initializer.InitializeInventory(),ability,armorSprites,maxStackCount);
    }
}
