﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using InventorySystem;
using UnityEngine;
[CreateAssetMenu(fileName = "New Equipment Item", menuName = "Items/Equipment", order = 2)]
public class EquipmentItemInfo : ItemInfo
{
    [Tooltip("Тип надеваемой экипировки. На персонаже единомоментно может быть надет только один предмет типа")]
    [SerializeField]
    protected string equipmentType;
    [Tooltip("Инициализатор инвентаря")]
    [SerializeField] protected InventoryInitializer initializer;
    [Tooltip("Описание инициализируемых свойств оружия")]
    [SerializeField]
    protected PropertyInitInfo[] properties;
    [SerializeField]
    [Tooltip("Ключевые слова лежащие на предмете снаряжения изначально без всяких модулей")]
    protected KeyWordInfo[] baseKeyWords;

    public string EquipmentType => equipmentType;

    public PropertyInitInfo[] Properties => properties;

    public KeyWordInfo[] BaseKeyWords => baseKeyWords;

    public override Item initializeItem()
    {
        var kw = baseKeyWords.Select(k=>k.GenerateKeyWord()).ToArray();
        return new EquipmentItem(name,path,id,icon,description,type,equipmentType,isStackable,tags,estimatedCost,properties,kw,(SpecialInventory)initializer.InitializeInventory(),maxStackCount);
    }
}
