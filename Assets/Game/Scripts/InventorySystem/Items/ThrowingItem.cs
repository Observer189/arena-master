﻿using System.Collections;
using System.Collections.Generic;
using InventorySystem;
using Newtonsoft.Json;
using UnityEngine;

public class ThrowingItem : UsableItem
{
    //Ссылка на префаб метательного объекта
    protected GameObject projectile;

    public GameObject Projectile => projectile;
    [JsonConstructor]
    public ThrowingItem(string path, int quantity):base(path,quantity)
    {
        
    }

    protected override string DetermineLoadPath(string name)
    {
        return "Items/UsableItems/ThrowingItems/"+name;
    }

    protected override ItemInfo LoadInfo(string path)
    {
        return base.LoadInfo(path) as ThrowingItemInfo;
    }

    protected override ItemInfo InitializeFromInfo(ItemInfo info)
    {
        var i = base.InitializeFromInfo(info) as ThrowingItemInfo;

        projectile = i.Projectile;
        
        return i;
    }

    public ThrowingItem(string name,string path ,int id, Sprite icon, string description, ItemType type, 
        bool isStackable, string[] tags, ItemUseType useType, 
        bool isConsumable, AudioClip useSound, GameObject projectile,float estimatedCost,int maxStackCount = 1) 
        : base(name, path,id, icon, description, type, isStackable,
            tags, useType, isConsumable, useSound,estimatedCost, maxStackCount)
    {
        this.projectile = projectile;
    }

    public override Item getCopy()
    {
        return new ThrowingItem(name,path,id,icon,description,type,isConsumable,tags,useType,isConsumable,useSound,projectile,estimatedCost,maxStackCount);
    }
}
