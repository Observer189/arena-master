﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using InventorySystem;
using Newtonsoft.Json;
using UnityEngine;

public class ModuleItem : Item
{
    /// <summary>
    /// Список свойств модуля
    /// </summary>
    protected KeyWord[] keyWords;

    public KeyWord[] KeyWords => keyWords;
    [JsonConstructor]
    public ModuleItem(string path, int quantity):base(path,quantity)
    {
        
    }

    protected override ItemInfo LoadInfo(string path)
    {
        return base.LoadInfo(path) as ModuleItemInfo;
    }

    protected override ItemInfo InitializeFromInfo(ItemInfo info)
    {
        var i = base.InitializeFromInfo(info) as ModuleItemInfo;
        keyWords=i.KeyWords.Select(k=>k.GenerateKeyWord()).ToArray();
        return i;
    }

    public ModuleItem(string name, string path,int id, Sprite icon, string description, ItemType type,
        bool isStackable, string[] tags,KeyWord[] keyWords,float estimatedCost ,int maxStackCount = 1) 
        : base(name,path ,id, icon, description, type, isStackable, tags,estimatedCost, maxStackCount)
    {
        this.keyWords = keyWords;
    }

    public override Item getCopy()
    {
        return new ModuleItem(name,path,id,icon,description,type,isStackable,tags,keyWords,estimatedCost,maxStackCount);
    }
}
