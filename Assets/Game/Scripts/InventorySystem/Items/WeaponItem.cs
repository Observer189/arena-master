﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using InventorySystem;
using MoreMountains.TopDownEngine;
using Newtonsoft.Json;
using UnityEngine;
[DataContract]
public class WeaponItem : EquipmentItem
{
   /// <summary>
   /// Ссылка на префаб соответствующего оружия
   /// </summary>
   protected GameObject weaponPrefab;
   /// <summary>
   /// Количество патронов заряженных в магазин в данный момен
   /// -1 означает полную обойму
   /// </summary>
   [JsonProperty]
   protected int ammoCount;
   
   public GameObject WeaponPrefab => weaponPrefab;

   public int AmmoCount
   {
       get => ammoCount;
       set => ammoCount = value;
   }
   [JsonConstructor]
   public WeaponItem(string path, int quantity, SpecialInventory moduleInventory, int ammoCount):base(path,quantity,moduleInventory)
   {
       this.ammoCount = ammoCount;
   }

   protected override ItemInfo LoadInfo(string path)
   {
       return base.LoadInfo(path) as WeaponItemInfo;
   }

   protected override ItemInfo InitializeFromInfo(ItemInfo info)
   {
       var i = base.InitializeFromInfo(info) as WeaponItemInfo;
       weaponPrefab = i.WeaponPrefab;
       
       return i;
   }

   public WeaponItem(string name, string path,int id, Sprite icon, string description, ItemType type,string equipmentType, bool isStackable, 
        string[] tags,GameObject weaponPrefab,int ammoCount,PropertyInitInfo[] props,KeyWord[] keyWords,
        SpecialInventory inventory,float estimatedCost,int maxStackCount = 1) 
        : base(name, path,id, icon, description, type, equipmentType,
            isStackable, tags, estimatedCost,props,keyWords,inventory,maxStackCount)
    {
        this.weaponPrefab = weaponPrefab;
        this.ammoCount = ammoCount;
    }

   public override Item getCopy()
    {
        return new WeaponItem(name,path,id,icon,description,type,equipmentType,isStackable,tags,weaponPrefab,ammoCount,properties,baseKeyWords,moduleInventory,estimatedCost,maxStackCount);
    }
}
