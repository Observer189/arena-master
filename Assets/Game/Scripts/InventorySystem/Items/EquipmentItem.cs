﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using InventorySystem;
using Newtonsoft.Json;
using UnityEngine;


/// <summary>
/// Класс предметов экипировки
/// Предметы экипировки имеют свои базовые параметры, а также внутренний инвентарь,
/// позволяющий вставлять в них модули улучшения
/// Базовые параметры устанавливаются при использовании данного предмета экипировки
/// Модули же изменяют те параметры, что уже есть
/// </summary>
[DataContract]
public class EquipmentItem : Item
{
    /// <summary>
    /// Тип надеваемой экипировки
    /// На персонаже единомоментно может быть надет только один предмет типа
    /// </summary>
    protected string equipmentType;
    /// <summary>
    /// Описание свойств оружия
    /// </summary>
    protected PropertyInitInfo[] properties;
   
    /// <summary>
    /// Инвентарь предмета в котором размещаются модули
    /// </summary>
    [JsonProperty]
    protected SpecialInventory moduleInventory;
    /// <summary>
    /// Словарь, хранящий бонусы от модулей, вставленных в предмет экипировки
    /// ключ-id свойства
    /// значение - увеличение свойства от всех модулей
    /// 1-ый элемент значения - константное увеличение
    /// 2-ой элемент - процентное увеличение от базы
    /// </summary>
    private Dictionary<int, (float, float)> moduleBonuses;
    /// <summary>
    /// Ключевые слова лежащие на предмете снаряжения изначально
    /// без всяких модулей
    /// </summary>
    protected KeyWord[] baseKeyWords;

    public Dictionary<int, (float,float)> ModuleBonuses => moduleBonuses;

    public string EquipmentType => equipmentType;

    public SpecialInventory ModuleInventory
    {
        get => moduleInventory;
        set => moduleInventory = value;
    }
    
    public PropertyInitInfo[] Properties => properties;
    [JsonConstructor]
    public EquipmentItem(string path, int quantity, SpecialInventory moduleInventory):base(path,quantity)
    {
        this.moduleInventory = moduleInventory;
    }

    protected override ItemInfo LoadInfo(string path)
    {
        return base.LoadInfo(path) as EquipmentItemInfo;
    }

    protected override ItemInfo InitializeFromInfo(ItemInfo info)
    {
        var i = base.InitializeFromInfo(info) as EquipmentItemInfo;
        equipmentType = i.EquipmentType;
        baseKeyWords = i.BaseKeyWords.Select(k=>k.GenerateKeyWord()).ToArray();;
        properties = i.Properties;
        return i;
    }


    public EquipmentItem(string name, string path,int id, Sprite icon, string description, ItemType type,string equipmentType,
        bool isStackable, string[] tags, float estimatedCost,PropertyInitInfo[] props,KeyWord[] keyWords,
        SpecialInventory inventory, int maxStackCount = 1) 
        : base(name, path,id, icon, description, type, isStackable, tags, estimatedCost, maxStackCount)
    {
        moduleInventory = inventory;
        properties = props;
        this.equipmentType = equipmentType;
        baseKeyWords = keyWords;
        moduleBonuses=new Dictionary<int, (float,float)>();
    }
    /// <summary>
    /// Высчитывает все бонусы модулей и записывает в moduleBonuses
    /// </summary>
    public void CalculateModuleBonuses()
    {
        moduleBonuses=new Dictionary<int, (float, float)>();
        Debug.Log("Bonus calculation");
        AddKeyWords(baseKeyWords);
        foreach (var item in moduleInventory.Items)
        {
            if (item != null)
            {
                var module = (ModuleItem)item;
                AddKeyWords(module.KeyWords);
            }
        }
    }

    protected void AddKeyWords(KeyWord[] keyWords)
    {
        for (int i = 0; i < keyWords.Length; i++)
        {
            var kw = keyWords[i];
            Debug.Log($"Ключевое слово id{kw.TargetId}, значение {kw.Value}");
                    
            if (!moduleBonuses.ContainsKey(kw.TargetId))
            {
                moduleBonuses[kw.TargetId] = (0, 0);
            }

            (float constant, float percent) curTuple = moduleBonuses[kw.TargetId];
            if (kw.IsPercent)
            {
                curTuple.percent += kw.Value;
            }
            else
            {
                curTuple.constant += kw.Value;
            }

            moduleBonuses[kw.TargetId] = curTuple;
        }
    }
}
