﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public class KeyWordInfo
{
    [Tooltip("id свойства на которое направлено действие ключевого слова")]
    [SerializeField]
    private int targetId;
    [Tooltip("Минимальное возможное значение при генерации")]
    [SerializeField]
    private float minValue;
    [Tooltip("Максимальное возможное значение при генерации")]
    [SerializeField]
    private float maxValue;
    [Tooltip("Следует ли рассматривать это ключевое слово процентным или константным")]
    [SerializeField]
    private bool isPercent;

    public int TargetId => targetId;

    public float MinValue => minValue;

    public float MaxValue => maxValue;

    public bool IsPercent => IsPercent;

    public KeyWord GenerateKeyWord()
    {
        return new KeyWord(targetId,Random.Range(minValue,maxValue),isPercent);
    }

}
