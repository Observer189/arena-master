﻿using System.Collections;
using System.Collections.Generic;
using InventorySystem;
using UnityEngine;

public class UsableItemInfo : ItemInfo
{
    [Tooltip("Способ использования предмета")]
    [SerializeField]
    protected UsableItem.ItemUseType useType;
    [Tooltip("Будет ли расходоваться предмет при использовании")]
    [SerializeField]
    protected bool isConsumable;
    
    [Tooltip("Звук, воспроизводимый при использовании предмета")]
    [SerializeField]
    protected AudioClip useSound;

    public UsableItem.ItemUseType UseType => useType;

    public bool IsConsumable => isConsumable;

    public AudioClip UseSound => useSound;

    public override Item initializeItem()
    {
        return new UsableItem(name,path,id,icon,description,type,isStackable,tags,useType,isConsumable,useSound,estimatedCost,maxStackCount);
    }
}
