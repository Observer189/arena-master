﻿using System.Collections;
using System.Collections.Generic;
using InventorySystem;
using UnityEngine;
[CreateAssetMenu(fileName = "New Boost", menuName = "Items/Boost", order = 2)]
public class BoostItemInfo : UsableItemInfo
{
   [Tooltip("Список эффектов, накладываемых на игрока при использовании")]
   [SerializeField]
   protected EffectDescription[] effects;

   public EffectDescription[] Effects => effects;
   public override Item initializeItem()
   {
      return new BoostItem(name,path,id,icon,description,type,isStackable,tags,useType,isConsumable,useSound,effects,estimatedCost,maxStackCount);
   }
}
