﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyWord
{
   /// <summary>
   /// id свойства на, которое направлено действие ключевого свойства
   /// </summary>
   private int targetId;
   /// <summary>
   /// Значение ключевого слова
   /// </summary>
   private float value;
   /// <summary>
   /// Является ли свойство процентным или константным
   /// </summary>
   private bool isPercent;

   public KeyWord(int targetId, float value,bool isPercent)
   {
      this.targetId = targetId;
      this.value = value;
      this.isPercent = isPercent;
   }

   public int TargetId => targetId;

   public float Value => value;

   public bool IsPercent => isPercent;
}
