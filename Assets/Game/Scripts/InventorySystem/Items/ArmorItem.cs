﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using InventorySystem;
using Newtonsoft.Json;
using UnityEngine;
[DataContract]
public class ArmorItem : EquipmentItem
{
    
    private string ability;

    private Sprite[] armorSprites;

    public string Ability => ability;

    public Sprite[] ArmorSprites => armorSprites;
    
    [JsonConstructor]
    public ArmorItem(string path, int quantity, SpecialInventory moduleInventory):base(path,quantity,moduleInventory)
    {
        
    }
    public ArmorItem(string name,string path, int id, Sprite icon, string description, ItemType type, string equipmentType,bool isStackable, string[] tags, 
        float estimatedCost, PropertyInitInfo[] props,KeyWord[] keyWords, SpecialInventory inventory,string activeAbility, Sprite[] armorSprites,int maxStackCount = 1) : 
        base(name,path ,id, icon, description, type, equipmentType,isStackable, tags, estimatedCost, props, keyWords,inventory, maxStackCount)
    {
        ability = activeAbility;
        this.armorSprites = armorSprites;
    }
    
    protected override string DetermineLoadPath(string name)
    {
        return "Items/Armor/"+name;
    }

    protected override ItemInfo LoadInfo(string path)
    {
        return base.LoadInfo(path) as ArmorItemInfo;
    }

    protected override ItemInfo InitializeFromInfo(ItemInfo info)
    {
        var i = base.InitializeFromInfo(info) as ArmorItemInfo;
        this.ability = i.ability;
        this.armorSprites = i.armorSprites;
        return i;
    }
    
    public override Item getCopy()
    {
        return new ArmorItem(name,path,id,icon,description,type,equipmentType,isStackable,tags,estimatedCost,properties,baseKeyWords,moduleInventory,ability,armorSprites,maxStackCount);
    }
    
}
