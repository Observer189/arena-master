﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using InventorySystem;
using UnityEngine;

namespace InventorySystem
{
    [CreateAssetMenu(fileName = "New Weapon", menuName = "Items/Weapon", order = 2)]
    public class WeaponItemInfo : EquipmentItemInfo
    {
        [Tooltip("Ссылка на префаб соответствующего оружия")]
        [SerializeField]
        protected GameObject weaponPrefab;
        [Tooltip("Количество патронов в начальном магазине. -1 значит он полностью заполнен")]
        [SerializeField]
        protected int startAmmo;

        public GameObject WeaponPrefab => weaponPrefab;

        public override Item initializeItem()
        {
            var kw = baseKeyWords?.Select(k=>k.GenerateKeyWord()).ToArray();
            return new WeaponItem(name,path,id,icon,description,type,"Weapon",isStackable,tags,weaponPrefab,startAmmo,properties,kw,(SpecialInventory)initializer.InitializeInventory(),estimatedCost,maxStackCount);
        }
    }
}
/// <summary>
/// Класс, позволяющий задать начальную инициализацию любого свойства
/// </summary>
[Serializable]
public class PropertyInitInfo
{
    [Tooltip("Свойство, которое мы хотим инициализировать")]
    [SerializeField] 
    private ObjectPropertyDescription propertyDescription;
    [Tooltip("Базовое значение, инициализируемого свойства")]
    [SerializeField]
    private float baseValue;
    [Tooltip("Текущее значение, инициализируемого свойства")]
    [SerializeField]
    private float curValue;

    public float BaseValue => baseValue;

    public float CurValue => curValue;

    public ObjectPropertyDescription PropertyDescription => propertyDescription;
}
