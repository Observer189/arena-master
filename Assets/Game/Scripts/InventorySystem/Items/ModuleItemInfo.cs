﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using InventorySystem;
using UnityEngine;
[CreateAssetMenu(fileName = "New Module", menuName = "Items/Module", order = 3)]
public class ModuleItemInfo : ItemInfo
{
    [SerializeField] private KeyWordInfo[] keyWords;

    public KeyWordInfo[] KeyWords => keyWords;

    public override Item initializeItem()
    {
        var kw = keyWords.Select(k=>k.GenerateKeyWord()).ToArray();
        return new ModuleItem(name,path,id,icon,description,type,isStackable,tags,kw,estimatedCost,isStackable?maxStackCount:1);
    }
}
