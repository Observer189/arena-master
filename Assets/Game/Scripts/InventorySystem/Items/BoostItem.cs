﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using InventorySystem;
using Newtonsoft.Json;
using UnityEngine;

public class BoostItem : UsableItem
{
    /// <summary>
    /// Список эффектов, накладываемых на игрока при использовании предмета
    /// </summary>
    private EffectDescription[] effects;

    public EffectDescription[] Effects => effects;

    [JsonConstructor]
    public BoostItem(string path,int quantity):base(path,quantity)
    {
        
    }

    protected override string DetermineLoadPath(string name)
    {
        return "Items/UsableItems/Boosts/"+name;
    }

    protected override ItemInfo LoadInfo(string path)
    {
        return base.LoadInfo(path) as BoostItemInfo;
    }

    protected override ItemInfo InitializeFromInfo(ItemInfo info)
    {
        var i = base.InitializeFromInfo(info) as BoostItemInfo;
        effects = i.Effects;
        
        return i;
    }

    public BoostItem(string name,string path ,int id, Sprite icon, string description, ItemType type, 
        bool isStackable, string[] tags, ItemUseType useType, 
        bool isConsumable,AudioClip useSound,EffectDescription[] effects,
        float estimatedCost,int maxStackCount = 1) :
        base(name,path ,id, icon, description, type, isStackable, tags, useType,isConsumable ,useSound,estimatedCost,maxStackCount)
    {
        this.effects = effects;
    }

    public override Item getCopy()
    {
        return new BoostItem(name,path,id,icon,description,type,isStackable,tags,UseType,isConsumable,useSound,effects,estimatedCost,maxStackCount);
    }
}
