﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using InventorySystem;
using MoreMountains.Tools;
using Newtonsoft.Json;
using UnityEngine;

namespace InventorySystem
{

    [DataContract]
    public class InventoryController : MonoBehaviour, ISavableComponent
    {
        [Tooltip("Инициализатор инвентаря")]
        [SerializeField] private InventoryInitializer initializer;
        [JsonProperty]
        private Inventory inventory;
        public Inventory Inventory => inventory;
        /// <summary>
        ///  Ссылка на менеджер инвентарей
        /// </summary>
        private InventoryManager inventoryManager;
        [SerializeField]
        private long componentId;
        public long ComponentId => componentId;

        private void Awake()
        {
            inventoryManager = GetInventoryManager();
            inventory = initializer.InitializeInventory();
        }

        // Start is called before the first frame update
        void Start()
        {
            //inventoryManager = GetInventoryManager();
            //inventory = initializer.InitializeInventory();
        }

        // Update is called once per frame
        void Update()
        {
             
        }

        protected InventoryManager GetInventoryManager()
        {
            return GameObject.FindWithTag("InventoryManager").GetComponent<InventoryManager>();
        }

        /// <summary>
        /// Выводит на консоль содержимое инвентаря
        /// </summary>
        public void PrintInventoryContent()
        {
            Debug.Log(inventory.ToString());
        }

        public void InvokeVisualisation()
        {
            inventoryManager.OpenVisualizer(inventory,inventory.VisualizerType);
        }

        [Serializable]
        public class ItemQuantityPair
        {
            public ItemInfo item;
            public int quantity;
        }
        
        [Serializable]
        public class TagList
        {
            public List<string> tags;
        }
    }
}
