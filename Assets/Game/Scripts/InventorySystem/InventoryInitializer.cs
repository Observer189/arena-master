﻿using System;
using System.Collections;
using System.Collections.Generic;
using InventorySystem;
using UnityEngine;
[Serializable]
public class InventoryInitializer
{
    [Tooltip("Имя создаваемого инвентаря")] [SerializeField]
    private string inventoryName;

    [Tooltip("Размер инвентаря в ячейках")] [SerializeField]
    private int size;
    [Tooltip("Должен ли массив автоматически расширяться если в нем не хватает места на добавление предмета")]
    [SerializeField]
    private bool isAutoExtendable;
    [Tooltip("Определяет тип инвентаря и его визуализацию")]
    [SerializeField]
    private VisualizerType visualizerType;

    [Header("Содержание инвентаря")]
    [Tooltip("Перечисление предметов инвентаря, которые будут находится там изначально")]
    [SerializeField]
    private InventoryController.ItemQuantityPair[] items;

    [Header("Special Things")] 
    [Tooltip("Является ли инвентарь обычным или содержит особые слоты")]
    [SerializeField]
    private bool isSpecial;
    /// <summary>
    /// Список возможных используемых тэгов для каждого итема
    /// </summary>
    [SerializeField] private InventoryController.TagList[] tags;

    public Inventory InitializeInventory()
    {
        Inventory inventory;
        
        if (!isSpecial)
        {
            inventory = new Inventory(inventoryName, size,isAutoExtendable,visualizerType);
        }
        else
        {
            List<string>[] lst = new List<string>[size];
            for (int i = 0; i < lst.Length; i++)
            {
                lst[i] = tags[i].tags;
            }
            Debug.Log($"Intialize inventory <{inventoryName}> with size:{size}");
            inventory = new SpecialInventory(inventoryName,size,visualizerType,lst);
        }

        var inventoryManager = GetInventoryManager();
        
        if (!isSpecial)
        {
            for (int i = 0; i < items.Length; i++)
            {
                inventory.AddItem(items[i].item.initializeItem(), items[i].quantity);
            }
        }
        else
        {
            for (int i = 0; i < items.Length; i++)
            {
                inventory.AddItemToSlot(items[i].item.initializeItem(), i);
            }
        }

        /*if (visualizerType == VisualizerType.PlayerInventory)
        {
            inventoryManager.RegisterPlayerInventory(inventory);
        }
        else if(visualizerType == VisualizerType.PlayerEquipment)
        {
            inventoryManager.RegisterPlayerEquipment(inventory);
        }*/

        return inventory;
    }
    private InventoryManager GetInventoryManager()
    {
        return GameObject.FindWithTag("InventoryManager").GetComponent<InventoryManager>();
    }
}
