﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using UnityEngine;

namespace InventorySystem
{
    [DataContract]
    public class SpecialInventory : Inventory
    {
        /// <summary>
        /// Массив тэгов разрешенных для соответсвующих итемов
        /// </summary>
        [JsonProperty]
        protected List<string>[] correspondingTags;
        
        [JsonConstructor]
        public SpecialInventory(string name, int size,VisualizerType type, List<string>[] tags) : base(name, size,false,type)
        {
            correspondingTags = tags;
        }
        /// <summary>
        /// Special Inventory не поддерживает данную функцию
        /// Поэтому просто возвращает все итемы, которые пытались добавить
        /// </summary>
        public override int AddItem(Item item, int quantity)
        {
            return quantity;
        }

        public override int AddItemToSlot(Item item, int slot)
        {
            Debug.Log("special "+slot);
            if (ItemCanBePlaced(item, slot))
            {
                return base.AddItemToSlot(item, slot);
            }
            else
            {
                return item.Quantity;
            }
        }

        public override void ReplaceItem(Item item, int slot)
        {
            if(ItemCanBePlaced(item,slot))
                base.ReplaceItem(item, slot);
          
        }
        

        /// <summary>
        /// Может ли заданный итем быть помещен в ячейку с заданный номером?
        /// Проверяет совпадение тэгов
        /// </summary>
        public override bool ItemCanBePlaced(Item item, int pos)
        {
            Debug.Log("Verification "+(base.ItemCanBePlaced(item, pos) && correspondingTags[pos].Intersect(item.Tags).Any()));
            return base.ItemCanBePlaced(item, pos) && correspondingTags[pos].Intersect(item.Tags).Any();
        }
        /// <summary>
        /// Возвращает номер слота с заявленным тэгом
        /// Или -1 если такого слота нет
        /// </summary>
        public int GetSlotWithTag(string tag)
        {
            int n = -1;
            for (int i = 0; i < correspondingTags.Length; i++)
            {
                if (correspondingTags[i].Contains(tag))
                {
                    n = i;
                    break;
                }
            }

            return n;
        }
        
        public List<int> GetSlotsWithTag(string tag)
        {
            var lst = new List<int>();
            for (int i = 0; i < correspondingTags.Length; i++)
            {
                if (correspondingTags[i].Contains(tag))
                {
                   lst.Add(i);
                }
            }

            return lst;
        }
    }
}

