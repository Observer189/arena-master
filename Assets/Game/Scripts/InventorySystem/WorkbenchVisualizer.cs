﻿using System.Collections;
using System.Collections.Generic;
using InventorySystem;
using UnityEngine;

public class WorkbenchVisualizer : SpecialInventoryVisualizer
{

    private InventoryVisualizer lastOpenedVisualizer = null;
    public override void RebuildInventory()
    {
        base.RebuildInventory();
        if (slotList[0] != null)
        {
            InventoryManager.instance.CloseWorkbench();
            if (slotList[0].Item != null)
            {
                var eqItem = (EquipmentItem)slotList[0].Item;
                InventoryManager.instance.OpenWorkBench(eqItem);
            }
            /*else
            {
                InventoryManager.instance.CloseWorkbench();
            }*/
           
        }
    }

    public override void Close()
    {
        base.Close();
        InventoryManager.instance.CloseWorkbench();
    }
}
