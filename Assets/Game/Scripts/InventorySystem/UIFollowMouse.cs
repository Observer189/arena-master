﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using UnityEngine;

namespace InventorySystem
{
    public class UIFollowMouse : MonoBehaviour
    {
        public Canvas TargetCanvas { get; set; }
        public Vector2 offset { get; set; }
        protected Vector2 _newPosition;

        public bool isReticle=false;

        public Transform playerPos;
        public Transform weaponPos;

        protected virtual void LateUpdate()
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(TargetCanvas.transform as RectTransform, Input.mousePosition, TargetCanvas.worldCamera, out _newPosition);
            if (!isReticle)
            {
                transform.position = TargetCanvas.transform.TransformPoint(_newPosition + offset);
            }
            else
            {
                transform.position = TargetCanvas.transform.TransformPoint(_newPosition) - (playerPos.position -
                                     weaponPos.position).MMProject(weaponPos.up);
                
            }
        }
    }
}

