﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using MoreMountains.Tools;
using Newtonsoft.Json;
using UnityEngine;

namespace InventorySystem
{

    [DataContract]
    public class Item
    {
         /// <summary>
         ///Имя предмета отображаемое в инвентаре 
         /// </summary>
         protected string name;
         /// <summary>
         /// Путь к Info-Файлу
         /// </summary>
         [JsonProperty]
         protected string path;
        /// <summary>
        /// Уникальный идентификатор предмета 
        /// </summary>
          protected int id;
        /// <summary>
        /// Иконка объекта в инвентаре
        /// </summary>
         protected Sprite icon;
        /// <summary>
        /// Отображаемое описание объекта
        /// </summary>
        protected string description;
        /// <summary>
        /// Может ли предмет стакаться или является уникальным
        /// </summary>
        protected bool isStackable;
        /// <summary>
        /// Максимальное количество одинаковых итемов, помещающихся в одну ячейку инвентаря
        /// </summary>
        protected int maxStackCount;
        /// <summary>
        /// Тип предмета
        /// </summary>
        protected ItemType type;
        /// <summary>
        /// Список тэгов предмета
        /// Служат для проверки возможности установки итема в слот
        /// </summary>
        protected string[] tags;
        
        
        /// <summary>
        /// Текущее количество элементов
        /// </summary>
        [MMReadOnly]
        [JsonProperty]
        protected int quantity;
        /// <summary>
        /// Примерная стоимость предмета при покупке/продаже за единицу
        /// </summary>
        protected float estimatedCost;

        public string Name => name;
        
        public int Id => id;

        public Sprite Icon => icon;

        public string Description => description;

        public int Quantity => quantity;

        public bool IsStackable => isStackable;

        public int MaxStackCount => maxStackCount;

        public string[] Tags => tags;

        public ItemType Type => type;

        public float EstimatedCost => estimatedCost;

        public Item(string name,string path,int id,Sprite icon,string description,ItemType type,bool isStackable,string[] tags,float estimatedCost,int maxStackCount=1)
        {
            this.name = name;
            this.path = path;
            this.id = id;
            this.icon = icon;
            this.description = description;
            this.type = type;
            this.isStackable = isStackable;
            this.maxStackCount = maxStackCount;
            this.tags = tags;
            this.estimatedCost = estimatedCost;
            quantity = 1;
        }
        [JsonConstructor]
        public Item(string path,int quantity)
        {
            var info = LoadInfo(path);
            InitializeFromInfo(info);
            this.quantity = quantity;
        }

        protected virtual string DetermineLoadPath(string name)
        {
            return "*/"+name;
        }

        protected virtual ItemInfo LoadInfo(string path)
        {
            //Debug.Log(path);
            return Resources.Load(path) as ItemInfo;
        }

        protected virtual ItemInfo InitializeFromInfo(ItemInfo info)
        {
            Debug.Log(info);
            name = info.Name;
            path = info.Path;
            id = info.Id;
            icon = info.Icon;
            description = info.Description;
            estimatedCost = info.EstimatedCost;
            isStackable = info.IsStackable;
            maxStackCount = info.MaxStackCount;
            tags = info.Tags;
            type = info.Type;
            
            return info;
        }

        /// <summary>
        /// возвращает полную копию но с quantity=1
        /// </summary>
        public virtual Item getCopy()
        {
            return new Item(name,path,id,icon,description,type,isStackable,tags,estimatedCost,maxStackCount);
        }

        public void SetQuantity(int q)
        {
            if (q <= maxStackCount && q > 0)
            {
                quantity = q;
            }
        }
    }

    public enum ItemType
    {
        Weapon,Module,Usable,Material,Ammo,Armor
    }
    
}
