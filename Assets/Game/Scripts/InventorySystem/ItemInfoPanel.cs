﻿using System;
using System.Collections;
using System.Collections.Generic;
using InventorySystem;
using Lean.Localization;
using UnityEngine;
using UnityEngine.UI;

public class ItemInfoPanel : MonoBehaviour
{
    [SerializeField]
    private Text name;
    [SerializeField]
    private Text type;
    [SerializeField]
    private Text description;

    [SerializeField] private Text costText;

    [SerializeField] private Camera camera;

   
    /// <summary>
    /// Позиция текущего слота в глобальных координатах
    /// </summary>
    private Vector3 itemPos;
    public void SetItem(Item item,Vector3 position)
    {
        name.text = LeanLocalization.GetTranslationText(item.Name,item.Name);
        type.text = LeanLocalization.GetTranslationText(item.Type.ToString(),item.Type.ToString());
        description.text = LeanLocalization.GetTranslationText(item.Name+"Desc",item.Description);
        costText.text = item.EstimatedCost.ToString();
        itemPos = position;
        StartCoroutine(ClampPosition());
    }
    
    /// <summary>
    /// Размещаем панель так чтобы она не вылезала за экран
    /// Делаем это на следующий кадр, чтобы она успела перестроиться
    /// </summary>
    private IEnumerator ClampPosition()
    {
        yield return new WaitForEndOfFrame();
        transform.position = itemPos + new Vector3(0,2,0);
        var rect = (RectTransform) transform;
        RectTransformUtility.ScreenPointToLocalPointInRectangle((RectTransform) transform.parent, new Vector2(0, 0), camera,
            out Vector2 bottomLeft);
        RectTransformUtility.ScreenPointToLocalPointInRectangle((RectTransform) transform.parent, new Vector2(Screen.width, Screen.height), camera,
            out Vector2 topRight);
        
        transform.localPosition = new Vector3(Mathf.Clamp(transform.localPosition.x,
                bottomLeft.x+rect.sizeDelta.x*rect.pivot.x,topRight.x-rect.sizeDelta.x*rect.pivot.x),
        Mathf.Clamp(transform.localPosition.y,bottomLeft.y+rect.sizeDelta.y*rect.pivot.y,
            topRight.y-rect.sizeDelta.y*rect.pivot.y),0);
    }
}
