﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventorySystem
{
    /// <summary>
    /// Родительский класс для всех визуализаторов инвентарей
    /// </summary>
    public abstract class InventoryVisualizer : MonoBehaviour
    {
        /// <summary>
        /// Инвентарь, который надо визуализировать
        /// </summary>
        protected Inventory inventory;
        [Header("Настройки доступа")]
        [Tooltip("Доступ на перемещение предметов внутри визуализатора")]
        public bool internalAccess=true;
        [Tooltip("Доступ на взятие предметов из этого визуализатора")]
        public VisualizerAccessLevel receiveAccess;
        
        [Tooltip("Доступ на возможность положить предмет в визуализатор")]
        public VisualizerAccessLevel putAccess;

        public Inventory Inventory => inventory;
        
        
        public virtual void Visualize(Inventory inventory)
        {
            
        }
        /// <summary>
        /// Выставляет новый инвентарь
        /// </summary>
        public virtual void SetInventory(Inventory inventory)
        {
            this.inventory = inventory;
        }
        /// <summary>
        /// Полностью перестраивает окно инвентаря
        /// </summary>
        public virtual void RebuildInventory()
        {
            
        }
        /// <summary>
        /// Закрывает визуализатор
        /// </summary>
        public virtual void Close()
        {
            
        }
        
    }

    public enum VisualizerAccessLevel
    {
        Public, Shop, Private
    }
}
