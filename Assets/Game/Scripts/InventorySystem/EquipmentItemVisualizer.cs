﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventorySystem
{
    public class EquipmentItemVisualizer : SpecialInventoryVisualizer
    {
        private EquipmentItem eqIt;
        /// <summary>
        /// служит для запоминания последнего слота, с которым проводилось сравнение
        /// чтобы потом возвращать визуализатор слота в рнормальное состояние
        /// </summary>
        private int compareSlotNum=-1;
        /// <summary>
        /// Ссылка на визуализируемое оружие
        /// </summary>
        public EquipmentItem EqIt
        {
            get => eqIt;
            set
            {
                eqIt = value;
            }
        }

        [Tooltip("Ссылка на панель свойств")]
        [SerializeField]
        private PropertyPanel propertyPanel;
        [Tooltip("Ссылка на панель для показа информации о сравниваемом предмете")]
        [SerializeField] private ItemInfoPanel _infoPanel;

        [Tooltip("Смещение инфопанели относительно слота")] 
        [SerializeField]
        private Vector3 infoPanelOffset;

        private void Start()
        {
            InventoryManager.instance.RegisterSelectSlotCallback(OnSelectSlot);
            _infoPanel.gameObject.SetActive(false);
        }

        public override void RebuildInventory()
        {
            base.RebuildInventory();
            if (eqIt != null)
            {
                propertyPanel.PrimaryInitialization(eqIt);
                eqIt.CalculateModuleBonuses();
                propertyPanel.UpdateValues();
            }
        }

        private void OnEnable()
        {
            InventoryManager.instance?.RegisterSelectSlotCallback(OnSelectSlot);
        }

        private void OnDisable()
        {
            InventoryManager.instance.UnregisterSelectSlotCallback(OnSelectSlot);
        }

        public override void Close()
        {
            base.Close();
            eqIt = null;
        }
        /// <summary>
        /// Функция срабатывает, когда игрок наводит курсор на любой слот в ЛЮБОМ визуализаторе или наоборот переводит на пустое место
        /// используется для сравнения выбранного модуля с тем, который установлен в оружии в данный момент
        /// </summary>
        /// <param name="selectedItem"></param>
        /// <param name="visualizer"></param>
        private void OnSelectSlot(Item selectedItem,InventoryVisualizer visualizer)
        {
            if (eqIt!=null && selectedItem != null && visualizer != this)
            {
                for (int i = 0; i < selectedItem.Tags.Length; i++)
                {
                    compareSlotNum = eqIt.ModuleInventory.GetSlotWithTag(selectedItem.Tags[i]);
                    if(compareSlotNum!=-1) break;
                }
                //ничего не делаем если у выбранного предмета нет подходящих тэгов
                if (compareSlotNum == -1) return;

                slotList[compareSlotNum].StartCompare();
                if (slotList[compareSlotNum].Item != null)
                {
                    _infoPanel.gameObject.SetActive(true);
                    _infoPanel.SetItem(slotList[compareSlotNum].Item,slotList[compareSlotNum].transform.position);
                }
            }
            else
            {
                if (compareSlotNum >= 0)
                {
                    slotList[compareSlotNum].EndCompare();
                    _infoPanel.gameObject.SetActive(false);
                }
            }
        }
    }
}

