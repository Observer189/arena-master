﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleStarter : MonoBehaviour
{
    /// <summary>
    /// Список всех боёв для данной локации
    /// </summary>
    public List<BattleInfo> battles;

    protected BattleController battleController;

    private void Awake()
    {
        battleController = GetComponent<BattleController>();
    }
    public void StartBattle(string battleName)
    {
        BattleInfo battleToStart = battles.Find((info => info.Name == battleName));
        battleController.StartBattle(battleToStart);
    }
}
[Serializable]
public class BattleInfo
{
    /// <summary>
    /// Имя по которому бой можно начать
    /// </summary>
    public string Name;
    /// <summary>
    /// Список участников боя
    /// </summary>
    public List<BattleParticipant> Participants;
    /// <summary>
    /// Количество очков необходимых для победы
    /// </summary>
    public int RequiredPoints;
    /// <summary>
    /// Количество очков начисляемых ежесекундно в бою
    /// Нужно для реализации возможности завершения боя по времени
    /// </summary>
    public int EverySecondPointIncrement;
    
    /// <summary>
    /// Музыка для конкретного боя
    /// Оставьте null чтобы использовать стандартную
    /// </summary>
    public AudioClip BattleMusic;

}
