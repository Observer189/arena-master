﻿using System;
using System.Collections;
using System.Collections.Generic;
using InventorySystem;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;

public class CharacterArmorHandler : CharacterAbility, MMEventListener<MMGameEvent>
{
    [Header("ShieldProperties")]
    public float baseCurShieldPower;
    public float baseShieldPower;
    public Shield shield;

    /// <summary>
    /// Время которое должно пройти с полного отключения щита, чтобы его можно было начать восстанавливать
    /// </summary>
    public float shieldRecoverDelay = 15f;

    [Header("EnergyProperties")] 
    public float baseEnergyMax;
    public float baseEnergy;
    public float baseEnergyRecoverSpeed;
    /// <summary>
    /// Воемя с последнеё траты энергии поле которого энергия начинает восполняться 
    /// </summary>
    public float energyRecoverDelay;
    
    [Header("Resists")]
    public float basePhysicalResistance;
    public float baseEnergyResistance;
    public float basePlasmaResistance;

    [Header("Abilities")] 
    public GameObject armorAbilitiesNode;

    [Header("Sprite setting")]
    [SerializeField]
    protected ArmorSpritesSetter armorSpritesSetter;
    
    protected PropertyManager _propertyManager;


    protected ObjectProperty energy;
    protected ObjectProperty shieldPower;
    protected ObjectProperty energyRecoverSpeed;
    protected ObjectProperty physResist;
    protected ObjectProperty energyResist;
    protected ObjectProperty plasmaResist;

    protected ExGUIManager guiManager;

    protected SpecialInventory playerEquipment;
    protected int armorSlot;
    protected ArmorItem armorItem = null;
    protected CharacterAbility armorAbility;

    protected ExCharacter exCharacter;
    
    /// <summary>
    /// Время с последнего потребления энергии(в секундах)
    /// </summary>
    protected float lastEnergyConsumeTime=10000000;
   /// <summary>
   /// Время с последнего отключения щита
   /// </summary>
    protected float lastShieldDeactivationTime=1000000;

   protected override void PreInitialization()
   {
       base.PreInitialization();
       _propertyManager = GetComponent<PropertyManager>();
       if (_propertyManager == null)//гарантия того что объекты с компонентом health имеют PropertyManager
       {
           _propertyManager = gameObject.AddComponent<PropertyManager>();
       }
   }

   protected override void Initialization()
    {
        base.Initialization();
        GetInventoryInfo();

        exCharacter = (ExCharacter) _character;
        SetArmor(armorItem);
    }

    protected virtual void GetInventoryInfo()
    {
        if (_character.CharacterType == Character.CharacterTypes.Player)
        {
            guiManager = (ExGUIManager) GUIManager.Instance;
            playerEquipment = (SpecialInventory) InventoryManager.instance.PlayerEquipment;
            Debug.Log(playerEquipment);
            armorSlot = playerEquipment.GetSlotWithTag("Armor");
            Debug.Log($"armorSlot = {armorSlot}");
            armorItem = (ArmorItem)playerEquipment.GetItem(armorSlot);
        }
    }


    protected void SetArmor(ArmorItem armor)
    {
        if(armorAbility!=null)
        armorAbility.enabled = false;
        
        if (armor == null)
        {
            armorSpritesSetter?.SetDefaultArmorSprites();
            energy = _propertyManager.AddProperty("Energy", baseEnergyMax, baseEnergy);
            energyRecoverSpeed = _propertyManager.AddProperty("EnergyRecoverSpeed", baseEnergyRecoverSpeed);
            shieldPower = _propertyManager.AddProperty("ShieldPower", baseShieldPower, baseCurShieldPower);
            
            physResist=_propertyManager.AddProperty("PhysicalResistance",basePhysicalResistance);
            energyResist=_propertyManager.AddProperty("EnergyResistance",baseEnergyResistance);
            plasmaResist=_propertyManager.AddProperty("PlasmaResistance",basePlasmaResistance);
            
            _propertyManager.RemoveEquipmentBonuses("Armor");
        }
        else
        {
            armorSpritesSetter?.SetArmorSprites(armor.ArmorSprites);
            for (int i = 0; i < armor.Properties.Length; i++)
            {
                var cur = armor.Properties[i];
                _propertyManager.AddProperty(cur.PropertyDescription,cur.BaseValue,cur.CurValue);
            }

            shieldPower = _propertyManager.GetPropertyById(25);
            energy = _propertyManager.GetPropertyById(26);
            energyRecoverSpeed = _propertyManager.GetPropertyById(27);

            physResist = _propertyManager.GetPropertyById(20);
            energyResist = _propertyManager.GetPropertyById(21);
            plasmaResist = _propertyManager.GetPropertyById(22);
            
            armor.CalculateModuleBonuses();
            _propertyManager.SetEquipmentBonuses(armor.EquipmentType,armor.ModuleBonuses);

            armorAbility = armorAbilitiesNode.GetComponent(armor.Ability) as CharacterAbility;
            if (armorAbility == null) Debug.LogError($"Способность с именем {armor.Ability} не найдена!");
            armorAbility.enabled = true;
        }
        
        _propertyManager.AddResistance(EffectNature.Physical,physResist);
        _propertyManager.AddResistance(EffectNature.Energy,energyResist);
        _propertyManager.AddResistance(EffectNature.Plasma, plasmaResist);
         
        if(shield!=null)
            shield.Initialize(_character,shieldPower);
        
        energy.RegisterChangeCallback(OnEnergyChanged);
        shieldPower.RegisterChangeCallback(OnShieldPowerChanged);
    }

    private void Update()
    {
        UpdateEnergyBar();
        UpdateShieldBar();

        lastEnergyConsumeTime += Time.deltaTime;
        lastShieldDeactivationTime += Time.deltaTime;

        if (lastEnergyConsumeTime > energyRecoverDelay)
        {
            energy.ChangeCurValue(energyRecoverSpeed.Value*Time.deltaTime);
        }
    }

    protected override void HandleInput()
    {
        base.HandleInput();
        if(_character.CharacterType!=Character.CharacterTypes.Player) return;

        if (Input.GetAxis("ShieldRecover")>0)
        {
            RecoverShield();
        }

    }
    /// <summary>
    /// Функция, которая постепенно восстанавливает щит за счёт энергии
    /// </summary>
    public void RecoverShield()
    {
        if (lastShieldDeactivationTime < shieldRecoverDelay) return;
        if (energy.GetCurValue() > 1 && shieldPower.GetCurValue()<shieldPower.Value)
        {
            var inc = Time.deltaTime * 10;
            shieldPower.ChangeCurValue(inc);
            energy.ChangeCurValue(-inc);
        }
    }


    protected virtual void UpdateEnergyBar()
    {
        if (_character != null)
        {
            if (_character.CharacterType == Character.CharacterTypes.Player)
            {
                // We update the health bar
                if (GUIManager.Instance != null)
                {
                    guiManager.UpdateEnergyBar(energy.GetCurValue(), 0f, energy.Value, _character.PlayerID);
                }
            }
        }
    }
    
    protected virtual void UpdateShieldBar()
    {
        
        if (_character != null)
        {
            if (_character.CharacterType == Character.CharacterTypes.Player)
            {
                // We update the health bar
                if (GUIManager.Instance != null)
                {
                    guiManager.UpdateShieldBar(shieldPower.GetCurValue(), 0f, shieldPower.Value, _character.PlayerID);
                }
            }
        }
    }
    
    
    
    protected void OnEnergyChanged(float oldCurValue, float newCurValue, float oldValue, float newValue)
    {
        if(newValue==0 && oldValue!=0) guiManager.HideEnergyBars(true,_character.PlayerID);
        else if(oldValue==0 && newValue!=0)
        {
            guiManager.HideEnergyBars(false,_character.PlayerID);
        }
        else
        {
            UpdateEnergyBar();   
        }
        if (oldCurValue > newCurValue) lastEnergyConsumeTime = 0;
    }
    
    
    protected void OnShieldPowerChanged(float oldCurValue, float newCurValue, float oldValue, float newValue)
    {
        if(newValue==0 && oldValue!=0) guiManager.HideShieldBars(true,_character.PlayerID);
        else if(oldValue==0 && newValue!=0)
        {
            guiManager.HideShieldBars(false,_character.PlayerID);
        }
        else
        {
            UpdateShieldBar();
        }

        if (oldCurValue > 0 && newCurValue <= 0) lastShieldDeactivationTime = 0;

    }
    
    public virtual void OnMMEvent(MMGameEvent gameEvent)
    {
        switch (gameEvent.EventName)
        {
            case "inventoryOpens":
                break;

            case "inventoryCloses":
                break;
            case "PlayerInventoryUpdate":
                GetInventoryInfo();
                if (_character.CharacterType == Character.CharacterTypes.Player)
                {
                    SetArmor(playerEquipment.GetItem(armorSlot) as ArmorItem);
                }
                break;
        }
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        this.MMEventStartListening();
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        this.MMEventStopListening();
    }
}
