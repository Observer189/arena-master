﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;

public class AIDashToTarget : ExCharacterDash
{[SerializeField] private int countShoot = 4;

    private float shootDuration;
    private float _shootTimer;
    private AIBrain _brain;

    
    [Header("Throw Items")] 
    protected CharacterUseItems grenade;
    [Tooltip("Which index of Grenade in CharecterUseItems")]
    public int indexOfGrenade;
    [Tooltip("How far to throw")]
    public float distanceThrow = 5;
    [Tooltip("When start throw")]
    public float startDistanceThrow = 5;
    protected override void Initialization()
    {
        base.Initialization();
        _brain = GetComponent<AIBrain>();
        Debug.Log("AIDash Initialization " + (_brain != null));
        shootDuration = DashDuration / countShoot;
        grenade = GetComponentInParent<CharacterUseItems>();
    }

    protected virtual void RotateToTarget() 
    {
        var characterMovement = this.gameObject.GetComponentInParent<TopDownController>();
        var direction = _brain.Target.transform.position - characterMovement.transform.position;
        direction = direction.normalized;
        characterMovement.CurrentDirection = new Vector3(direction.x, direction.y, direction.y);
    }

    public override void DashStart()
    {
        if (!Cooldown.Ready())
        {
            return;
        }
        Cooldown.Start();
        
        _movement.ChangeState(CharacterStates.MovementStates.Dashing);
        _dashing = true;
        _dashTimer = 0f;
        _dashOrigin = this.transform.position;
        _controller.FreeMovement = false;
        DashFeedback?.PlayFeedbacks(this.transform.position);
        PlayAbilityStartFeedbacks();

        RotateToTarget();
        var modelVector =
            gameObject.transform.parent.GetComponent<CharacterRotation2D>().MovementRotatingModel.transform.rotation *
            Vector2.left.normalized;
        Debug.Log("DashStart  _fakeTarget.transform.position " + _brain.Target.transform.position + " modelVector " + modelVector + " Razn " 
                  + (transform.position - _brain.Target.transform.position).normalized + 
                  " Distance " + (Vector2.Distance(modelVector, (transform.position - _brain.Target.transform.position).normalized) ));
        if ((Vector2.Distance(modelVector, (transform.position - _brain.Target.transform.position).normalized) < 2))
            switch (DashMode)
            {
                case DashModes.MainMovement:
                    _dashDestination = this.transform.position + _controller.CurrentDirection.normalized * DashDistance;
                    break;

                case DashModes.Fixed:
                    Debug.LogError("None");
                    break;

                case DashModes.SecondaryMovement:
                    Debug.LogError("None");
                    break;

                case DashModes.MousePosition:
                    Debug.LogError("None");
                    break;
            }

        if (shouldChangeLayer)
        {
            ChangeLayer(LayerMask.NameToLayer(layerWhileDashing));
        }
    }

    public override void ProcessAbility()
    {
        Debug.Log("AIDash ProcessAbility");
        Cooldown.Update();
        UpdateDashBar();
        Debug.Log("DashDuration " + DashDuration + " shootDuration " + shootDuration);
        if (_dashing)
        {
            if (_dashTimer < DashDuration)
            {

                _dashAnimParameterDirection = (_dashDestination - _dashOrigin).normalized;
                _newPosition = Vector3.Lerp(_dashOrigin, _dashDestination,
                    DashCurve.Evaluate(_dashTimer / DashDuration));
                _dashTimer += Time.deltaTime;
                _shootTimer += Time.deltaTime;
                _controller.MovePosition(_newPosition);
                
                if (_shootTimer > shootDuration)
                {
                    _brain.Target.position =  transform.position + gameObject.transform.parent.GetComponent<CharacterRotation2D>().MovementRotatingModel.transform.rotation *
                        Vector2.up.normalized * distanceThrow;
                    grenade.UseItem(indexOfGrenade);
                    
                    _brain.Target.position = transform.position - gameObject.transform.parent.GetComponent<CharacterRotation2D>().MovementRotatingModel.transform.rotation *
                        Vector2.up.normalized * distanceThrow;
                    grenade.UseItem(indexOfGrenade);

                    _shootTimer = 0;
                }
            }
            else
            {
                DashStop();
            }
        }
    }

}
