﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.TopDownEngine;
using UnityEngine;

public class CharacterLookAround : CharacterAbility
{
    [Tooltip("Ссылка на Transform, за которым следует камера")]
    private Transform cameraTarget;

    [Tooltip("Требуется ли, нажатие на кнопку, чтобы можно было осмотреться")] [SerializeField]
    private bool needButtonPressed;

    [Tooltip("Кнопка при нажатии которой способность работает")] [SerializeField]
    private KeyCode useButton;

    [Tooltip("Как далеко игрок может осматриваться по умолчанию")] [SerializeField]
    private float baseVisionRange;

    private Camera mainCamera;

    [SerializeField] [Tooltip("Сотношение сдвига камеры по вектору между игроком и точкой прицеливания")]
    private float offsetCoef = 0.8f;

    private PropertyManager _propertyManager;

    private ObjectProperty visionRange;

    protected override void PreInitialization()
    {
        base.PreInitialization();
        _propertyManager = GetComponent<PropertyManager>();
    }

    protected override void Initialization()
    {
        base.Initialization();
        mainCamera = Camera.main;
        cameraTarget = transform.Find("CameraTarget");
        visionRange = _propertyManager.AddProperty("VisionRange", baseVisionRange);
    }

    public override void ProcessAbility()
    {
        base.ProcessAbility();
        if (AbilityAuthorized&&(!needButtonPressed || Input.GetKey(useButton)))
        {
            Vector2 offset = mainCamera.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            offset *= offsetCoef;
            offset = (offset.magnitude > visionRange.Value) ? offset.normalized * visionRange.Value : offset;
            cameraTarget.position = (Vector2) transform.position + offset;
            //Debug.Log((offset).magnitude);
        }
        else
        {
            cameraTarget.localPosition = Vector3.zero;
        }
    }
}