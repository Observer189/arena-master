﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.TopDownEngine;
using UnityEngine;

public class ExCharacterDash : CharacterDash2D
{
    [Header("Change layer")] 
    public bool shouldChangeLayer;
    public string layerWhileDashing;
    public GameObject modelToChange;
    [Header("Energy")] 
    public float energyConsuming;

    protected ObjectProperty energyProp;

    protected int[] normalLayer;
    Collider2D[] modelChildren;

    protected override void Initialization()
    {
        base.Initialization();
        modelChildren = modelToChange.GetComponentsInChildren<Collider2D>();
        normalLayer = new int[modelChildren.Length];
        for (int i = 0; i < modelChildren.Length; i++)
        {
            normalLayer[i] = modelChildren[i].gameObject.layer;
        }
        var propertyManager = _character.GetComponent<PropertyManager>();
        energyProp = propertyManager.GetPropertyById(26);
    }

    public override void DashStart()
    {
        if (!Cooldown.Ready() || energyProp.GetCurValue()<energyConsuming)
        {
            return;
        }

        Cooldown.Start();
        _movement.ChangeState(CharacterStates.MovementStates.Dashing);	
        _dashing = true;
        _dashTimer = 0f;
        _dashOrigin = this.transform.position;
        _controller.FreeMovement = false;
        DashFeedback?.PlayFeedbacks(this.transform.position);
        PlayAbilityStartFeedbacks();

        switch (DashMode)
        {
            case DashModes.MainMovement:
                _dashDestination = this.transform.position + _controller.CurrentDirection.normalized * DashDistance;
                break;

            case DashModes.Fixed:
                _dashDestination = this.transform.position + DashDirection.normalized * DashDistance;
                break;

            case DashModes.SecondaryMovement:
                _dashDestination = this.transform.position + (Vector3)_character.LinkedInputManager.SecondaryMovement.normalized * DashDistance;
                break;

            case DashModes.MousePosition:
                _inputPosition = _mainCamera.ScreenToWorldPoint(Input.mousePosition);
                _inputPosition.z = this.transform.position.z;
                _dashDestination = this.transform.position + (_inputPosition - this.transform.position).normalized * DashDistance;
                break;
        }
        energyProp.ChangeCurValue(-energyConsuming);
        if (shouldChangeLayer)
        {
            ChangeLayer(LayerMask.NameToLayer(layerWhileDashing));
        }
    }

    protected override void DashStop()
    {
        base.DashStop();
        RecoverLayer();
    }
    
    /// <summary>
    /// Меняет слой объекта и всех его дочерних объектов на заданный
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="layer"></param>
    protected void RecoverLayer()
    {
        for (int i = 0; i < modelChildren.Length; i++)
        {
            modelChildren[i].gameObject.layer = normalLayer[i];
        }
    }

    protected void ChangeLayer(int layer)
    {
        foreach (var mod in modelChildren)
        {
            mod.gameObject.layer = layer;
        }
    }
}
