﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;

public class ExCharacterRun : CharacterRun,MMEventListener<MMStateChangeEvent<CharacterActionState>>
{
    [Header("Properties")]
    [Tooltip("Базовое ускорение при беге")]
    [SerializeField]
    protected float baseRunningAcceleration;

    [SerializeField] protected float runThreshold = 1f;
    
    [SerializeField]
    private EffectDescription runEffect;
    
    protected PropertyManager propertyManager;

    protected ObjectProperty runningAcceleration;
    
    protected ExCharacter exCharacter; 
    
    protected CharacterRotation2D characterRotation;

    protected override void Initialization()
    {
        base.Initialization();
        propertyManager = GetComponent<PropertyManager>();

        runningAcceleration = propertyManager.AddProperty("RunningAcceleration", baseRunningAcceleration);
        characterRotation = GetComponent<CharacterRotation2D>();
        if (_character is ExCharacter ex) exCharacter = ex;
    }

    public override void RunStart()
    {
        if ( !AbilityAuthorized // if the ability is not permitted
             || (!_controller.Grounded) // or if we're not grounded
             || (_condition.CurrentState != CharacterStates.CharacterConditions.Normal) // or if we're not in normal conditions
             || (_movement.CurrentState != CharacterStates.MovementStates.Walking) 
             ||(exCharacter!=null && (exCharacter.actionState.CurrentState==CharacterActionState.UsingItem
                 || exCharacter.actionState.CurrentState==CharacterActionState.ThrowingItem
                 || exCharacter.actionState.CurrentState==CharacterActionState.Shooting
                 || exCharacter.actionState.CurrentState==CharacterActionState.Reloading))) // or if we're not walking
        {
            // we do nothing and exit
            return;
        }
        
        if(_controller.CurrentMovement.magnitude<runThreshold) return;

        // if the player presses the run button and if we're on the ground and not crouching and we can move freely, 
        // then we change the movement speed in the controller's parameters.
        if (_characterMovement != null)
        {
            propertyManager.AddEffect(new Effect(runEffect,propertyManager));
        }
        if (exCharacter != null)
        {
            exCharacter.actionState.ChangeState(CharacterActionState.Running);
            if (exCharacter.BlockingRotationMode == BlockingRotationModes.BlockWhenRun)
            {
                if (characterRotation != null)
                {
                    characterRotation.RotationMode = CharacterRotation2D.RotationModes.MovementDirection;
                    characterRotation.ShouldRotateToFaceWeaponDirection = false;
                }
            }
        }
       

        // if we're not already running, we trigger our sounds
        if (_movement.CurrentState != CharacterStates.MovementStates.Running)
        {
            PlayAbilityStartSfx();
            PlayAbilityUsedSfx();
            PlayAbilityStartFeedbacks();
            _runningStarted = true;
        }

        _movement.ChangeState(CharacterStates.MovementStates.Running);
    }

    public override void RunStop()
    {
        if (_runningStarted)
        {
            if(exCharacter!=null)
                exCharacter.actionState.ChangeState(CharacterActionState.Idle);
            
            RunInterrupt();
        }            
    }

    protected void RunInterrupt()
    {
        if (_runningStarted)
        {
            // if the run button is released, we revert back to the walking speed.
            if ((_characterMovement != null))
            {
                _movement.ChangeState(CharacterStates.MovementStates.Idle);
            }
            Debug.Log("Interrupt");
            propertyManager.RemoveEffect(runEffect.Id);
            if (exCharacter != null && exCharacter.BlockingRotationMode == BlockingRotationModes.BlockWhenRun)
            {
                if (characterRotation != null)
                {
                    characterRotation.RotationMode = CharacterRotation2D.RotationModes.WeaponDirection;
                    characterRotation.ShouldRotateToFaceWeaponDirection = true;
                }
            }

            StopFeedbacks();
            StopSfx();
            _runningStarted = false;
        }
    }

    protected override void HandleRunningExit()
    {
        if(_controller.CurrentMovement.magnitude<=runThreshold) RunInterrupt();
    }

    public void OnMMEvent(MMStateChangeEvent<CharacterActionState> eventType)
    {
        if (eventType.NewState != CharacterActionState.Running)
        {
            RunInterrupt();
        }
    }
    
    protected override void OnEnable()
    {
        base.OnEnable();
        this.MMEventStartListening();
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        this.MMEventStopListening();
    }
}
