﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;

public class ExCharacterMovement : CharacterMovement,
    MMEventListener<MMGameEvent>, 
    MMEventListener<DialogueEvent>,
    MMEventListener<TopDownEngineEvent> 
{

    protected PropertyManager _propertyManager;

    private ObjectProperty movementSpeed;
    protected override void Initialization()
    {
        base.Initialization();
        _propertyManager = GetComponent<PropertyManager>();
        if (_propertyManager == null)//гарантия того что объекты с компонентом имеют PropertyManager
        {
            _propertyManager = gameObject.AddComponent<PropertyManager>();
        }
        movementSpeed=_propertyManager.AddProperty("MovementSpeed",WalkSpeed);
        movementSpeed.RegisterChangeCallback(OnMovementSpeedChange);
    }

    protected void OnMovementSpeedChange(float oldCurValue, float newCurValue, float oldValue, float newValue)
    {
        MovementSpeed = movementSpeed.GetCurValue();
    }
    
    
    public virtual void OnMMEvent(MMGameEvent gameEvent)
    {
        switch (gameEvent.EventName)
        {
            case "inventoryOpens":
                Debug.Log("Stop movement!");
                _controller.SetMovement(Vector3.zero);
                _movement.ChangeState(CharacterStates.MovementStates.Idle);
                MovementForbidden = true;
                break;
            case "inventoryCloses":
                MovementForbidden = false;
                Debug.Log("Start movement!");
                break;
            
        }
    }
    
    public void OnMMEvent(DialogueEvent dEvent)
    {
        switch (dEvent.eventType)
        {
            case DialogueEventType.DialogueOpen:
                _controller.SetMovement(Vector3.zero);
                _movement?.ChangeState(CharacterStates.MovementStates.Idle);
                MovementForbidden = true;
                break;
            case DialogueEventType.DialogueClose:
                MovementForbidden = false;
                break;
        }
            
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        this.MMEventStartListening<MMGameEvent>();
        this.MMEventStartListening<TopDownEngineEvent>();
        this.MMEventStartListening<DialogueEvent>();
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        this.MMEventStopListening<MMGameEvent>();
        this.MMEventStopListening<TopDownEngineEvent>();
        this.MMEventStopListening<DialogueEvent>();
    }

    public void OnMMEvent(TopDownEngineEvent eventType)
    {
        if (eventType.EventType == TopDownEngineEventTypes.Pause)
        {
            _controller.SetMovement(Vector3.zero);
            _movement.ChangeState(CharacterStates.MovementStates.Idle);
            MovementForbidden = true;
        }
        else if(eventType.EventType == TopDownEngineEventTypes.UnPause)
        {
            MovementForbidden = false;
        }
    }
}
