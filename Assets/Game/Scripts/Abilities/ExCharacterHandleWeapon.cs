﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;

public class ExCharacterHandleWeapon : CharacterHandleWeapon
{
   
   [Header("Расширенные настройки")]
   [Tooltip("разворачивает оружие при смене так, как развернут его weaponAttachment")]
   [SerializeField]
   protected bool UseAttachmentRotation=false;
  
   [Tooltip("Требуется ли находиться в состоянии прицеливания для стрельбы")]
   [SerializeField]
   protected bool needAiming = false;
   [Tooltip("Следует ли запретить стрельбу на бегу")]
   [SerializeField]
   protected bool preventShootingWhileRunning = true;

   [MMInspectorGroup("Animation settings", true, 10)]
   [SerializeField]
   protected bool changeAnimatorControllers;
   [SerializeField]
   protected RuntimeAnimatorController bareHandAnimatorController;
   [SerializeField]
   protected RuntimeAnimatorController meleeAnimatorController;
   [SerializeField]
   protected RuntimeAnimatorController handgunAnimatorController;
   [SerializeField]
   protected RuntimeAnimatorController rifleAnimatorController;

   [SerializeField] protected SpriteRenderer weaponRenderer;

   [Header("Multiple spawn points")]
   [SerializeField]
   protected bool useMultipleSpawnPoints = false;
   [SerializeField]
   protected Transform[] multipleSpawnPoints;
   
   private bool abilityAuthorized;

   protected ExCharacter exCharacter;

   protected CharacterAimAbility characterAimAbility;
   protected override void PreInitialization()
   {
      base.PreInitialization();
      if (_character is ExCharacter ex) exCharacter = ex;
      characterAimAbility=GetComponent<CharacterAimAbility>();
   }

   protected override void Initialization()
   {
      base.Initialization();
   }

   public void ChangeWeapon(Weapon newWeapon, string weaponID,WeaponItem item, bool combo = false)
   {
      Debug.Log("Change weapon start!");
      // if the character already has a weapon, we make it stop shooting
      if (CurrentWeapon != null)
      {
         CurrentWeapon.TurnWeaponOff();
         if (!combo)
         {
            ShootStop();
            if (_weaponAim != null) { _weaponAim.RemoveReticle(); }
            Destroy(CurrentWeapon.gameObject);
         }
      }

      if (newWeapon != null)
      {
         Debug.Log("Inst weapon request!");
         InstantiateWeapon(newWeapon, weaponID,item, combo);
      }
      else
      {
         CurrentWeapon = null;
      }
   }

   protected void InstantiateWeapon(Weapon newWeapon,string weaponID, WeaponItem item,bool combo = false )
   {
      if (!combo)
      {
         ///Разворачивает оружие при создании так же, как было повернуто предыдущее
         /// Хорошо работает для персонажей, у которых оружие также выполняет роль туловища
         CurrentWeapon = (Weapon)Instantiate(newWeapon, WeaponAttachment.transform.position + newWeapon.WeaponAttachmentOffset, /*(CurrentWeapon!=null)?CurrentWeapon.transform.rotation:*/Quaternion.identity);
      }
      CurrentWeapon.transform.parent = WeaponAttachment.transform;
      Debug.Log($"1: "+CurrentWeapon.transform.rotation.eulerAngles);
      if (UseAttachmentRotation)
      {
         CurrentWeapon.transform.localRotation = Quaternion.Euler(new Vector3(0,0,90));
         Debug.Log($"2: "+CurrentWeapon.transform.rotation.eulerAngles);
      }

      CurrentWeapon.transform.localPosition = newWeapon.WeaponAttachmentOffset;
      CurrentWeapon.SetOwner(_character, this);
      CurrentWeapon.WeaponID = weaponID;
      _weaponAim = CurrentWeapon.gameObject.MMGetComponentNoAlloc<WeaponAim>();
      _weaponAim.SetCurrentAim(lastWeaponAim);

      // we handle (optional) inverse kinematics (IK) 
      HandleWeaponIK();

      // we handle the weapon model
      //HandleWeaponModel(newWeapon, weaponID, combo, CurrentWeapon);
      IExWeapon exWeapon = ((IExWeapon) CurrentWeapon);
      exWeapon.Item = item;
      if (characterAimAbility != null)
      {
         if (exWeapon.NeedAimToShoot) characterAimAbility.AbilityPermitted = true;
         else
         {
            characterAimAbility.AbilityPermitted = false;
         }
      }

      if (useMultipleSpawnPoints && CurrentWeapon is ExtendedProjWeapon projWeapon)
      {
         projWeapon.MultipleSpawnPos = multipleSpawnPoints;
      }

      if(changeAnimatorControllers) ChangeAnimatorController(exWeapon);
      // we turn off the gun's emitters.
      CurrentWeapon.Initialization();
      CurrentWeapon.InitializeComboWeapons();
      CurrentWeapon.InitializeAnimatorParameters();
      InitializeAnimatorParameters();

      if (CurrentWeapon is IAmmoBasedWeapon abw)
      {
         abw.UpdateAvailableAmmo();
      }
   }

   protected void ChangeAnimatorController(IExWeapon iface)
   {
      if (exCharacter != null)
      {
         switch (iface.WeaponAnimationType)
         {
            case WeaponAnimationType.Barehanded: exCharacter._animator.runtimeAnimatorController = bareHandAnimatorController;
               break;
            case WeaponAnimationType.Melee: exCharacter._animator.runtimeAnimatorController = meleeAnimatorController;
               break;
            case WeaponAnimationType.Handgun: exCharacter._animator.runtimeAnimatorController = handgunAnimatorController;
               break;
            case WeaponAnimationType.Rifle: exCharacter._animator.runtimeAnimatorController = rifleAnimatorController;
               break;
         }
         weaponRenderer.sprite = iface.DisplayImage;

      }
   }

   protected override void InstantiateWeapon(Weapon newWeapon, string weaponID, bool combo = false)
   {
      if (!combo)
      {
         CurrentWeapon = (Weapon)Instantiate(newWeapon, WeaponAttachment.transform.position + newWeapon.WeaponAttachmentOffset, (CurrentWeapon!=null)?CurrentWeapon.transform.rotation:Quaternion.identity);
      }
      CurrentWeapon.transform.parent = WeaponAttachment.transform;
      
      if (UseAttachmentRotation)
      {
         CurrentWeapon.transform.localRotation = Quaternion.Euler(new Vector3(0,0,90));
      }
      
      CurrentWeapon.transform.localPosition = newWeapon.WeaponAttachmentOffset;
      CurrentWeapon.SetOwner(_character, this);
      CurrentWeapon.WeaponID = weaponID;
      _weaponAim = CurrentWeapon.gameObject.MMGetComponentNoAlloc<WeaponAim>();
      _weaponAim.SetCurrentAim(lastWeaponAim);

      // we handle (optional) inverse kinematics (IK) 
      HandleWeaponIK();

      // we handle the weapon model
      //HandleWeaponModel(newWeapon, weaponID, combo, CurrentWeapon);
      
      IExWeapon exWeapon = ((IExWeapon) CurrentWeapon);

      if (characterAimAbility != null)
      {
         if (exWeapon.NeedAimToShoot) characterAimAbility.AbilityPermitted = true;
         else
         {
            characterAimAbility.AbilityPermitted = false;
         }
      }
      
      if (useMultipleSpawnPoints && CurrentWeapon is ExtendedProjWeapon projWeapon)
      {
         projWeapon.MultipleSpawnPos = multipleSpawnPoints;
      }
      
      if(changeAnimatorControllers) ChangeAnimatorController(exWeapon);
      // we turn off the gun's emitters.
      CurrentWeapon.Initialization();
      CurrentWeapon.InitializeComboWeapons();
      CurrentWeapon.InitializeAnimatorParameters();
      InitializeAnimatorParameters();
   }

   public override void UpdateAmmoDisplay()
   {
      if ((GUIManager.Instance != null) && (_character.CharacterType == Character.CharacterTypes.Player))
      {
         if (CurrentWeapon == null)
         {
            GUIManager.Instance.SetAmmoDisplays(false, _character.PlayerID, AmmoDisplayID);
            return;
         }

         if (!CurrentWeapon.MagazineBased && (CurrentWeapon.WeaponAmmo == null))
         {
            GUIManager.Instance.SetAmmoDisplays(false, _character.PlayerID, AmmoDisplayID);
            return;
         }

         if (CurrentWeapon is IAmmoBasedWeapon abw)
         {
            GUIManager.Instance.SetAmmoDisplays(true, _character.PlayerID, AmmoDisplayID);
            GUIManager.Instance.UpdateAmmoDisplays(CurrentWeapon.MagazineBased, abw.AvailableAmmo+CurrentWeapon.CurrentAmmoLoaded, 
               int.MaxValue, CurrentWeapon.CurrentAmmoLoaded, CurrentWeapon.MagazineSize, _character.PlayerID, AmmoDisplayID, true);
         }
         else if (CurrentWeapon.WeaponAmmo == null)
         {
            GUIManager.Instance.SetAmmoDisplays(true, _character.PlayerID, AmmoDisplayID);
            GUIManager.Instance.UpdateAmmoDisplays(CurrentWeapon.MagazineBased, 0, 0, CurrentWeapon.CurrentAmmoLoaded, CurrentWeapon.MagazineSize, _character.PlayerID, AmmoDisplayID, false);
            return;
         }
         else
         {
            GUIManager.Instance.SetAmmoDisplays(true, _character.PlayerID, AmmoDisplayID); 
            GUIManager.Instance.UpdateAmmoDisplays(CurrentWeapon.MagazineBased, CurrentWeapon.WeaponAmmo.CurrentAmmoAvailable + CurrentWeapon.CurrentAmmoLoaded, CurrentWeapon.WeaponAmmo.MaxAmmo, CurrentWeapon.CurrentAmmoLoaded, CurrentWeapon.MagazineSize, _character.PlayerID, AmmoDisplayID, true);
            return;
         }
      }
   }

   public override void ShootStart()
   {
      if (exCharacter != null)
      {
         
         Debug.Log("AIActionShoot2D shootStart IF");
         if (((IExWeapon) CurrentWeapon).NeedAimToShoot)
         {
            
            Debug.Log("AIActionShoot2D shootStart   if (((IExWeapon) CurrentWeapon).NeedAimToShoot)");
            //Debug.Log("Need");
            if (exCharacter.actionState.CurrentState != CharacterActionState.Aiming) return;
         }
         if (exCharacter.actionState.CurrentState == CharacterActionState.Running &&
             CurrentWeapon is ExMeleeWeapon) return;
      }
      Debug.Log("AIActionShoot2D shootStart before Base");
      base.ShootStart();
   }
}
