﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using InventorySystem;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;

public class CharacterUseItems : CharacterAbility,MMEventListener<MMGameEvent>
{ 
    private PropertyManager _propertyManager;
    
    /// <summary>
    /// Ссылка на инвентарь игрока
    /// </summary>
    protected Inventory playerInventory;
    /// <summary>
    /// Структура хранящая данные обо всех используемых модулях в инвентаре и позволяющая
    /// переключаться между ними
    /// </summary>
    protected SortedDictionary<int, List<int>> usableItemData;
    /// <summary>
    /// указатель на текущий выбранный предмет
    /// номер выбранного элемента в массиве ключей
    /// </summary>
    protected int pointer;
    [Tooltip("Звук, воспроизводимый при смене используемого предмета")]
    [SerializeField]
    protected AudioClip switchItemSound;
    [Tooltip("Стартовая позиция полета метательных предметов")]
    [SerializeField]
    protected Vector2 throwingItemOffset;

    [SerializeField] 
    protected Transform throwStartPos;
    
    public CharacterActionState[] BlockingActionStates;
    
    [Tooltip("Время в секундах, требуемое для использование предмета")]
    [SerializeField]
    protected float UseItemTime;
    [Tooltip("Время в секундах, требуемое для метания предмета")]
    [SerializeField]
    protected float ThrowItemTime;

    [Header("Settings for AI")] 
    
    [SerializeField]
    protected ItemInfo[] aiItemList;

    protected string usingItemAnimationParameter = "UsingItem";
    protected string throwingItemAnimationParameter = "ThrowingItem";
    
    protected string usingSpeedAnimationParameter = "UsingItemSpeed";
    
    protected string throwingSpeedAnimationParameter = "ThrowingItemSpeed";

    protected int _usingItemAnimParamNum;
    protected int _throwingItemAnimParamNum;
    
    protected int _usingSpeedAnimParamNum;
    
    protected int _throwingSpeedAnimParamNum;
    
    protected ExCharacter exCharacter;

    protected bool isPlayer;
    protected override void PreInitialization()
    {
        base.PreInitialization();
        _propertyManager = GetComponent<PropertyManager>();
        usableItemData = new SortedDictionary<int, List<int>>();
        pointer = 0;
        isPlayer = _character.CharacterType == Character.CharacterTypes.Player;
    }

    protected override void Initialization()
    {
        base.Initialization();
        if (_character is ExCharacter c) exCharacter = c;
        GetInventoryInfo();
    }

    protected virtual void GetInventoryInfo()
    {
        if (isPlayer)
        {
            playerInventory = InventoryManager.instance.PlayerInventory;
            AnalyzeInventory();
            UpdateItemInfo();
        }
    }

    /// <summary>
    /// Анализирует инвентарь на наличие используемых модулей
    /// и таким образом заполняет usableItemData
    /// </summary>
    protected void AnalyzeInventory()
    {
        usableItemData.Clear();

        for (int i = 0; i < playerInventory.Size; i++)
        {
            if (playerInventory.Items[i] != null)
            {
                if (playerInventory.Items[i] is UsableItem item)
                {
                    if (!usableItemData.ContainsKey(item.Id))
                    {
                        var list= new List<int>();
                        list.Add(i);
                        usableItemData.Add(item.Id,list);
                    }
                    else
                    {
                        usableItemData[item.Id].Add(i);
                    }
                }
            }
        }
    }

    protected override void HandleInput()
    {
        base.HandleInput();
        if(!isPlayer) return;
        
        if (!GameManager.Instance.Paused && InputManager.Instance.InputDetectionActive)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                pointer++;
                if (pointer >= usableItemData.Count) pointer = 0;
                if (switchItemSound != null)
                {
                    MMSoundManagerSoundPlayEvent.Trigger(switchItemSound, MMSoundManager.MMSoundManagerTracks.UI,
                        transform.position, false, 0.6f);
                }
                UpdateItemInfo();
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                UseItem();
            }
            
            if (Input.GetKeyDown(KeyCode.T))
            {
                InterruptAction();
            }
        }
    }


    protected void UseItem()
    {
        if(!AbilityAuthorized || (exCharacter!=null &&BlockingActionStates.Contains(exCharacter.actionState.CurrentState)))
            return;
        Debug.Log(exCharacter.actionState.CurrentState);
        if (usableItemData.Count > 0)
        {
            var id = usableItemData.Keys.ToArray()[pointer];
            var list = usableItemData[id];
            int slot = -1;
            int min = int.MaxValue;
            //Находим слот, содержащий наименьшее количество предметов
            for (int i = 0; i < list.Count; i++)
            {
                if (playerInventory.Items[list[i]].Quantity < min)
                {
                    min = playerInventory.Items[list[i]].Quantity;
                    slot = list[i];
                }
            }
            
            var it = playerInventory.Items[slot];
            
            StartCoroutine(UseItemProcess(it,slot,id,list));
            
            UpdateItemInfo();
        }
    }
    /// <summary>
    /// Использование предмета для скриптов AI
    /// </summary>
    /// <param name="itemIndex">Номер используемого предмета из списка предметов для AI</param>
    public void UseItem(int itemIndex)
    {
        if(!AbilityAuthorized || (exCharacter!=null &&BlockingActionStates.Contains(exCharacter.actionState.CurrentState)))
            return;
        
        StartCoroutine(UseItemProcess(aiItemList[itemIndex].initializeItem(),-1,-1,null));
    }

    protected IEnumerator UseItemProcess(Item it, int slot,int id, List<int> list)
    {
        switch (it)
        {
            case BoostItem boost:
                if(exCharacter!=null) exCharacter.actionState.ChangeState(CharacterActionState.UsingItem);
                if(UseItemTime != 0)
                    yield return new WaitForSeconds(UseItemTime);
                UseBoost(boost);
                break;
            case ThrowingItem throwingItem:
                if(exCharacter!=null) exCharacter.actionState.ChangeState(CharacterActionState.ThrowingItem);
                if(ThrowItemTime != 0)
                    yield return new WaitForSeconds(ThrowItemTime);
                UseThrowingItem(throwingItem);
                break;
        }
        
        if(exCharacter!=null) exCharacter.actionState.ChangeState(CharacterActionState.Idle);
        
        //Если мы работаем не игроком а с AI то ничего с инвентарем не делаем
        if (!isPlayer) yield break;
        //Если тратим последний предмет из слота, то удаляем ссылку на слот
        if (it.Quantity == 1) list.Remove(slot);
        //Если тратим последний элемент данного типа, то удаляем этот тип из списка
        if (list.Count == 0)
        {
            usableItemData.Remove(id);
            if (pointer >= usableItemData.Count) pointer = usableItemData.Count - 1;
        }

        //Обновляем значение в инвентаре
        playerInventory.SetQuantityToSlot(it.Quantity - 1, slot);

        UpdateItemInfo();
    }

    /// <summary>
    /// Обновляет графическую информацию о выбранном в данный момент предмете
    /// </summary>
    protected void UpdateItemInfo()
    {
        if (usableItemData.Count > 0)
        {
            var id = usableItemData.Keys.ToArray()[pointer];
            var list = usableItemData[id];
            var item = playerInventory.Items[list[0]];
            int q = 0;
            for (int i = 0; i < list.Count; i++)
            {
                q += playerInventory.Items[list[i]].Quantity;
            }

            (ExGUIManager.Instance as ExGUIManager).UsableItemsPanel.UpdateItemInfo((UsableItem) item, q);
        }
        else
        {
            (ExGUIManager.Instance as ExGUIManager).UsableItemsPanel.UpdateItemInfo(null, 0);
        }
    }

    protected void UseBoost(BoostItem boost)
    {
        Debug.Log("Ем буст "+boost.Name);
        foreach (var desc in boost.Effects)
        {
            _propertyManager.AddEffect(new Effect(desc));
        }
        if (boost.UseSound != null)
        {
            MMSoundManagerSoundPlayEvent.Trigger(boost.UseSound, MMSoundManager.MMSoundManagerTracks.UI,
                transform.position);
        }
    }

    protected void UseThrowingItem(ThrowingItem proj)
    {
        var weapon = GetComponent<CharacterHandleWeapon>().CurrentWeapon;
        /*var rot = weapon.transform.rotation;
        var rotVec = throwingItemOffset.MMRotate(rot.eulerAngles.z);
        var obj = Instantiate(proj.Projectile,(Vector2)transform.position+rotVec,rot);*/
        var obj = Instantiate(proj.Projectile,throwStartPos.position,throwStartPos.rotation);
        var altProj = obj.GetComponent<AltProjectile>();
        if (_character.CharacterType == Character.CharacterTypes.AI)
        {
            ///Поменял потому что прошлая реализация ломала метод если мозгов вообще нет
            AIBrain[] brains = GetComponentsInChildren<AIBrain>();
            //у персонажа может быть несколько мозгов, надо проверять, какой включен
            var brain = brains.First(aiBrain => aiBrain.enabled);
            if (brain != null)
            {
                Debug.Log("PerformAction UseThrowingItem " + brain.Target.position + " NewDirection " + (brain.Target.position - transform.position));
                altProj.SetTargetPosition(brain.Target.position, brain.Target.position - transform.position);
            }
        }

        //obj.GetComponent<Projectile>().SetDirection(rotVec,rot);
        if (proj.UseSound != null)
        {
            MMSoundManagerSoundPlayEvent.Trigger(proj.UseSound, MMSoundManager.MMSoundManagerTracks.UI,
                transform.position);
        }
    }

    public void InterruptAction()
    {
        StopAllCoroutines();
        Debug.Log("Interrupt");
        if(exCharacter!=null)
        exCharacter.actionState.ChangeState(CharacterActionState.Idle);
    }


    public virtual void OnMMEvent(MMGameEvent gameEvent)
    {
        if (isPlayer)
        {
            switch (gameEvent.EventName)
            {
                case "inventoryOpens":
                    InterruptAction();
                    break;

                case "inventoryCloses":
                    break;
                case "PlayerInventoryUpdate":
                    GetInventoryInfo();
                    break;
            }
        }
    }
    
    protected override void OnEnable()
    {
        base.OnEnable();
        this.MMEventStartListening();
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        this.MMEventStopListening();
    }

    protected override void InitializeAnimatorParameters()
    {
        base.InitializeAnimatorParameters();
        RegisterAnimatorParameter(usingItemAnimationParameter,AnimatorControllerParameterType.Bool,out _usingItemAnimParamNum);
        RegisterAnimatorParameter(throwingItemAnimationParameter,AnimatorControllerParameterType.Bool,out _throwingItemAnimParamNum);
        RegisterAnimatorParameter(usingSpeedAnimationParameter,AnimatorControllerParameterType.Float,out _usingSpeedAnimParamNum);
        RegisterAnimatorParameter(throwingSpeedAnimationParameter,AnimatorControllerParameterType.Float,out _throwingSpeedAnimParamNum);
    }

    public override void UpdateAnimator()
    {
        base.UpdateAnimator();
        if (exCharacter != null)
        {
            MMAnimatorExtensions.UpdateAnimatorBool(_animator, usingItemAnimationParameter, exCharacter.actionState.CurrentState==CharacterActionState.UsingItem);
            MMAnimatorExtensions.UpdateAnimatorBool(_animator, throwingItemAnimationParameter, exCharacter.actionState.CurrentState==CharacterActionState.ThrowingItem);
        }
        MMAnimatorExtensions.UpdateAnimatorFloat(_animator, usingSpeedAnimationParameter, 2 / UseItemTime,
            _character.PerformAnimatorSanityChecks);
        MMAnimatorExtensions.UpdateAnimatorFloat(_animator, throwingSpeedAnimationParameter, 1 / ThrowItemTime,
            _character.PerformAnimatorSanityChecks);
    }
}
