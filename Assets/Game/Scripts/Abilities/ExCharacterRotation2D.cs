﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;

[MMHiddenProperties("AbilityStartFeedbacks", "AbilityStopFeedbacks")]
public class ExCharacterRotation2D : MoreMountains.TopDownEngine.CharacterRotation2D
{
  public GameObject[] additionalMovementRotatingModels;
  
  public GameObject[] additionalWeaponRotatingModels;


  protected override void RotateModel()
  {
    base.RotateModel();
    
    foreach (var model in additionalMovementRotatingModels)
    {
      model.transform.rotation = _newMovementQuaternion;
    }

    if (_shouldRotateTowardsWeapon)
    {
      foreach (var model in additionalWeaponRotatingModels)
      {
        model.transform.rotation = _newWeaponQuaternion;
      }
    }
  }
}
