﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using DialogueSystem;
using InventorySystem;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using MoreMountains.Feedbacks;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.PlayerLoop;
[DataContract]
public class CharacterInventoryHandler : CharacterAbility,MMEventListener<MMGameEvent>,ISavableComponent
{
    /// <summary>
    /// Оружие по умолчанию, которое будет использоваться, если игрок выбирает пустой слот
    /// </summary>
    [SerializeField]
    protected GameObject defaultWeapon;

    [SerializeField]
    protected CharacterActionState[] blockingActionStates;
  /// <summary>
  /// Ссылка на инвентарь игрока
  /// </summary>
   protected Inventory playerInventory;
     /// <summary>
     /// ссылка на снаряжение игрока
     /// </summary>
      protected SpecialInventory playerEquipment;
     /// <summary>
     /// Номер оружейного слота в снаряжении
     /// </summary>
     protected List<int> weaponSlots;
     /// <summary>
     /// Номер текущенго выбранного слота
     /// </summary>
     [JsonProperty]
     protected int curChoice;
     
     protected ExCharacterHandleWeapon chw;

     protected ExCharacter exCharacter;

     protected PropertyManager _propertyManager;
     
     private long componentId;

     public MMFeedbacks weaponChangeFeedback;

     protected override void Initialization()
     {
         base.Initialization();
         _propertyManager = GetComponent<PropertyManager>();
         if (_character is ExCharacter ex) exCharacter = ex;
         
         GetInventoryInfo();
         
         if (chw == null)
         {
             chw = _character?.FindAbility<ExCharacterHandleWeapon> ();	
         }
         curChoice = 0;
         EquipWeapon();
     }

     protected virtual void GetInventoryInfo()
     {
         playerInventory = InventoryManager.instance.PlayerInventory;
         playerEquipment = (SpecialInventory)InventoryManager.instance.PlayerEquipment;
         
         weaponSlots = playerEquipment.GetSlotsWithTag("Weapon");
     }

     protected override void HandleInput()
     {
         base.HandleInput();
         if(exCharacter!=null && blockingActionStates.Contains(exCharacter.actionState.CurrentState))
             return;
         
         
         if (!GameManager.Instance.Paused && !DialogueManager.instance.IsDialogueOpen)
         {
             var scroll = Input.GetAxis("Mouse ScrollWheel");
             if (scroll != 0)
             {
                 ScrollChoice(Math.Sign(scroll));
                 EquipWeapon();
             }

             if (Input.GetKeyDown(KeyCode.Alpha1))
             {
                 if (curChoice != 0)
                 {
                     curChoice = 0;
                     EquipWeapon();
                 }
             }
             else if (Input.GetKeyDown(KeyCode.Alpha2))
             {
                 if (curChoice != 1)
                 {
                     curChoice = 1;
                     EquipWeapon();
                 }
             }
             else if (Input.GetKeyDown(KeyCode.Alpha3))
             {
                 if (curChoice != 2)
                 {
                     curChoice = 2;
                     EquipWeapon();
                 }
             }
         }
     }

     protected void ScrollChoice(int scroll)
     {
         weaponChangeFeedback?.PlayFeedbacks();
         curChoice += scroll;
         if (curChoice >= weaponSlots.Count)
         {
             curChoice = 0;
             return;
         }
         if (curChoice < 0)
         {
             curChoice = weaponSlots.Count - 1;
             return;
         }
     }


     /// <summary>
     /// Надевает оружие из текущего слота
     /// </summary>
     protected void EquipWeapon()
     {        
         var weapon = (WeaponItem) playerEquipment.GetItem(weaponSlots[curChoice]);
         if (weapon != null)
         {
             chw.ChangeWeapon(weapon.WeaponPrefab.GetComponent<Weapon>(), "", weapon);
         }
         else
         {
             Debug.Log("Default weapon equip request!" + defaultWeapon);
             Debug.Log(chw);
             chw?.ChangeWeapon(defaultWeapon.GetComponent<Weapon>(), "");
         }
     }


     public virtual void OnMMEvent(MMGameEvent gameEvent)
     {
         switch (gameEvent.EventName)
         {
             case "inventoryOpens":
                 break;

             case "inventoryCloses" :
                 break;
             case "PlayerInventoryUpdate":
                 GetInventoryInfo();
                 EquipWeapon();
                 break;
         }
     }

     protected override void OnEnable()
     {
         base.OnEnable();
         this.MMEventStartListening();
     }

     protected override void OnDisable()
     {
         base.OnDisable();
         this.MMEventStopListening();
     }

     public long ComponentId => componentId;
}
