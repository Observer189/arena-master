﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.TopDownEngine;
using UnityEngine;

public class ExtendedHealth : Health
{
    protected PropertyManager _propertyManager;
    protected ObjectProperty hProperty;
    protected ObjectProperty physResist;
    protected ObjectProperty energyResist;
    protected ObjectProperty plasmaResist;

    protected DamageNotifier damageNotifier;
    protected override void Initialization()
    {
        base.Initialization();
        _propertyManager = GetComponent<PropertyManager>();
        if (_propertyManager == null)//гарантия того что объекты с компонентом health имеют PropertyManager
        {
            _propertyManager = gameObject.AddComponent<PropertyManager>();
        }
        
        hProperty=_propertyManager.AddProperty("Health", MaximumHealth, InitialHealth);
        hProperty.RegisterChangeCallback(OnHealthChanged);
       
        
        damageNotifier = GetComponent<DamageNotifier>();
        if(damageNotifier!=null)
        damageNotifier.InitializeParameter(CurrentHealth);
    }

    private void Update()
    { 
        // Debug.Log(physResist.GetCurValue());
    }

    protected void OnHealthChanged(float oldCurValue, float newCurValue, float oldValue, float newValue)
    {
        MaximumHealth = (int)newValue;
        float healthChange = newCurValue - oldCurValue;
        if (healthChange<0)
        {
           // Damage((int)-healthChange,null,0,0);
           // if we're already below zero, we do nothing and exit
           if ((oldCurValue <= 0) && (InitialHealth != 0))
           {
               return;
           }

           //CurrentHealth = Mathf.FloorToInt(newCurValue);
           CurrentHealth = Mathf.CeilToInt(newCurValue);
           Debug.Log($"Health:{CurrentHealth}");
           if (OnHit != null)
           {
               OnHit();
           }
           
           // we trigger a damage taken event
           MMDamageTakenEvent.Trigger(_character, null, CurrentHealth, -healthChange, oldCurValue);

           if (_animator != null)
           {
               _animator.SetTrigger("Damage");
           }

           DamageMMFeedbacks?.PlayFeedbacks(this.transform.position);
            
           // we update the health bar
           UpdateHealthBar(true);
           // if health has reached zero
           if (CurrentHealth <= 0)
           {
               // we set its health to zero (useful for the healthbar)
               CurrentHealth = 0;
               
               Kill();
               Debug.Log("Kill");
           }
        }
        else if (healthChange > 0)
        {
            //GetHealth((int)healthChange,null);
            Debug.Log("Heal");
            CurrentHealth = Mathf.FloorToInt(newCurValue);
            UpdateHealthBar(true);
        }
        else if(newCurValue!=CurrentHealth)
        {
            CurrentHealth = Mathf.FloorToInt(newCurValue);
            UpdateHealthBar(true);
        }
        
        if(damageNotifier!=null)
            damageNotifier.UpdateValue(CurrentHealth);
        //Debug.Log("HealthChange = "+healthChange);
        //Debug.Log("old = "+oldCurValue+", newValue = "+newCurValue);
        //Debug.Log(CurrentHealth);
    }

    protected void OnPhysResistChanged(float oldCurValue, float newCurValue, float oldValue, float newValue)
    {
        Debug.Log("Phys changed: "+oldValue+" "+newValue);
        Debug.Log(_propertyManager);
    }
}
