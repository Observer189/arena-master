﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;

public class CharacterAimAbility : CharacterAbility,MMEventListener<MMStateChangeEvent<CharacterStates.MovementStates>>,MMEventListener<MMStateChangeEvent<CharacterActionState>>
{
    [Tooltip("Название параметра анимации отвечающего за нахождение в прицеливании")] [SerializeField]
    protected string animationParameterName = "Aiming";

    [Tooltip("Должна ли камера при прицеливании сдвигаться ближе к прицелу. Требует CharacterLookAround")]
    [SerializeField]
    protected bool useLookAround;

    [Tooltip("Запрещает использование оружия без прицеливания")] [SerializeField]
    protected bool preventShootingWhenNotAiming;

    protected int animationParameterNum;

    protected CharacterLookAround characterLookAround;

    protected CharacterRotation2D characterRotation;

    protected ExCharacter exCharacter;

    protected override void Initialization()
    {
        base.Initialization();
        if (useLookAround) characterLookAround = GetComponent<CharacterLookAround>();
        characterRotation = GetComponent<CharacterRotation2D>();
        if (_character is ExCharacter ex) exCharacter = ex;
    }

    protected bool isAiming = false;

    public bool IsAiming => isAiming;

    protected override void HandleInput()
    {
        base.HandleInput();
        if (InputManager.Instance.InputDetectionActive)
        {
            if (Input.GetAxis("Aim") > 0)
            {
                AimStart();
            }
            else
            {
                AimStop();
            }
        }
    }

    protected void AimStart()
    {
        if (!AbilityAuthorized || _character.MovementState.CurrentState==CharacterStates.MovementStates.Running
        ||((ExCharacter)_character).actionState.CurrentState==CharacterActionState.Shooting
        ||((ExCharacter)_character).actionState.CurrentState==CharacterActionState.Reloading
        ||((ExCharacter)_character).actionState.CurrentState==CharacterActionState.UsingItem
        ||((ExCharacter)_character).actionState.CurrentState==CharacterActionState.ThrowingItem) return;
        
        isAiming = true;
        if (exCharacter != null && exCharacter.BlockingRotationMode == BlockingRotationModes.BlockWhenNotAim)
        {
            if (characterRotation != null) characterRotation.RotationMode = CharacterRotation2D.RotationModes.Both;
        }

        if (useLookAround)
            characterLookAround.AbilityPermitted = true;
        if (exCharacter != null)
        {
            exCharacter.actionState.ChangeState(CharacterActionState.Aiming);
        }

        //_condition.ChangeState(CharacterStates.CharacterConditions.Aiming);
    }

    protected void AimStop()
    {
       AimInterrupt();
       
        if (exCharacter != null && exCharacter.actionState.CurrentState==CharacterActionState.Aiming)
        {
            exCharacter.actionState.ChangeState(CharacterActionState.Idle);
        }
        //_condition.ChangeState(CharacterStates.CharacterConditions.Normal);
    }

    protected void AimInterrupt()
    {
        if (isAiming)
        {
            isAiming = false;
            if (exCharacter != null && exCharacter.BlockingRotationMode == BlockingRotationModes.BlockWhenNotAim)
            {
                if (characterRotation != null)
                {
                    characterRotation.RotationMode = CharacterRotation2D.RotationModes.MovementDirection;
                }
            }

            if (useLookAround)
                characterLookAround.AbilityPermitted = false;
        }
    }

    protected override void InitializeAnimatorParameters()
    {
        RegisterAnimatorParameter(animationParameterName, AnimatorControllerParameterType.Bool,
            out animationParameterNum);
    }

    public override void UpdateAnimator()
    {
        MMAnimatorExtensions.UpdateAnimatorBool(_animator, animationParameterNum, isAiming,
            _character._animatorParameters, _character.PerformAnimatorSanityChecks);
    }

    public void OnMMEvent(MMStateChangeEvent<CharacterStates.MovementStates> eventType)
    {
        //Debug.Log(eventType.NewState);
        if (eventType.NewState == CharacterStates.MovementStates.Running)
        {
            if(isAiming)
            AimInterrupt();
        }
    }
    
    public void OnMMEvent(MMStateChangeEvent<CharacterActionState> eventType)
    {
        //Debug.Log(eventType.NewState);
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        this.MMEventStartListening<MMStateChangeEvent<CharacterActionState>>();
        this.MMEventStartListening<MMStateChangeEvent<CharacterStates.MovementStates>>();
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        this.MMEventStopListening<MMStateChangeEvent<CharacterActionState>>();
        this.MMEventStopListening<MMStateChangeEvent<CharacterStates.MovementStates>>();
    }
}