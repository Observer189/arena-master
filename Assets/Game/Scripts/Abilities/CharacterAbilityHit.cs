﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;

public class CharacterAbilityHit : CharacterAbility
{
    public Collider2D effectCollider;
    
    public string hitAnimationParameterName;

    public float hitTime;

    protected bool isHitStarted;

    protected float lastHitStartTime=0;

    protected int hitAnimationParameter;

    protected override void Initialization()
    {
        base.Initialization();
        effectCollider.enabled = false;
    }

    public void StartHit()
    {
        if (!isHitStarted)
        {
            isHitStarted = true;
            lastHitStartTime = Time.time;
            effectCollider.enabled = true;
        }
    }

    private void Update()
    {
        if (isHitStarted && lastHitStartTime  + hitTime < Time.time)
        {
            EndHit();
        }
    }

    public void EndHit()
    {
        isHitStarted = false;
        effectCollider.enabled = false;
    }

    protected override void InitializeAnimatorParameters()
    {
        base.InitializeAnimatorParameters();
        RegisterAnimatorParameter (hitAnimationParameterName, AnimatorControllerParameterType.Bool, out hitAnimationParameter);
    }

    public override void UpdateAnimator()
    {
        base.UpdateAnimator();
        MMAnimatorExtensions.UpdateAnimatorBool(_animator, hitAnimationParameter, isHitStarted,_character._animatorParameters, _character.PerformAnimatorSanityChecks);
    }
}
