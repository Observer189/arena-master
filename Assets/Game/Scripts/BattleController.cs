﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Feedbacks;
using MoreMountains.Tools;
using UnityEngine;
using UnityEngine.Events;

public class BattleController : MonoBehaviour,MMEventListener<BattleEvent>
{
    private bool isBattleStarted;
    
    private int curPoints;

    private int requiredPoints;

    public UnityEvent OnStartBattle;

    public MMFeedbacks startBattleFeedback;
    
    public UnityEvent OnEndBattle;
    
    public MMFeedbacks endBattleFeedback;
    public BattleInfo CurrentBattle { get; private set; }

    [SerializeField]
    private AudioClip battleMusic;
    [Tooltip("Набор фидбэков старта и начала боя. Объекты к которым прикреплены " +
             "фидбэки должны иметь названия [BattleName]Start и соответсвенно [BattleName]Stop")]
    [SerializeField]
    protected MMFeedbacks[] battleStartFeedbacks;
    
    [Tooltip("Набор фидбэков старта и начала боя. Объекты к которым прикреплены " +
             "фидбэки должны иметь названия [BattleName]Start и соответсвенно [BattleName]Stop")]
    [SerializeField]
    protected MMFeedbacks[] battleEndFeedbacks;

    protected float lastPointUpdateTime;

    public void StartBattle(List<BattleParticipant> participants)
    {
        curPoints = 0;
        for (int i = 0; i < participants.Count; i++)
        {
            requiredPoints += participants[i].DeathPoints;
            participants[i].InitiateParticipation();
        }
        OnStartBattle?.Invoke();
        startBattleFeedback?.PlayFeedbacks();

        isBattleStarted = true;

    } 
    
    public void StartBattle(BattleInfo battle)
    {
        CurrentBattle = battle;
        curPoints = 0;
        requiredPoints = battle.RequiredPoints;
        for (int i = 0; i < battle.Participants.Count; i++)
        {
            //requiredPoints += participants[i].DeathPoints;
            battle.Participants[i].InitiateParticipation();
        }
        OnStartBattle?.Invoke();
        startBattleFeedback?.PlayFeedbacks();
        for (int i = 0; i < battleStartFeedbacks.Length; i++)
        {
            if(battleStartFeedbacks[i].gameObject.name == CurrentBattle.Name + "Start")
            {
                battleStartFeedbacks[i].PlayFeedbacks();
            }
        }
        
        BattleEvent.Trigger(BattleEventType.StartBattle,0,CurrentBattle);
        isBattleStarted = true;
    } 

    private void EndBattle()
    {
        OnEndBattle?.Invoke();
        endBattleFeedback?.PlayFeedbacks();
        
        for (int i = 0; i < CurrentBattle.Participants.Count; i++)
        {
            //requiredPoints += participants[i].DeathPoints;
            CurrentBattle.Participants[i]?.InterruptParticipation();
        }
        
        for (int i = 0; i < battleEndFeedbacks.Length; i++)
        {
            if(battleEndFeedbacks[i].gameObject.name == CurrentBattle.Name + "End")
            {
                battleEndFeedbacks[i].PlayFeedbacks();
            }
        }
        BattleEvent.Trigger(BattleEventType.EndBattle,0,CurrentBattle);
        CurrentBattle = null;
        isBattleStarted = false;
    }

    public void OnMMEvent(BattleEvent eventType)
    {
        Debug.Log(isBattleStarted);
        if (eventType.eventType == BattleEventType.BattlePointsChange && isBattleStarted)
        {
            Debug.Log($"+ score:{eventType.scoreChange}");
            curPoints += eventType.scoreChange;
            if (curPoints>=requiredPoints)
            {
                Debug.Log("End");
                EndBattle();
            }
        }
    }

    private void Update()
    {
        if (CurrentBattle != null && isBattleStarted)
        {
            if (lastPointUpdateTime + 1 < Time.time)
            {
                BattleEvent.Trigger(BattleEventType.BattlePointsChange,CurrentBattle.EverySecondPointIncrement);
                lastPointUpdateTime = Time.time;
            }
        }
    }

    private void OnEnable()
    {
        this.MMEventStartListening<BattleEvent> ();
    }

    private void OnDisable()
    {
        this.MMEventStopListening<BattleEvent> ();
        //MMSoundManagerSoundControlEvent.Trigger(MMSoundManagerSoundControlEventTypes.Stop, 100);
    }
}

public struct BattleEvent
{
    public BattleEventType eventType;

    public int scoreChange;

    public BattleInfo battleInfo;

    static BattleEvent e;

    public static void Trigger(BattleEventType eventType, int scoreChange, BattleInfo battleInfo = null)
    {
        e.eventType = eventType;
        e.scoreChange = scoreChange;
        e.battleInfo = battleInfo;
        MMEventManager.TriggerEvent(e);
    }
}

public enum BattleEventType
{
    StartBattle, EndBattle, BattlePointsChange
}
