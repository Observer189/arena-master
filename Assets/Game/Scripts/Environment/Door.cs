﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using MoreMountains.Feedbacks;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using Newtonsoft.Json;
using UnityEngine;
[DataContract]
public class Door : KeyOperatedZone,ISavableComponent,MMEventListener<SceneLoadEvent>
{
    [JsonProperty]
    [SerializeField]
    protected bool isOpen;
    [SerializeField]
    protected Animator animator;
    [SerializeField]
    protected MMFeedbacks openDoorFeedback;
    [SerializeField]
    protected MMFeedbacks closeDoorFeedback;

    private long componentId;

    void Start()
    {
        UpdateAnimator();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OpenDoor()
    {
        isOpen = true;
        openDoorFeedback?.PlayFeedbacks();
        UpdateAnimator();
    }

    public void CloseDoor()
    {
        isOpen = false;
        closeDoorFeedback?.PlayFeedbacks();
        UpdateAnimator();
    }

    protected void UpdateAnimator()
    {
        animator.SetBool("IsOpen",isOpen);
    }

    public void ToggleDoor()
    {
        if (isOpen)
        {
            CloseDoor();
        }
        else
        {
            OpenDoor();
        }
    }

    public void LockDoor()
    {
        Activable = false;
    }
    
    public void UnLockDoor()
    {
        Activable = true;
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        OnActivation.AddListener(ToggleDoor);
        this.MMEventStartListening<SceneLoadEvent> ();
    }

    private void OnDisable()
    {
        OnActivation.RemoveListener(ToggleDoor);
        this.MMEventStopListening<SceneLoadEvent> ();
    }

    public long ComponentId => componentId;
    public void OnMMEvent(SceneLoadEvent eventType)
    {
        if (eventType.eventType == SceneLoadEventType.LoadEnd)
        {
            if (isOpen)
            {
                OpenDoor();
            }
            else
            {
                CloseDoor();
            }
        }
    }
}
