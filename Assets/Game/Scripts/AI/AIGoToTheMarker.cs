﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MoreMountains.Tools;
using UnityEngine;

public class AIGoToTheMarker : MonoBehaviour
{
    protected Transform _marker;
    protected CharacterAstarPathfinder2D _characterPathfinder;
    [MMReadOnly]
    private int numberOfMarker = 0;
    private void Awake()
    {
        _marker = gameObject.transform.Find("Markers");
        _characterPathfinder =  gameObject.GetComponent<CharacterAstarPathfinder2D>();
    }

    private void FixedUpdate()
    {
    }
}
