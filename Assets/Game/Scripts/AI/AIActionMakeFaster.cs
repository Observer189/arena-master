﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;

public class AIActionMakeFaster : AIAction
{
    private AIDecisionTimeInState[] _timeInStates;

    [Tooltip("Насколько ускоряться каждый раз")] [SerializeField]
    public float fasterTime = (float)1.0;

    protected override void Initialization()
    {
        if (fasterTime == 0)
            Debug.LogError("Нельзя ускорять в 0 раз " + fasterTime);
        _timeInStates = GetComponents<AIDecisionTimeInState>();
    }

    public override void PerformAction()
    {
    }

    public override void OnEnterState()
    {
        base.OnEnterState();
        foreach (var state in _timeInStates)
        {
            state.AfterTimeMax *= fasterTime;
            state.AfterTimeMin *= fasterTime;
        }
    }
}