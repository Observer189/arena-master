﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.TopDownEngine;
using Pathfinding;
using Pathfinding.RVO;
using UnityEngine;
[RequireComponent(typeof(Seeker))]
[RequireComponent(typeof(CharacterMovement))]
public class CharacterAstarPathfinder2D : MonoBehaviour
{
    
    [Header("Движение")]
    [Tooltip("Порог расстояния в юнитах, используемое для пометки путевой точки посещенной")]
    [SerializeField]
    protected float pickNextWaypointDist = 2;

    [Header("Pathfinding")]
    [Tooltip("Должен ли pathfinder пересчитывать путь в update?")]
    [SerializeField]
    protected bool autoRepath;
    [Tooltip("Как часто (в секундах) путь должен пересчитываться")]
    [SerializeField]
    protected float repathRate;

    protected float lastRepathTime = float.NegativeInfinity;
    [Header("Debug")]
    /// <summary>Draws detailed gizmos constantly in the scene view instead of only when the agent is selected and settings are being modified</summary>
    public bool drawGizmos;


    protected Seeker seeker;
    protected CharacterMovement _characterMovement;
    /// <summary>
    /// Его может и не быть, но скрипт будет работать правильно
    /// </summary>
    protected RVOController _rvoController;
    
    /// <summary>Current path which is followed</summary>
    protected Path path;
    /// <summary>
    /// Индекс следующей путевой точки
    /// </summary>
    protected int nextWaypointIndex=-1;
    /// <summary>
    /// Точка, попасть в которую стремится агент
    /// </summary>
    protected Vector2 destination = Vector2.negativeInfinity;
    /// <summary>
    /// Направление движения до следующей путевой точки
    /// </summary>
    protected Vector2 direction;
    /// <summary>
    /// Пришел ли агент к конечной точке назначения
    /// </summary>
    protected bool pathComplete = true;

    private void Awake()
    {
        seeker = GetComponent<Seeker>();
        _characterMovement = GetComponent<CharacterMovement>();
        _rvoController = GetComponent<RVOController>();
    }
    /// <summary>
    /// Устанавливает новую точку назначения
    /// </summary>
    /// <param name="newDest"></param>
    public void SetNewDestination(Vector2 newDest)
    {
        destination = newDest;
    }
    /// <summary>
    /// Пересчитывает маршрут
    /// </summary>
    public void Repath()
    {
        if (seeker.IsDone())
        {
            seeker.StartPath(transform.position, destination);
        }

    }

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (_rvoController == null)
        {
            if (destination != null && destination.x != Vector2.negativeInfinity.x && autoRepath &&
                Time.time - lastRepathTime > repathRate)
            {
                Repath();
                lastRepathTime = Time.time;
            }
            DrawDebugPath();
            DetermineNextWaypoint();
        }
        MoveController();
    }

    public void DetermineNextWaypoint()
    {
        if (path!=null && path.vectorPath.Count <= 0)
        {
            return;
        }

        if (pathComplete || nextWaypointIndex < 0)
        {
            return;
        }

        if (Vector2.Distance(transform.position, path.vectorPath[nextWaypointIndex])<pickNextWaypointDist)
        {
            if (nextWaypointIndex + 1 < path.vectorPath.Count)
            {
                nextWaypointIndex++;
            }
            else
            {
                pathComplete = true;
                nextWaypointIndex = -1;
            }
        }
    }
    
    /// <summary>
    /// Moves the controller towards the next point
    /// </summary>
    protected virtual void MoveController()
    {
        if (_rvoController != null)
        {
            if (!(destination.x == Vector2.negativeInfinity.x))
            {
                var rvo = GetComponent<RVOController>();
                rvo.SetTarget(destination, _characterMovement.WalkSpeed, _characterMovement.WalkSpeed);
                var delta = rvo.CalculateMovementDelta(transform.position, Time.deltaTime);
                direction = delta.normalized;
                Debug.Log(direction);
                Vector2 _newMovement;
                _newMovement.x = direction.x;
                _newMovement.y = direction.y;
                if (delta.x==0 && delta.y == 0)
                {
                    Debug.LogWarning(_newMovement);
                }
                _characterMovement.SetMovement(_newMovement);
            }
            return;
        }

        if (pathComplete || (destination.x == Vector2.negativeInfinity.x) || (nextWaypointIndex < 0))
        {
            _characterMovement.SetMovement(Vector2.zero);
            return;
        }
        else
        {
            direction = ((Vector2)path.vectorPath[nextWaypointIndex] - new Vector2(transform.position.x,transform.position.y)).normalized;
            Vector2 _newMovement;
            _newMovement.x = direction.x;
            _newMovement.y = direction.y;
            _characterMovement.SetMovement(_newMovement);
        }

    }

    public void OnPathComplete(Path p)
    {
        // We got our path back
        if (p.error)
        {
            Debug.Log(p.errorLog);
        }
        else
        {
            path = p;
            nextWaypointIndex = 1;
            pathComplete = false;
        }
    }

    private void OnEnable()
    {
        seeker.pathCallback += OnPathComplete;
    }

    private void OnDisable()
    {
        seeker.pathCallback -= OnPathComplete;
    }
    
    /// <summary>
    /// Draws a debug line to show the current path
    /// </summary>
    protected virtual void DrawDebugPath()
    {
        if (drawGizmos && path!=null)
        {
            for (int i = 0; i < path.vectorPath.Count - 1; i++)
            {
                Debug.DrawLine(path.vectorPath[i], path.vectorPath[i + 1], Color.blue);
            }
        }
    }
}
