﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;

public class AIActionPathfindTarget2D : AIAction
{
// Start is called before the first frame update

    protected CharacterMovement _characterMovement;
    protected CharacterAstarPathfinder2D _characterPathfinder;
    public LayerMask TargetLayer;
    public float Radius;
    public double Angle = 0.5;
    public bool HoldOn = false;

    /// <summary>
    /// On init we grab our CharacterMovement ability
    /// </summary>
    protected override void Initialization()
    {
        _characterMovement = this.gameObject.GetComponentInParent<Character>()?.FindAbility<CharacterMovement>();
        _characterPathfinder = this.gameObject.GetComponentInParent<CharacterAstarPathfinder2D>();
    }

    /// <summary>
    /// On PerformAction we move
    /// </summary>
    public override void PerformAction()
    {
        Move();
    }

    /// <summary>
    /// Moves the character towards the target if needed
    /// </summary>
    protected virtual void Move()
    {
        var p = GetComponent<CharacterAstarPathfinder2D>();
        if (_brain.Target == null)
        {
            _characterPathfinder.SetNewDestination(Vector2.negativeInfinity);
            return;
        }
        else
        {
            // var a = Math.Sqrt(Radius * Radius + (Radius * 1.00)*(Radius * 1.00) - 2 * Radius* (Radius * 1.00)* Math.Cos(0.1));
            var position = transform.position;
            // Проверяет, нужно ли отойти от других, кого он заметил
            var fAll = Physics2D.OverlapCircleAll(position, Radius, TargetLayer);
            if (HoldOn && fAll.Length >= 2 &&
                Vector2.Distance(fAll[1].transform.position, _brain.Target.transform.position) <
                Vector2.Distance(transform.position, _brain.Target.transform.position))
            {
                var f = fAll[1];
                var d = new Vector2(
                    (float)((position.x - f.transform.position.x) * Math.Cos(Angle) -
                        (position.y - f.transform.position.y) * Math.Sin(Angle) + f.transform.position.x),
                    (float)((position.x - f.transform.position.x) * Math.Sin(Angle) +
                            (position.y - f.transform.position.y) * Math.Cos(Angle) + f.transform.position.y)
                );
                if (Vector2.Distance(d, _brain.Target.gameObject.transform.position) >
                    Vector2.Distance(position, _brain.Target.transform.position))
                {
                    d = new Vector2(
                        (float)((position.x - f.transform.position.x) * Math.Cos(-Angle) -
                            (position.y - f.transform.position.y) * Math.Sin(-Angle) + f.transform.position.x),
                        (float)((position.x - f.transform.position.x) * Math.Sin(-Angle) +
                                (position.y - f.transform.position.y) * Math.Cos(-Angle) + f.transform.position.y)
                    );
                }

                d = (Vector2)f.transform.position + (d - (Vector2)f.transform.position).normalized * (Radius);
                _characterPathfinder.SetNewDestination(d);
                return;
            }

            Debug.Log("Brain target");
            _characterPathfinder.SetNewDestination(_brain.Target.transform.position);
        }
    }

    public override void OnEnterState()
    {
        base.OnEnterState();
        _characterPathfinder.enabled = true;
    }

    /// <summary>
    /// On exit state we stop our movement
    /// </summary>
    public override void OnExitState()
    {
        base.OnExitState();
        _characterPathfinder.enabled = false;
        _characterPathfinder.SetNewDestination(Vector2.negativeInfinity);
        _characterMovement.SetHorizontalMovement(0f);
        _characterMovement.SetVerticalMovement(0f);
    }
}