﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using UnityEngine;

public class AIDecisionMarker : AIDecision
{
    [SerializeField] protected Transform _marker;
    public override bool Decide()
    {
        _brain.Target = _marker;
        return true;
    }
}
