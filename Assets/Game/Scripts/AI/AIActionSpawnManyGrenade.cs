﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.TopDownEngine;
using UnityEngine;
using MoreMountains.Tools;
public class AIActionSpawnManyGrenade : AIAction
{
    
    [Tooltip("Which index of Grenade in CharecterUseItems")]
    public int indexOfGrenade;

    [Tooltip("What radius should the turrets have")]
    [SerializeField]
    private int radius;
    [Tooltip("How many turrets")]
    [SerializeField]
    private int count;
    [Tooltip("How often use Granade")]
    [SerializeField]
    private float timeOfUse;

    protected CharacterUseItems grenade;
   

    private float lastTime = 0;
    public override void PerformAction()
    {
        
    }

    protected override void Initialization()
    {
        grenade = GetComponentInParent<CharacterUseItems>();
    }

    public override void OnEnterState()
    {
        lastTime -= Time.deltaTime;
        if (lastTime <= 0)
        {
            var target = _brain.Target;
            Debug.Log("PerformAction  Target Position " + _brain.Target.position); 
            for (int i = 1; i <= count; i++)
            {
                var X =  (float)Math.Cos(2 * Math.PI / count * i) * radius + transform.position.x;
                var Y = (float)Math.Sin(2 * Math.PI / count * i) * radius + transform.position.y;
                GameObject fakeTarget = new GameObject();
                fakeTarget.transform.position = new Vector2(X,Y);
                _brain.Target =  fakeTarget.transform;
                Debug.Log("PerformAction Fake X " + X + " Y " + Y );
                grenade.UseItem(indexOfGrenade);
            }
            lastTime = timeOfUse;
            _brain.Target = target;
        }
    }
 
    
    
}
