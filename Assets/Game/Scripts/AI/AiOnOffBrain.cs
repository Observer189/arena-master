﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using UnityEngine;

public class AiOnOffBrain : MonoBehaviour
{
    private AIBrain[] _brains;
    public bool defaultON = true;

    public AIBrain costylBrain;
    private void Awake()
    {
        _brains = GetComponentsInChildren<AIBrain>();
        if (defaultON)
        {
            OnBrain();
        }
        else
        {
            OffBrain();
        }
    }

    public void OffBrain()
    {
     
        foreach (var x in _brains)
        {
            x.BrainActive = false;
            Debug.Log(gameObject.name + " Brain "+ x.name + " is off");
        }

        if (costylBrain != null) costylBrain.BrainActive = false;
    }
    public void OnBrain()
    {
        _brains[0].BrainActive = true;
        if (costylBrain != null) costylBrain.BrainActive = true;
        Debug.Log(gameObject.name + " Brain "+   _brains[0].name + " is on");
    }
}
