﻿using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MoreMountains.TopDownEngine
{
    [RequireComponent(typeof(AITeam))]
    public class AIDecisionDetectTargetEnemyRadius2D : AIDecisionDetectTargetRadius2D
    {
        protected AITeamRelative _teamRelative;
        protected AITeam _team;
        protected AITeam _targetTeam;
        /// <summary>
        /// On init we grab our Character component
        /// </summary>
        public override void Initialization()
        {

            base.Initialization();
            _teamRelative = Resources.Load<AITeamRelative>("AITeamRelative");
            _team = this.gameObject.GetComponent<AITeam>();
            _collider = this.gameObject.GetComponentInParent<Rigidbody2D>().GetComponentInChildren<Collider2D>(); 

        }

        /// <summary>
        /// On Decide we check for our target
        /// </summary>
        /// <returns></returns>
        public override bool Decide()
        {
            return DetectTarget();
        }
        /// <summary>
        /// Returns true if a target Enemy is found within the circle
        /// </summary>
        /// <returns></returns>
        protected override bool DetectTarget()
        {
            _detectionCollider = null;

            if (_orientation2D != null)
            {
                _facingDirection = _orientation2D.IsFacingRight ? Vector2.right : Vector2.left;
                _raycastOrigin.x = transform.position.x + _facingDirection.x * DetectionOriginOffset.x / 2;
                _raycastOrigin.y = transform.position.y + DetectionOriginOffset.y;
            }
            else
            {
                _raycastOrigin = transform.position + DetectionOriginOffset;
            }

            // we cast a ray to the left of the agent to check for a Player
            _detectionCollider = Physics2D.OverlapCircle(_raycastOrigin, Radius, TargetLayer);
            if (_detectionCollider == null) return false;
            _targetTeam = _detectionCollider.attachedRigidbody.GetComponent<AITeam>();
            
         
            if ((_targetTeam != null && _teamRelative[(int)_targetTeam.team, (int)_team.team] != _teamRelative.A ))
            {
                return false;
            }
            else
            {
                if (!ObstacleDetection)
                {
                    _brain.Target = _detectionCollider.attachedRigidbody.transform;
                    return true;
                }
                // we cast a ray to make sure there's no obstacle
                _boxcastDirection = (Vector2)(_detectionCollider.gameObject.MMGetComponentNoAlloc<Collider2D>().bounds.center - _collider.bounds.center);
                RaycastHit2D hit = Physics2D.BoxCast(_collider.bounds.center, _collider.bounds.size, 0f, _boxcastDirection.normalized, _boxcastDirection.magnitude, ObstacleMask);
                if (!hit)
                {
                    _brain.Target = _detectionCollider.attachedRigidbody.transform;
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        /// <summary>
        /// Draws gizmos for the detection circle
        /// </summary>
        protected override void OnDrawGizmosSelected()
        {
            _raycastOrigin.x = transform.position.x + _facingDirection.x * DetectionOriginOffset.x / 2;
            _raycastOrigin.y = transform.position.y + DetectionOriginOffset.y;

            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(_raycastOrigin, Radius);
            if (_init)
            {
                Gizmos.color = _gizmoColor;
                Gizmos.DrawSphere(_raycastOrigin, Radius);
            }
        }

    }
}