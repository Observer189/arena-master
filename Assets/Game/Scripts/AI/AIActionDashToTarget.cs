﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using UnityEngine;

public class AIActionDashToTarget : AIAction
{  // Start is called before the first frame update
    protected AIDashToTarget _dash;
    public override void PerformAction()
    {
        _dash = GetComponentInParent<AIDashToTarget>();
        _dash.DashStart();
    }
    protected override void Initialization()
    {
    }

    public override void OnEnterState()
    {
    }
    void Start()
    {
        
    }
}
