﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

sealed public class AITeam : MonoBehaviour
{
    public enum Teams { NoTeam, CharacterTeam, ZombieTeam }
    [SerializeField]
    private Teams _team = Teams.NoTeam;
    public Teams team => this._team;

}
