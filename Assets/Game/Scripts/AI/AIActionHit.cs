﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using UnityEngine;

public class AIActionHit : AIAction
{
    protected CharacterAbilityHit _hit;
    public override void PerformAction()
    {
    }

    protected override void Initialization()
    {
    }

    public override void OnEnterState()
    {
        _hit = GetComponentInParent<CharacterAbilityHit>();
        _hit.StartHit();
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
