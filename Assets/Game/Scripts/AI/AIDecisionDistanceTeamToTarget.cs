﻿using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MoreMountains.TopDownEngine
{
    /// <summary>
    /// This Decision will return true if the current Brain's Target is within the specified range, false otherwise.
    /// </summary>
    [AddComponentMenu("TopDown Engine/Character/AI/Decisions/AIDecisionDistanceTeamToTarget")]
    public class AIDecisionDistanceTeamToTarget : AIDecisionDistanceToTarget
    {
        protected AITeam _team;

        protected virtual void Awake()
        {
            base.Awake();
            _team = this.gameObject.GetComponentInParent<AITeam>();
        }
        public override bool Decide()
        {
            return EvaluateDistance();
        }

        /// <summary>
        /// Returns true if the distance conditions are met
        /// </summary>
        /// <returns></returns>
        protected override bool EvaluateDistance()
        {

            if (_brain.Target == null)
            {
                return false;
            }

            AITeam targetTeams = _brain.Target.gameObject.GetComponent<AITeam>();
            if (targetTeams.team == _team.team)
                return false;

            float distance = Vector3.Distance(this.transform.position, _brain.Target.position);

            if (ComparisonMode == ComparisonModes.StrictlyLowerThan)
            {
                return (distance < Distance);
            }
            if (ComparisonMode == ComparisonModes.LowerThan)
            {
                return (distance <= Distance);
            }
            if (ComparisonMode == ComparisonModes.Equals)
            {
                return (distance == Distance);
            }
            if (ComparisonMode == ComparisonModes.GreatherThan)
            {
                return (distance >= Distance);
            }
            if (ComparisonMode == ComparisonModes.StrictlyGreaterThan)
            {
                return (distance > Distance);
            }
            return false;
        }
    }
}
