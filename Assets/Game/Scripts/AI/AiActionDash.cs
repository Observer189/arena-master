﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using UnityEngine;

public class AiActionDash : AIAction
{
    // Start is called before the first frame update
    protected AIDash _dash;
    public override void PerformAction()
    {
        _dash = GetComponentInParent<AIDash>();
        _dash.DashStart();
    }
    protected override void Initialization()
    {
    }

    public override void OnEnterState()
    {
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
