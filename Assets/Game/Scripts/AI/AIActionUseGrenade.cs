﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.TopDownEngine;
using UnityEngine;
using MoreMountains.Tools;

public class AIActionUseGrenade : AIAction
{
    private AIAction _aiActionImplementation;

    [Tooltip("Which index of Grenade in CharecterUseItems")]
    public int indexOfGrenade;

    [Tooltip("How often use Granade")]
    [SerializeField]
    private float timeOfUse;

    private float lastTime = 0;
    protected CharacterUseItems grenade;

    protected override void Initialization()
    {
        grenade = GetComponentInParent<CharacterUseItems>();
    }

    // Start is called before the first frame update
    public override void PerformAction()
    {
        RotateToTarget();   
        lastTime -= Time.deltaTime;
        if (lastTime <= 0)
        {
            grenade.UseItem(indexOfGrenade);
            lastTime = timeOfUse;
        }
    }   
    protected virtual void RotateToTarget() // Не из библиотеки
    {
        var characterMovement = this.gameObject.GetComponentInParent<TopDownController>();
        var direction = _brain.Target.position - characterMovement.transform.position;
        direction = direction.normalized;
        characterMovement.CurrentDirection =  new Vector3(direction.x, direction.y, direction.y);
    }
}