﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using UnityEngine;

public class AIActionMakeTarget : AIAction
{
    [Tooltip("Обьект, который ")] 
    [SerializeField]
    protected GameObject _target;
    public override void PerformAction()
    {
        _brain.Target = _target.transform;
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
