﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "AITeamRelative", menuName = "Configs/AITeamRelative")]
public class AITeamRelative : ScriptableObject
{
    //1 строка - NoTeam
    //2 строка - CharacterTeam
    //3 строка - ZombieTeam
    //n - нейтральное отношение
    //p - дружеское
    //a - агрессивное

    private readonly int a = -1;
    private readonly int p = 1;
    private readonly int n = 0;
    /// <summary>
    /// Отношение команд друг к другу
    /// </summary>
    public int this[int teamTarget, int team] => new int[,] {  { n, n, n },
                                                { n, p, a },
                                                 { n, a, p } }[teamTarget, team];
    /// <summary>
    /// Позитивное(дружеское) отношение
    /// </summary>
    public int P => this.p;
    /// <summary>
    /// Нейтральное отношение
    /// </summary>
    public int N => this.n;
    /// <summary>
    /// Агрессивное отношение
    /// </summary>
    public int A => this.a;

}
