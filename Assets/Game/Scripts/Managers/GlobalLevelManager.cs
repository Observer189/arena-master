﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;
/// <summary>
/// Менеджер хранящий данные обо всех уровнях игры в том числе, о том который в данный момент запущен
/// </summary>
public class GlobalLevelManager : MMPersistentSingleton<GlobalLevelManager>
{
    public Level CurrentLevel { get; private set; }

    public GameSave CurrentGameSave { get; private set; }

    public LoadMode CurrentLoadMode { get; set; }
    /// <summary>
    /// Вызывается при загрузки игры из сохранения
    /// </summary>
    /// <param name="currentGameSave"></param>
    public void LoadSave(GameSave currentGameSave)
    {
        CurrentLevel = Resources.Load<Level>($"Levels/{currentGameSave.loadedLevelName}");
        CurrentGameSave = currentGameSave;
        CurrentLoadMode = LoadMode.Standard;
    }
    /// <summary>
    /// Вызывается при намерении игрока перейти на другой уровень(не загрузить, а именно перейти)
    /// </summary>
    public void GoToLevel(Level level, LoadMode mode, int pointOfEntryIndex = -1)
    {
        CurrentLevel = level;
        CurrentLoadMode = mode;
        if (pointOfEntryIndex < 0)
        {
            if (!CurrentLevel.UseSceneSpawnPosition)
            {
                GameManager.Instance.StorePointsOfEntry(level.LevelName,level.StartPointOfEntryIndex,Character.FacingDirections.East);
            }
        }
        else
        {
            GameManager.Instance.StorePointsOfEntry(level.LevelName,pointOfEntryIndex,Character.FacingDirections.East);
        }
    }
    /// <summary>
    /// Обновляет текущий сэйв через сохранение уровня
    /// </summary>
    public void UpdateSave(List<GameObjectSave> sceneObjects, List<GameObjectSave> currentLevelObjects,string characterName, GameObjectSave currentCharacterSave)
    {
        if (CurrentGameSave == null)
        {
            CurrentGameSave = new GameSave();
            CurrentGameSave.sceneGameObjects = new Dictionary<string, List<GameObjectSave>>();
            CurrentGameSave.currentLevelObjects = new List<GameObjectSave>();
            CurrentGameSave.savedCharacters = new Dictionary<string, GameObjectSave>();
        }
        CurrentGameSave.sceneGameObjects[CurrentLevel.LevelScene] = sceneObjects;
        CurrentGameSave.savedCharacters[characterName] = currentCharacterSave;
        CurrentGameSave.currentLevelObjects = currentLevelObjects;
        CurrentGameSave.loadedLevelName = CurrentLevel.LevelName;
    }
    /// <summary>
    /// Очищаем текущий сейв при старте новой игры
    /// </summary>
    public void NewGame()
    {
        CurrentGameSave = null;
    }

    public List<GameObjectSave> GetCurrentSceneObjectsSave()
    {
        return CurrentGameSave.sceneGameObjects[CurrentLevel.LevelScene];
    }
}

public enum LoadMode
{
    Standard, IgnoreLevelSave, IgnoreSceneSave
}
