﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using Newtonsoft.Json;
using UnityEngine;

public class SaveLoadManager : MMPersistentSingleton<SaveLoadManager>,MMEventListener<MMSceneLoadingManager.LoadingSceneEvent>,MMEventListener<MMGameEvent>
{
    [Header("Save settings")]
    /// the chosen save method (json, encrypted json, binary, encrypted binary)
    public MMSaveLoadManagerMethods SaveLoadMethod = MMSaveLoadManagerMethods.Binary;
    /// the name of the destination folder
    public string FolderName = "MMTest/";
    /// the extension to use
    public string SaveFileExtension = ".testObject";
    /// the key to use to encrypt the file (if needed)
    public string EncryptionKey = "ThisIsTheKey";
    
    [Tooltip("Название сцены, которая служит экраном загрузки")]
    [SerializeField]
    private string loadingScreenScene;
    
    [Tooltip("Количество автосохранений, которые будут циклически перезаписываться")]
    public int maxAutosavesCount=3;
    
    protected IMMSaveLoadManagerMethod _saveLoadManagerMethod;

    protected GameSave currentLoadingSave;

    protected bool isCurrentSaveLoad = false;
    [SerializeField]
    protected ExLevelManager currentLevelManager;

    public ExLevelManager CurrentLevelManager
    {
        get => currentLevelManager;
        set
        {
            Debug.LogWarning("Присваивание "+value);
            currentLevelManager = value;
        }
    }

    protected override void Awake()
    {
        base.Awake();
        InitializeSaveLoadMethod();
    }

    /// <summary>
    /// Сохраняет игру
    /// </summary>
    /// <param name="saveName"></param>
    /// <returns>возвращает true если удалось сохранить и false в противном случае</returns>
    public bool SaveGame(string saveName)
    {
        if (currentLevelManager == null)
        {
            Debug.LogError("Невозможно сохранить игру! Сцена не загружена!");
            return false;
        }
        GameSave save = currentLevelManager.SaveScene();
        
        //save.PlayerData = PlayerData.instance;
        MMSaveLoadManager.Save(save, saveName, FolderName);
        Debug.Log($"Сохранение: {saveName}");
        return true;
    }

    public bool AutoSave()
    {
        return SaveGame(DetermineAutosaveName());
    }

    public void LoadGame(string fileName)
    {
        currentLoadingSave = (GameSave)MMSaveLoadManager.Load(typeof(GameSave),fileName,FolderName);
        GlobalLevelManager.Instance.LoadSave(currentLoadingSave);
        GlobalLevelManager.Instance.CurrentLoadMode = LoadMode.Standard;
        isCurrentSaveLoad = true;
        MMAdditiveSceneLoadingManager.LoadScene("GameScene");
    }

    public void LoadLastSave()
    {
        var files = GetAllGameSaves();
        LoadGame(files[0].Name);
    }

    public List<FileInfo> GetAllGameSaves()
    {
        var savePath = MMSaveLoadManager.DetermineSavePath(FolderName);
        if (!Directory.Exists(savePath))
        {
            Directory.CreateDirectory(savePath);
        }
        string[] fileNames = Directory.GetFiles(savePath);
        List<FileInfo> files = new List<FileInfo>();
        for (int i = 0; i < fileNames.Length; i++)
        {
            FileInfo f = new FileInfo(fileNames[i]);
            if (f.Exists && f.Extension==SaveFileExtension)
            {
                files.Add(f);
            }
        }
        files.Sort((f1,f2)=>-f1.LastWriteTime.CompareTo(f2.LastWriteTime));
        return files;
    }

    public void DeleteGameSave(string saveName)
    {
        MMSaveLoadManager.DeleteSave(saveName,FolderName);
    }

    protected string DetermineAutosaveName()
    {
        var files = GetAllGameSaves();
        string autoSavePattern=@"^Autosave\s*(\d*)\.arm"; 
        
        //Debug.Log(files[0].Name);
        var autosaves = files.FindAll((f) => Regex.IsMatch(f.Name,autoSavePattern,RegexOptions.IgnoreCase));
        
        Debug.Log(autosaves.Count+" "+maxAutosavesCount);
        
        if (autosaves.Count < maxAutosavesCount)
        {
            return "Autosave"+(autosaves.Count+1)+SaveFileExtension;
        }
        

        return autosaves.Last().Name;

    }
    
    protected virtual void InitializeSaveLoadMethod()
    {
        switch(SaveLoadMethod)
        {
            case MMSaveLoadManagerMethods.Binary:
                _saveLoadManagerMethod = new MMSaveLoadManagerMethodBinary();
                break; 
            case MMSaveLoadManagerMethods.BinaryEncrypted:
                _saveLoadManagerMethod = new MMSaveLoadManagerMethodBinaryEncrypted();
                (_saveLoadManagerMethod as MMSaveLoadManagerEncrypter).Key = EncryptionKey;
                break;
            case MMSaveLoadManagerMethods.Json:
                _saveLoadManagerMethod = new MMSaveLoadManagerMethodJson();
                break;
            case MMSaveLoadManagerMethods.JsonEncrypted:
                _saveLoadManagerMethod = new MMSaveLoadManagerMethodJsonEncrypted();
                (_saveLoadManagerMethod as MMSaveLoadManagerEncrypter).Key = EncryptionKey;
                break;
        }
        MMSaveLoadManager.saveLoadMethod = _saveLoadManagerMethod;

        var defaultSettings = JsonConvert.DefaultSettings.Invoke();
        defaultSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
        defaultSettings.TypeNameHandling = TypeNameHandling.Auto;
    }

    public void OnMMEvent(MMSceneLoadingManager.LoadingSceneEvent eventType)
    {
        Debug.Log(eventType.Status);
        if (eventType.Status == MMSceneLoadingManager.LoadingStatus.UnloadSceneLoader)
        {
            if (isCurrentSaveLoad)
            {
                //Debug.Log((HashSet<string>)PlayerData.instance.GetType().GetField("storyMarks").GetValue(PlayerData.instance));
                //Debug.Log(currentLoadingSave.Value.PlayerData.inventory);
                /*HashSet<string> h = new HashSet<string>();
                h.Add("123");
                h.Add("Hello reflections!");

                typeof(PlayerData)
                    .GetField("storyMarks", BindingFlags.NonPublic | BindingFlags.Instance)
                    .SetValue(PlayerData.instance,h);
                
                Debug.Log(((HashSet<string>)typeof(PlayerData).
                    GetField("storyMarks",BindingFlags.NonPublic | BindingFlags.Instance)
                    .GetValue(PlayerData.instance)).Count);*/
                
                Debug.Log("Загружаю сцену!");
                //currentSceneLoader.LoadScene(ref currentLoadingSave);
                
                ///Не забываем поставить текущий сэйв в null, чтобы при загрузке новой игры
                /// случайно не загрузилось, что-то не то
                isCurrentSaveLoad = false;
            }
        }
    }

    private void OnEnable()
    {
        this.MMEventStartListening<MMGameEvent>();
        this.MMEventStartListening<MMSceneLoadingManager.LoadingSceneEvent>();
    }

    private void OnDisable()
    {
        this.MMEventStopListening<MMGameEvent>();
        this.MMEventStopListening<MMSceneLoadingManager.LoadingSceneEvent>();
    }

    public void OnMMEvent(MMGameEvent eventType)
    {
        
    }
}
