﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;

public class ExGUIManager : GUIManager
{
    [Tooltip("Ссылка на панель отображения используемых предметов")]
    [SerializeField]
    protected UsableItemsPanel usableItemsPanel;

    public UsableItemsPanel UsableItemsPanel => usableItemsPanel;
    
    /// the health bars to update
    [Tooltip("the health bars to update")]
    public MMProgressBar[] EnergyBars;
    
    /// the health bars to update
    [Tooltip("the health bars to update")]
    public MMProgressBar[] ShieldBars;
    
    
    /// <summary>
    /// Updates the shield bar.
    /// </summary>
    /// <param name="currentHealth">Current health.</param>
    /// <param name="minHealth">Minimum health.</param>
    /// <param name="maxHealth">Max health.</param>
    /// <param name="playerID">Player I.</param>
    public virtual void UpdateShieldBar(float currentValue,float minValue,float maxValue,string playerID)
    {
        if (ShieldBars == null) { return; }
        if (ShieldBars.Length <= 0)	{ return; }

        foreach (MMProgressBar bar in ShieldBars)
        {
            if (bar == null) { continue; }
            if (bar.PlayerID == playerID)
            {
                if (maxValue <= 0)
                {
                    bar.HideBar(0);
                }
                else if(maxValue>0 && !bar.gameObject.activeSelf)
                {
                    bar.ShowBar();
                }
                else
                {
                    bar.UpdateBar(currentValue,minValue,maxValue);
                }
            }
        }

    }
    
    /// <summary>
    /// Updates the energy bar.
    /// </summary>
    /// <param name="currentHealth">Current health.</param>
    /// <param name="minHealth">Minimum health.</param>
    /// <param name="maxHealth">Max health.</param>
    /// <param name="playerID">Player I.</param>
    public virtual void UpdateEnergyBar(float currentValue,float minValue,float maxValue,string playerID)
    {
        if (EnergyBars == null) { return; }
        if (EnergyBars.Length <= 0)	{ return; }

        foreach (MMProgressBar bar in EnergyBars)
        {
            if (bar == null) { continue; }
            if (bar.PlayerID == playerID)
            {
                if (maxValue <= 0)
                {
                    bar.HideBar(0);
                }
                else if(maxValue>0 && !bar.gameObject.activeSelf)
                {
                    bar.ShowBar();
                }
                else
                {
                    bar.UpdateBar(currentValue,minValue,maxValue);
                }
            }
        }

    }

    public virtual void HideEnergyBars(bool hide,string playerID)
    {
        if (EnergyBars == null) { return; }
        if (EnergyBars.Length <= 0)	{ return; }

        foreach (MMProgressBar bar in EnergyBars)
        {
            if (bar == null) { continue; }
            if (bar.PlayerID == playerID)
            {
                if (hide)
                {
                    bar.HideBar(0.5f);
                }
                else
                {
                    bar.ShowBar();
                }
            }
        }
    }
    
    public virtual void HideShieldBars(bool hide,string playerID)
    {
        if (ShieldBars == null) { return; }
        if (ShieldBars.Length <= 0)	{ return; }

        foreach (MMProgressBar bar in ShieldBars)
        {
            if (bar == null) { continue; }
            if (bar.PlayerID == playerID)
            {
                if (hide)
                {
                    bar.HideBar(0.5f);
                }
                else
                {
                    bar.ShowBar();
                }
            }
        }
    }
}
