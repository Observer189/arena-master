﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using MoreMountains.TopDownEngine;
using Newtonsoft.Json;
using SaveLoad;
using UnityEngine;

public class ExLevelManager : LevelManager
{
    public List<GameObject> sceneObjectsToSave;
    [Tooltip("Объект который является родительским для всех наборов конфигураций уровня")]
    [SerializeField]
    protected GameObject levelSetsParent;
    /// <summary>
    /// Ссылка на объект, под которым находятся все объекты принадлежащие текущему уровню
    /// </summary>
    protected GameObject currentLevelSetParent;

    protected override void Awake()
    {
        base.Awake();
        SaveLoadManager.Instance.CurrentLevelManager = this;
        Debug.Log("Awake! "+SaveLoadManager.Instance.CurrentLevelManager);
    }

    protected override void Start()
    {
        if (GlobalLevelManager.Instance != null)
        {
            var curLevel = GlobalLevelManager.Instance.CurrentLevel;
            if (curLevel != null)
            {
                if (!curLevel.UseSceneCharacter && curLevel.PlayerCharacterPrefab != null)
                {
                    PlayerPrefabs = new Character[1];
                    PlayerPrefabs[0] = curLevel.PlayerCharacterPrefab.GetComponent<Character>();
                    SceneCharacters.Clear();
                }
            }
        }

        base.Start();
       
        LoadScene();
        
    }


    public GameSave SaveScene()
    {
        SceneLoadEvent.Trigger(SceneLoadEventType.StartSave);
        var sceneGameObjects = new List<GameObjectSave>();
        var curLevelObjects = new List<GameObjectSave>();
        var curCharacterSave = SaveGameObject(Players[0].gameObject);

        foreach (var sceneObject in sceneObjectsToSave)
        {
            var curObj = SaveGameObject(sceneObject);
            //Debug.Log($"Add {curObj.ToString()}");
            sceneGameObjects.Add(curObj);
        }

        for (int i = 0; i < currentLevelSetParent.transform.childCount; i++)
        {
            var curObj = SaveGameObject(currentLevelSetParent.transform.GetChild(i).gameObject);
            
            curLevelObjects.Add(curObj);
        }
        
        GlobalLevelManager.Instance.UpdateSave(sceneGameObjects,curLevelObjects,Players[0].gameObject.name,curCharacterSave);
        SceneLoadEvent.Trigger(SceneLoadEventType.EndSave);
        return GlobalLevelManager.Instance.CurrentGameSave;
    }

    public void LoadScene()
    {
        SceneLoadEvent.Trigger(SceneLoadEventType.StartLoad);
        ///Игнорируем загрузку позиции игрока в том случае если загружаем непосредственно новый уровень
        /// , а не тупо сохранение
        bool ignorePlayerPosition = !(GlobalLevelManager.Instance.CurrentLevel != null &&
                                    GlobalLevelManager.Instance.CurrentGameSave != null &&
                                    GlobalLevelManager.Instance.CurrentGameSave.loadedLevelName ==
                                    GlobalLevelManager.Instance.CurrentLevel.LevelName);
        
        ///Загрузка главного героя
        if (GlobalLevelManager.Instance.CurrentGameSave != null &&
            GlobalLevelManager.Instance.CurrentGameSave.savedCharacters.ContainsKey(Players[0].gameObject.name))
        {
            LoadGameObjectData(Players[0].gameObject,
                GlobalLevelManager.Instance.CurrentGameSave.savedCharacters[Players[0].gameObject.name],ignorePlayerPosition);
        }

        for (int i = 0; i < levelSetsParent.transform.childCount; i++)
            {
                var cur = levelSetsParent.transform.GetChild(i);
                if (cur.name == GlobalLevelManager.Instance.CurrentLevel.LevelName)
                {
                    currentLevelSetParent = cur.gameObject;
                }
                else
                {
                    cur.gameObject.SetActive(false);
                }
            }

            if (currentLevelSetParent == null)
            {
                Debug.LogError($"Ошибка! Не найден комплект объектов для уровня {GlobalLevelManager.Instance.CurrentLevel.LevelName}!");
            }

            if (GlobalLevelManager.Instance.CurrentLevel != null &&
                GlobalLevelManager.Instance.CurrentGameSave != null &&
                (GlobalLevelManager.Instance.CurrentLoadMode != LoadMode.IgnoreLevelSave ||
                 GlobalLevelManager.Instance.CurrentLoadMode != LoadMode.IgnoreSceneSave) &&
                GlobalLevelManager.Instance.CurrentGameSave.loadedLevelName ==
                GlobalLevelManager.Instance.CurrentLevel.LevelName)
            {
                Debug.Log("Load level objects");
                LoadLevelGameObjects(GlobalLevelManager.Instance.CurrentGameSave);
            }

            currentLevelSetParent.gameObject.SetActive(true);
        

        if (GlobalLevelManager.Instance.CurrentLoadMode != LoadMode.IgnoreSceneSave && 
            GlobalLevelManager.Instance.CurrentLevel != null)
        {
            LoadSceneObjects(GlobalLevelManager.Instance.CurrentGameSave);
        }
        
        SceneLoadEvent.Trigger(SceneLoadEventType.LoadEnd);
    }
    public void LoadLevelGameObjects(GameSave save)
    {
        if (currentLevelSetParent.transform.childCount != save.currentLevelObjects.Count)
        {
            Debug.LogError($"Ошибка! Количество загружаемых объектов уровня({save.currentLevelObjects.Count}) " +
                           $"не равно числу сохраненых({currentLevelSetParent.transform.childCount})!");
        }
        for (int i = 0; i < currentLevelSetParent.transform.childCount; i++)
        {
            LoadGameObjectData(currentLevelSetParent.transform.GetChild(i).gameObject,
                save.currentLevelObjects[i]);
        }
    }

    public void LoadSceneObjects(GameSave save)
    {
        //SceneLoadEvent.Trigger(SceneLoadEventType.StartLoad);
        var sceneGameObjects = save.sceneGameObjects[GlobalLevelManager.Instance.CurrentLevel.LevelScene];
        if (sceneGameObjects.Count != sceneObjectsToSave.Count)
        {
            Debug.LogError($"Ошибка! Количество загружаемых сценических объектов({save.sceneGameObjects.Count}) " +
                           $"не равно числу сохраненых({sceneObjectsToSave.Count})!");
        }
        for (int i = 0; i < sceneObjectsToSave.Count; i++)
        {
            LoadGameObjectData(sceneObjectsToSave[i],sceneGameObjects[i]);
        }

        //SceneLoadEvent.Trigger(SceneLoadEventType.LoadEnd);
        //MMGameEvent.Trigger("inventoryCloses");
    }
    /// <summary>
    /// Преобразует данные gameObject'а в GameObjectSave
    /// </summary>
    protected GameObjectSave SaveGameObject(GameObject gameObject)
    {
        GameObjectSave objectSave = new GameObjectSave
        {
            name = gameObject.name,
            components = new List<(string, ComponentSave)>(),
            isActive = gameObject.activeSelf,
            transform = new TransformCompSave(gameObject.transform),
            propertyManagerSave = null,
            collider = null
        };

        var propManager = gameObject.GetComponent<PropertyManager>();
        if (propManager != null)
        {
            objectSave.propertyManagerSave = new PropertyManagerSave(propManager);
        }

        var col = gameObject.GetComponent<Collider2D>();
        if (col != null)
        {
            objectSave.collider = new ColliderSave(col.enabled);
        }

        var comps = gameObject.GetComponents<MonoBehaviour>();
        foreach (var comp in comps)
        {
            var mb = (MonoBehaviour) comp;
            var savable = comp as ISavableComponent;    
            ComponentSave componentSave = new ComponentSave
            {
                isEnabled = mb.enabled,
                fields = new Dictionary<string, object>()
            };
            if (savable != null)
            {
                componentSave.componentId = savable.ComponentId;
            }

            var fields = comp.GetType().GetFields(
                BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            foreach (var f in fields)
            {
                if (f.IsDefined(typeof(JsonPropertyAttribute)))
                {
                    //Debug.Log(componentSave.fields);
                    componentSave.fields.Add(f.Name,f.GetValue(comp));
                }
            }
            objectSave.components.Add((comp.GetType().ToString(),componentSave));
                
            //Debug.Log(comp.GetType());
        }

        return objectSave;
    }

   /// <summary>
   /// Загружыет все данные из сохранения в уже существующий GameObject
   /// </summary>
   /// <param name="objectToLoad"></param>
   /// <param name="data"></param>
    protected void LoadGameObjectData(GameObject objectToLoad, GameObjectSave data, bool ignoreTransform = false)
    {
        var comps = objectToLoad.GetComponents<MonoBehaviour>();
            foreach (var kvPair in data.components)
            {
                foreach (var comp in comps)
                {
                    var savable = comp as ISavableComponent;
                    if (comp.GetType().ToString() == kvPair.Item1 && (savable==null || savable.ComponentId == kvPair.Item2.componentId))
                    {
                        var mb = (MonoBehaviour) comp;
                        mb.enabled = kvPair.Item2.isEnabled;

                        foreach (var fieldPair in kvPair.Item2.fields)
                        {
                            //Debug.Log($"Загружаю поле {fieldPair.Key}");
                            if (fieldPair.Value is long temp)
                            {
                                Debug.Log("Ага!");
                                object value;
                                
                                if (temp <= Byte.MaxValue && temp >= Byte.MinValue)
                                    value = Convert.ToByte(fieldPair.Value);
                                else if (temp >= Int16.MinValue && temp <= Int16.MaxValue)
                                    value = Convert.ToInt16(fieldPair.Value);
                                else if (temp >= Int32.MinValue && temp <= Int32.MaxValue)
                                    value = Convert.ToInt32(fieldPair.Value);
                                else
                                    value = temp;
                                
                                comp.GetType()
                                    .GetField(fieldPair.Key,
                                        BindingFlags.Public | 
                                        BindingFlags.NonPublic | 
                                        BindingFlags.Instance)
                                    .SetValue(comp,value);
                            }
                            else
                            {
                                comp.GetType()
                                    .GetField(fieldPair.Key,
                                        BindingFlags.Public | 
                                        BindingFlags.NonPublic | 
                                        BindingFlags.Instance)
                                    .SetValue(comp,fieldPair.Value);   
                            }
                        }
                    }
                }
            }
            //Debug.Log($"Set active {sceneObjectsToSave[i].name} to {save.gameObjects[i].isActive}");
            if (!ignoreTransform)
            {
                objectToLoad.transform.position = data.transform.position;
                objectToLoad.transform.rotation = Quaternion.Euler(data.transform.rotation);
                objectToLoad.transform.localScale = data.transform.scale;
            }

            var propManager = objectToLoad.GetComponent<PropertyManager>();
            if (propManager != null)
            {
                propManager.Load(data.propertyManagerSave);
            }

            var col = objectToLoad.GetComponent<Collider2D>();
            if (col != null)
            {
                col.enabled = data.collider.isEnabled;
            }

            objectToLoad.SetActive(data.isActive);
    }
   /// <summary>
   /// Переопределем способ получения точки входа с названия сцены на название уровня
   /// </summary>
   protected override void SpawnSingleCharacter()
   {
       PointsOfEntryStorage point = GameManager.Instance.GetPointsOfEntry(GlobalLevelManager.Instance.CurrentLevel.LevelName);
       if ((point != null) && (PointsOfEntry.Length >= (point.PointOfEntryIndex + 1)))
       {
           Debug.Log(point.PointOfEntryIndex);
           Players[0].RespawnAt(PointsOfEntry[point.PointOfEntryIndex], point.FacingDirection);
           TopDownEngineEvent.Trigger(TopDownEngineEventTypes.SpawnComplete, null);
           return;
       }

       if (InitialSpawnPoint != null)
       {
           InitialSpawnPoint.SpawnPlayer(Players[0]);
           TopDownEngineEvent.Trigger(TopDownEngineEventTypes.SpawnComplete, null);
           return;
       }
   }
}
