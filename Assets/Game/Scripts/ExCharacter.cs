﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class ExCharacter : Character
{
   public MMStateMachine<CharacterActionState> actionState;
   
   [SerializeField]
   protected Animator movementAnimator;
   [SerializeField]
   protected BlockingRotationModes _blockingRotationMode;

   public Animator MovementAnimator { get; protected set; }
   
   public HashSet<int> movementAnimatorParameters { get; set; }

   private int idleParameter;
   private int xVelocityParameter;
   private int yVelocityParameter;

   public BlockingRotationModes BlockingRotationMode => _blockingRotationMode;

   protected override void Initialization()
   {
      base.Initialization();
      actionState=new MMStateMachine<CharacterActionState>(gameObject,SendStateChangeEvents);
      MovementAnimator = movementAnimator;
      movementAnimatorParameters = new HashSet<int>();
   }

   protected override void InitializeAnimatorParameters()
   {
	   
	   if (_animator != null)
	   {
			MMAnimatorExtensions.AddAnimatorParameterIfExists(_animator, _groundedAnimationParameterName, out _groundedAnimationParameter, AnimatorControllerParameterType.Bool, _animatorParameters);
            MMAnimatorExtensions.AddAnimatorParameterIfExists(_animator, _currentSpeedAnimationParameterName, out _currentSpeedAnimationParameter, AnimatorControllerParameterType.Float, _animatorParameters);
            MMAnimatorExtensions.AddAnimatorParameterIfExists(_animator, _xSpeedAnimationParameterName, out _xSpeedAnimationParameter, AnimatorControllerParameterType.Float, _animatorParameters);
            MMAnimatorExtensions.AddAnimatorParameterIfExists(_animator, _ySpeedAnimationParameterName, out _ySpeedAnimationParameter, AnimatorControllerParameterType.Float, _animatorParameters);
            MMAnimatorExtensions.AddAnimatorParameterIfExists(_animator, _zSpeedAnimationParameterName, out _zSpeedAnimationParameter, AnimatorControllerParameterType.Float, _animatorParameters);
            MMAnimatorExtensions.AddAnimatorParameterIfExists(_animator, _idleAnimationParameterName, out _idleAnimationParameter, AnimatorControllerParameterType.Bool, _animatorParameters);
            MMAnimatorExtensions.AddAnimatorParameterIfExists(_animator, _aliveAnimationParameterName, out _aliveAnimationParameter, AnimatorControllerParameterType.Bool, _animatorParameters);
            MMAnimatorExtensions.AddAnimatorParameterIfExists(_animator, _randomAnimationParameterName, out _randomAnimationParameter, AnimatorControllerParameterType.Float, _animatorParameters);
            MMAnimatorExtensions.AddAnimatorParameterIfExists(_animator, _randomConstantAnimationParameterName, out _randomConstantAnimationParameter, AnimatorControllerParameterType.Float, _animatorParameters);
            MMAnimatorExtensions.AddAnimatorParameterIfExists(_animator, _xVelocityAnimationParameterName, out _xVelocityAnimationParameter, AnimatorControllerParameterType.Float, _animatorParameters);
            MMAnimatorExtensions.AddAnimatorParameterIfExists(_animator, _yVelocityAnimationParameterName, out _yVelocityAnimationParameter, AnimatorControllerParameterType.Float, _animatorParameters);
            MMAnimatorExtensions.AddAnimatorParameterIfExists(_animator, _zVelocityAnimationParameterName, out _zVelocityAnimationParameter, AnimatorControllerParameterType.Float, _animatorParameters);
            
            // we update our constant float animation parameter
            int randomConstant = UnityEngine.Random.Range(0, 1000);
            MMAnimatorExtensions.UpdateAnimatorInteger(_animator, _randomConstantAnimationParameter, randomConstant, _animatorParameters);
	   }

	   if (movementAnimator != null)
	   { 
		   movementAnimatorParameters = new HashSet<int>();
            MMAnimatorExtensions.AddAnimatorParameterIfExists(movementAnimator, _idleAnimationParameterName, out idleParameter, AnimatorControllerParameterType.Bool, movementAnimatorParameters);
            MMAnimatorExtensions.AddAnimatorParameterIfExists(movementAnimator, _xVelocityAnimationParameterName, out xVelocityParameter, AnimatorControllerParameterType.Float, movementAnimatorParameters);
            MMAnimatorExtensions.AddAnimatorParameterIfExists(movementAnimator, _yVelocityAnimationParameterName, out yVelocityParameter, AnimatorControllerParameterType.Float, movementAnimatorParameters);
	   }
   }

   protected override void UpdateAnimators()
   {
	   UpdateAnimationRandomNumber();

            if ((UseDefaultMecanim) && (_animator!= null))
			{
				MMAnimatorExtensions.UpdateAnimatorBool(_animator, _groundedAnimationParameter, _controller.Grounded,_animatorParameters, PerformAnimatorSanityChecks);
                MMAnimatorExtensions.UpdateAnimatorBool(_animator, _aliveAnimationParameter, (ConditionState.CurrentState != CharacterStates.CharacterConditions.Dead),_animatorParameters, PerformAnimatorSanityChecks);
                MMAnimatorExtensions.UpdateAnimatorFloat(_animator, _currentSpeedAnimationParameter, _controller.CurrentMovement.magnitude, _animatorParameters, PerformAnimatorSanityChecks);
                MMAnimatorExtensions.UpdateAnimatorFloat(_animator, _xSpeedAnimationParameter, _controller.CurrentMovement.x,_animatorParameters, PerformAnimatorSanityChecks);
                MMAnimatorExtensions.UpdateAnimatorFloat(_animator, _ySpeedAnimationParameter, _controller.CurrentMovement.y,_animatorParameters, PerformAnimatorSanityChecks);
                MMAnimatorExtensions.UpdateAnimatorFloat(_animator, _zSpeedAnimationParameter, _controller.CurrentMovement.z,_animatorParameters, PerformAnimatorSanityChecks);
                MMAnimatorExtensions.UpdateAnimatorBool(_animator, _idleAnimationParameter,(MovementState.CurrentState == CharacterStates.MovementStates.Idle),_animatorParameters, PerformAnimatorSanityChecks);
                MMAnimatorExtensions.UpdateAnimatorFloat(_animator, _randomAnimationParameter, _animatorRandomNumber, _animatorParameters, PerformAnimatorSanityChecks);
                MMAnimatorExtensions.UpdateAnimatorFloat(_animator, _xVelocityAnimationParameter, _controller.Velocity.x, _animatorParameters, PerformAnimatorSanityChecks);
                MMAnimatorExtensions.UpdateAnimatorFloat(_animator, _yVelocityAnimationParameter, _controller.Velocity.y, _animatorParameters, PerformAnimatorSanityChecks);
                MMAnimatorExtensions.UpdateAnimatorFloat(_animator, _zVelocityAnimationParameter, _controller.Velocity.z, _animatorParameters, PerformAnimatorSanityChecks);
                
                //Обновляем аниматор ног персонажа
                if (movementAnimator != null)
                {
	                /*MMAnimatorExtensions.UpdateAnimatorBool(movementAnimator, idleParameter,
		                (MovementState.CurrentState == CharacterStates.MovementStates.Idle), movementAnimatorParameters,
		                PerformAnimatorSanityChecks);
	                MMAnimatorExtensions.UpdateAnimatorFloat(movementAnimator, xVelocityParameter, 
		                _controller.Velocity.x, movementAnimatorParameters, PerformAnimatorSanityChecks);
	                MMAnimatorExtensions.UpdateAnimatorFloat(movementAnimator, yVelocityParameter, 
		                _controller.Velocity.y, movementAnimatorParameters, PerformAnimatorSanityChecks);*/
	                
	                movementAnimator.SetBool(_idleAnimationParameterName,(MovementState.CurrentState == CharacterStates.MovementStates.Idle));
	                Vector3 relativeNormVectorForward = _controller.Velocity.MMProject(CharacterModel.transform.right);
	                float forward = (Mathf.Abs(relativeNormVectorForward.x) > Mathf.Abs(relativeNormVectorForward.y))
		                ? relativeNormVectorForward.x
		                : relativeNormVectorForward.y;
	                movementAnimator.SetFloat(xVelocityParameter,forward);
	                //movementAnimator.SetFloat(_xVelocityAnimationParameterName,forward);
	                Vector3 relativeNormVectorLateral= _controller.Velocity.MMProject(CharacterModel.transform.up);
	                float lateral = (Mathf.Abs(relativeNormVectorLateral.x) > Mathf.Abs(relativeNormVectorLateral.y))
		                ? relativeNormVectorLateral.x
		                : relativeNormVectorLateral.y;
	                movementAnimator.SetFloat(yVelocityParameter,lateral);
	                //movementAnimator.SetFloat(_yVelocityAnimationParameterName,lateral);
	                
                }



                foreach (CharacterAbility ability in _characterAbilities)
				{
					if (ability.enabled && ability.AbilityInitialized)
					{	
						ability.UpdateAnimator();
					}
				}
                
	        }
            
   }
}

public enum BlockingRotationModes
{
	BlockWhenNotAim, BlockWhenRun, Free
}

public enum CharacterActionState
{
   Idle, Aiming, UsingItem, ThrowingItem,Shooting,Reloading,Running
}
