﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;
using UnityEngine;

public struct DialogueEvent
{
    public DialogueEventType eventType;

    static DialogueEvent e;

    public static void Trigger(DialogueEventType eventType)
    {
        e.eventType = eventType;
        MMEventManager.TriggerEvent(e);
    }
}

public enum DialogueEventType
{
    DialogueOpen, DialogueClose
}
