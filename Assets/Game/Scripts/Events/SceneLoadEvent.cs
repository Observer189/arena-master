﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using UnityEngine;
/// <summary>
/// События, вызываемые компонентом SceneSaveLoader, позволяющие отслеживать процесс загрузки сохранения 
/// </summary>
public struct SceneLoadEvent 
{
    public SceneLoadEventType eventType;

    static SceneLoadEvent e;

    public static void Trigger(SceneLoadEventType eventType)
    {
        e.eventType = eventType;
        MMEventManager.TriggerEvent(e);
    }
}

public enum SceneLoadEventType
{
    StartLoad, LoadEnd,StartSave, EndSave
}
