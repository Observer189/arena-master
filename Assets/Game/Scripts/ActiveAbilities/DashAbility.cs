﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Feedbacks;
using MoreMountains.TopDownEngine;
using UnityEngine;

public class DashAbility : ActiveAbility
{
    /// <summary>
    /// Время действия способности
    /// </summary>
    private float actionTime;
    
    private PropertyManager _propertyManager;
    private ObjectProperty energyProperty;
    
    public DashAbility(float energyCost, float cooldown) : base(energyCost, cooldown)
    {
        
    }

    public override void UseAbility(bool isActiveInput)
    {
        base.UseAbility(isActiveInput);
        if (isActiveInput)
        {
            if (timeAfterLastUse > _cooldown && energyProperty.GetCurValue() > _energyCost)
            {
                character.CharacterModel.layer = LayerMask.NameToLayer("NoCollisions");
            }
        }
        Debug.Log("Energy = "+_energyCost+" cooldown = "+_cooldown);
    }

    public override void InitializeAbility(ExCharacter character, MMFeedbacks startFeedback, MMFeedbacks endFeedback)
    {
        base.InitializeAbility(character, startFeedback, endFeedback);
        _propertyManager = character.GetComponent<PropertyManager>();
        energyProperty = _propertyManager.GetPropertyById(26);
    }
}
