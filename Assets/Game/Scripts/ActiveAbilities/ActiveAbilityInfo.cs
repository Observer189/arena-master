﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveAbilityInfo : ScriptableObject
{
   [Tooltip("Сколько энергии стоит использование способности")]
   public float energyCost;
   [Tooltip("Время перезарядки способности")]
   public float cooldown;

   public virtual ActiveAbility InitializeAbility()
   {
      return new ActiveAbility(energyCost,cooldown);
   }
}
