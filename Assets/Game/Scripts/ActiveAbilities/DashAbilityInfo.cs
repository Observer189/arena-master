﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Ability", menuName = "ActiveAbilities/Dash", order = 2)]
public class DashAbilityInfo : ActiveAbilityInfo
{
    public override ActiveAbility InitializeAbility()
    {
        return new DashAbility(energyCost,cooldown);
    }
}
