﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Feedbacks;
using MoreMountains.TopDownEngine;
using UnityEngine;

public class ActiveAbility
{
   protected float _energyCost;
   protected float _cooldown;

   protected float timeAfterLastUse=10000;

   protected ExCharacter character;
   protected MMFeedbacks startFeedback;
   protected MMFeedbacks endFeedback;

   public ActiveAbility(float energyCost, float cooldown)
   {
      _energyCost = energyCost;
      _cooldown = cooldown;
   }
    /// <summary>
    /// Способность должна вызываться каждый кадр, чтобы встроенный в неё отсчёт времени работал
    /// </summary>
    /// <param name="isActiveInput">Активна ли кнопка использования способности в данный момент</param>
   public virtual void UseAbility(bool isActiveInput)
   {
      
   }

   public virtual void InitializeAbility(ExCharacter character, MMFeedbacks startFeedback, MMFeedbacks endFeedback)
   {
      
   }

}
